export default {
  'project.common.ErrorDialog.title': 'Erro',

  'project.global.exceptions.NetworkError': 'Erro de comunicación',
  'project.global.buttons.close': 'Cerrar',
};
