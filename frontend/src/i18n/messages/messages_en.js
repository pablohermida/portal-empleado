export default {
  'project.common.ErrorDialog.title': 'Error',

  'project.global.exceptions.NetworkError': 'Network error',
  'project.global.buttons.close': 'Close',
};
