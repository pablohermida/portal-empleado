export default {
  'project.common.ErrorDialog.title': 'Error',

  'project.global.exceptions.NetworkError': 'Fallo de comunicación',
  'project.global.buttons.close': 'Cerrar',
};
