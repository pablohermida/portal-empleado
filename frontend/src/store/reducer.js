import {combineReducers} from 'redux';

import app from '../modules/app';
import employee from '../modules/employee';
import videoconference from '../modules/videoconference';

const reducer = combineReducers({
  app: app.reducer,
  employee: employee.reducer,
  videoconference: videoconference.reducer,
});

export default reducer;
