/* eslint-disable space-before-function-paren */
/* eslint-disable comma-dangle */
// TODO: Enable indent and fix errors on prettier
/* eslint-disable indent */
import {precacheAndRoute} from 'workbox-precaching';

const cacheName = 'app-files-v1';
const filesToCache = ['/', '/main.js'];

self.addEventListener('install', (event) => {
  event.waitUntil(
    caches.open(cacheName).then((cache) => {
      return cache.addAll(filesToCache);
    })
  );
});

precacheAndRoute(self.__WB_MANIFEST);

self.addEventListener('activate', function () {
  return self.clients.claim();
});

self.addEventListener('message', (e) => {
  if (e.data.action === 'skipWaiting') {
    self.skipWaiting();
  }
});
