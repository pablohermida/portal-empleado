import * as actionTypes from './actionTypes';
import * as employeeService from '../../api/services/employeeService';
import sha256 from 'crypto-js/sha256';
import hmacSHA512 from 'crypto-js/hmac-sha512';
import Base64 from 'crypto-js/enc-base64';

const initialEmployee = {
  name: null,
  lastname: null,
  dateBirth: null,
  email: null,
  dni: null,
  address: null,
  addressWork: null,
  phone: null,
  duration: null,
  dateStart: null,
  workDay: null,
  workHours: null,
  roleId: null,
  jobId: null,
  supervisorLogin: null,
  username: null,
  password: null,
  iban: null,
  vcsystems: null,
};

const setEmployee = (employee) => {
  const hashDigest = sha256(employee.password);
  initialEmployee.name = employee.name;
  initialEmployee.lastname = employee.lastname;
  initialEmployee.dateBirth = employee.dateBirth;
  initialEmployee.email = employee.email;
  initialEmployee.dni = employee.dni;
  initialEmployee.address = employee.address;
  initialEmployee.addressWork = employee.addressWork;
  initialEmployee.phone = employee.phone;
  initialEmployee.duration = employee.duration;
  initialEmployee.dateStart = employee.dateStart;
  initialEmployee.workDay = employee.workDay;
  if (employee.workHoursArray != null) {
    initialEmployee.workHours = employee.workHoursArray.toString();
  } else {
    initialEmployee.workHours = 'Pendiente de definir';
  }
  initialEmployee.roleId = employee.roleId;
  initialEmployee.jobId = employee.jobId;
  initialEmployee.supervisorLogin = employee.supervisorLogin;
  initialEmployee.username = employee.username;
  initialEmployee.password = Base64.stringify(
    hmacSHA512(hashDigest, process.env.HASH_PASS)
  );
  initialEmployee.iban = employee.iban;
  initialEmployee.vcsystems = employee.vcsystems;
  return initialEmployee;
};

export const changeEmployee = (key, value) => ({
  type: actionTypes.CHANGE_EMPLOYEE,
  key,
  value,
});

export const getRolesCompleted = (roles) => ({
  type: actionTypes.GET_ROLES,
  roles,
});

export const getRoles = () => (dispatch) => {
  employeeService.getRoles((response) => {
    dispatch(getRolesCompleted(response));
  });
};

export const getJobsCompleted = (jobs) => ({
  type: actionTypes.GET_JOBS,
  jobs,
});

export const getJobs = () => (dispatch) => {
  employeeService.getJobs((response) => {
    dispatch(getJobsCompleted(response));
  });
};

export const searchByUsernameCompleted = (employees) => ({
  type: actionTypes.GET_BY_USERNAME,
  employees,
});

export const searchByUsername = (username) => (dispatch) => {
  employeeService.searchByUsername(username, (response) => {
    dispatch(searchByUsernameCompleted(response));
  });
};

export const clearSupervisors = () => ({
  type: actionTypes.CLEAR_SUPERVISORS,
});

export const signUp = (employee, onSuccess, onError) => (dispatch) => {
  employeeService.signUp(
    setEmployee(employee),
    (response) => {
      onSuccess();
    },
    (error) => {
      onError(error);
    }
  );
};

export const editEmployee = (id, employee, onSuccess, onError) => (
  dispatch
) => {
  employeeService.editEmployee(
    id,
    setEmployee(employee),
    (response) => {
      onSuccess();
    },
    (error) => {
      onError(error);
    }
  );
};

export const loginCompleted = (login) => ({
  type: actionTypes.LOGIN_COMPLETED,
  login,
});

export const login = (
  {username, password},
  onSuccess,
  onError,
  reauthCallback
) => (dispatch) => {
  const hashDigest = sha256(password);
  const enPass = Base64.stringify(hmacSHA512(hashDigest, 'portalempleado'));
  employeeService.login(
    {
      username,
      password: enPass,
    },
    (auth) => {
      dispatch(loginCompleted(auth));
      onSuccess();
    },
    onError,
    reauthCallback
  );
};

export const loginFromServiceToken = (onError) => (dispatch) => {
  employeeService.loginFromServiceToken((auth) => {
    dispatch(loginCompleted(auth));
  }, onError);
};

export const logout = () => {
  employeeService.logout();
  return {type: actionTypes.CLEAR_LOGIN};
};

export const getEmployeeByIdCompleted = (employee) => ({
  type: actionTypes.GET_BY_ID,
  employee,
});

export const getEmployeeById = (id, onError) => (dispatch) => {
  employeeService.getById(
    id,
    (employee) => {
      dispatch(getEmployeeByIdCompleted(employee));
    },
    onError
  );
};

export const clearEmployee = () => ({
  type: actionTypes.CLEAR_EMPLOYEE,
});

export const getEmployeesByFiltersCompleted = (list) => ({
  type: actionTypes.GET_BY_FILTERS,
  list,
});

export const getEmployeesByFilters = (filters, onError) => (dispatch) => {
  employeeService.getByFilters(
    filters,
    (list) => {
      dispatch(getEmployeesByFiltersCompleted(list));
    },
    onError
  );
};

export const changeFilter = (key, value) => ({
  type: actionTypes.CHANGE_FILTER,
  key,
  value,
});

export const unsuscribe = (id, onSuccess, onError) => (dispatch) => {
  employeeService.unsuscribe(
    id,
    () => {
      onSuccess();
      dispatch(getEmployeesByFilters('', onError));
    },
    onError
  );
};
