import React from 'react';
import {Switch, Route, useRouteMatch} from 'react-router-dom';
import PropTypes from 'prop-types';
import {useHistory} from 'react-router-dom';

import EmployeeForm from './EmployeeForm';
import EmployeeList from './EmployeeList';
import EmployeeDetails from './EmployeeDetails';

const RouterEmployee = (props) => {
  const {setError, setSuccess} = props;
  const {path} = useRouteMatch();
  const history = useHistory();

  const successForm = (data) => {
    setSuccess(data);
    history.goBack();
  };

  return (
    <div>
      <Switch>
        <Route exact path={`${path}/signUp`}>
          <EmployeeForm setError={setError} setSuccess={successForm} />
        </Route>
        <Route
          exact
          path={`${path}/edit/:id`}
          render={(props) => (
            <EmployeeForm
              setError={setError}
              setSuccess={successForm}
              id={Number(props.match.params.id)}
            />
          )}
        />
        <Route
          exact
          path={`${path}/details/:id`}
          render={(props) => (
            <EmployeeDetails
              setError={setError}
              id={Number(props.match.params.id)}
            />
          )}
        />
        <Route exact path={`${path}/list`}>
          <EmployeeList setError={setError} setSuccess={successForm} />
        </Route>
      </Switch>
    </div>
  );
};

RouterEmployee.propTypes = {
  setError: PropTypes.func.isRequired,
  setSuccess: PropTypes.func.isRequired,
};

export default RouterEmployee;
