import React from 'react';
import {useSelector} from 'react-redux';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

import * as selectors from '../../selectors';

import {
  AccountCircle,
  Visibility as VisibilityIcon,
  Create as CreateIcon,
  DeleteForever as DeleteForeverIcon,
} from '@material-ui/icons';

import {useStyles} from '../../../../assets/styles/theme';

const Fields = (props) => {
  const {onDelete} = props;

  return [
    {
      field: 'image',
      headerName: 'Image',
      description: 'Image of employee',
      sortable: false,
      flex: 0.5,
      type: 'string',
      renderCell: (params) => (
        <div className={useStyles().dataGridSvg}>
          {!params.value && <AccountCircle fontSize="large" />}
        </div>
      ),
    },
    {
      field: 'username',
      headerName: 'Username',
      description: 'Username of employee',
      sortable: true,
      flex: 1,
      type: 'string',
    },
    {
      field: 'name',
      headerName: 'Name',
      description: 'Fullname of employee',
      sortable: true,
      flex: 1.5,
      type: 'string',
    },
    {
      field: 'job',
      headerName: 'Job',
      description: 'Job of employee',
      sortable: true,
      flex: 1,
      type: 'string',
    },
    {
      field: 'email',
      headerName: 'Email',
      description: 'Email of employee',
      sortable: true,
      flex: 1.5,
      type: 'string',
    },
    {
      field: 'phone',
      headerName: 'Phone',
      description: 'Phone of employee',
      sortable: false,
      flex: 1,
      type: 'string',
    },
    {
      field: 'id',
      headerName: '#',
      description: '',
      flex: 1,
      sortable: false,
      renderCell: (params) => (
        <div className={useStyles().dataGridActions}>
          <Link id="view-details-btn" to={`/employee/details/${params.value}`}>
            <VisibilityIcon />
          </Link>
          {useSelector(selectors.getLogin).roleId == 1 && (
            <span>
              <Link id="edit-form-btn" to={`/employee/edit/${params.value}`}>
                <CreateIcon />
              </Link>
              <DeleteForeverIcon
                id="unsub-btn"
                onClick={() => onDelete(params.value)}
              />
            </span>
          )}
        </div>
      ),
    },
  ];
};

Fields.propTypes = {
  onDelete: PropTypes.func.isRequired,
};

export default Fields;
