import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import PropTypes from 'prop-types';

import {useStyles} from '../../../assets/styles/theme';

import {DataGrid} from '@material-ui/data-grid';

import * as actions from '../actions';
import * as selectors from '../selectors';

import {Grid, Typography} from '@material-ui/core';
import {FilterList as FilterListIcon} from '@material-ui/icons';

import Fields from './list/fields';
import FilterList from '../../common/components/FilterList';
import ModalConfirm from '../../common/components/ModalConfirm';

const EmployeeList = (props) => {
  const {setError, setSuccess} = props;
  const dispatch = useDispatch();
  const classes = useStyles();

  const [openModal, setOpenModal] = React.useState(false);
  const [idUnsuscribe, setIdUnsuscribe] = React.useState(0);

  const [sortModel, setSortModel] = React.useState([
    {field: 'username', sort: 'asc'},
  ]);

  const [openFilter, setOpenFilter] = React.useState(false);

  const handleSortModelChange = (params) => {
    if (params.sortModel !== sortModel) {
      setSortModel(params.sortModel);
    }
    onFilterList();
  };

  const list = useSelector(selectors.getEmployeeList);
  const filters = useSelector(selectors.getFilters);
  const roles = useSelector(selectors.getRoles);
  const jobs = useSelector(selectors.getJobs);

  filters.map((filter) => {
    if (filter.name === 'roleId') {
      filter.options = roles;
    }
    if (filter.name === 'jobsId') {
      filter.options = jobs;
    }
  });

  React.useEffect(() => {
    dispatch(actions.getRoles());
    dispatch(actions.getJobs());
    dispatch(
      actions.getEmployeesByFilters(onFilterList(), (error) => {
        setError(error);
      })
    );
  }, [dispatch]);

  const setOrderRequest = () => {
    if (sortModel[0]) {
      const order = sortModel[0] ? sortModel[0].sort : 'asc';
      const orderAttribute = sortModel ? sortModel[0].field : 'username';
      return `?order=${order}&orderAttribute=${orderAttribute}`;
    }

    return '';
  };

  const changeEmployeeFilter = (idx, value, isChecked) => {
    const filterChange = filters[idx];
    if (isChecked) {
      filterChange.value.push(value);
    } else {
      filterChange.value = value;
    }

    dispatch(actions.changeFilter([idx], filterChange));
  };

  const onFilterList = () => {
    let filtersToUrl = '';
    filtersToUrl = setOrderRequest();
    if (filtersToUrl === '') {
      filtersToUrl += '?';
    } else {
      filtersToUrl += '&';
    }
    filters.map((filter) => {
      switch (filter.type) {
        case 'string':
          filtersToUrl += filter.name + '=' + filter.value + '&';
          break;
        case 'select':
          if (filter.value != 0) {
            filtersToUrl += filter.name + '=' + filter.value + '&';
          }
          break;
        case 'multivalue':
          filter.value.map((opt) => {
            filtersToUrl += filter.name + '=' + opt + '&';
          });
          break;
        default:
          break;
      }
    });

    dispatch(
      actions.getEmployeesByFilters(filtersToUrl.slice(0, -1), (error) => {
        setError(error);
      })
    );
    setOpenFilter(!openFilter);
  };

  const handleOpenFilter = () => {
    setOpenFilter(!openFilter);
  };

  const closeModal = () => {
    setOpenModal(false);
  };

  const unsuscribeEmployee = () => {
    dispatch(
      actions.unsuscribe(idUnsuscribe, () => {
        setSuccess('Employee unsuscribed succesfully');
      }),
      (error) => {
        setError(error);
      }
    );
  };

  const onDeleteEmployee = (id) => {
    setIdUnsuscribe(id);
    setOpenModal(true);
  };

  const fields = new Fields({onDelete: onDeleteEmployee});

  return (
    <div className={classes.dataGrid} id="list" data-test="list">
      <Grid container alignItems="center" spacing={2}>
        <Grid item xs={12} sm={10}>
          <Typography variant="h4" gutterBottom align="center">
            List Employees
          </Typography>
        </Grid>
        <Grid item xs={12} sm={2}>
          <FilterListIcon
            id="filter-show"
            size="large"
            onClick={handleOpenFilter}
          />
        </Grid>
      </Grid>
      <FilterList
        filters={filters}
        changeValue={changeEmployeeFilter}
        onFilter={onFilterList}
        open={openFilter}
      />
      {list.items && (
        <DataGrid
          rows={list.items}
          columns={fields}
          paginationMode="server"
          sortingMode="server"
          sortModel={sortModel}
          onSortModelChange={handleSortModelChange}
          disableColumnMenu
          pageSize={10}
          rowsPerPageOptions={[]}
          pagination
        />
      )}
      <ModalConfirm
        open={openModal}
        onClose={closeModal}
        confirm={unsuscribeEmployee}
        text={'Are you sure you want to unsuscribe this employee?'}
      />
    </div>
  );
};

EmployeeList.propTypes = {
  setError: PropTypes.func.isRequired,
  setSuccess: PropTypes.func.isRequired,
};

export default EmployeeList;
