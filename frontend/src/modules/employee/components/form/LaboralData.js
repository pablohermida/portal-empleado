import React from 'react';
import PropTypes from 'prop-types';
import {useSelector, useDispatch} from 'react-redux';
import {useHistory} from 'react-router-dom';
import {useStyles} from '../../../../assets/styles/theme';

import {Grid, TextField, Button} from '@material-ui/core';

import * as selectors from '../../selectors';
import * as actions from '../../actions';
import ValidateTextField from '../../../common/components/ValidateTextField';
import VideoConferenceEmployee from './VideoConferenceEmployee';

const LaboralData = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const classes = useStyles();
  const {value, index, changeIndex, actionForm, setError} = props;

  const employee = useSelector(selectors.getEmployee);
  const roleId = useSelector(selectors.getLogin).roleId;

  const changeEmployee = (e) => {
    dispatch(actions.changeEmployee([e.target.name], e.target.value));
  };

  let errors = {
    password: null,
    repeatPassword: null,
    name: null,
    lastname: null,
    dateBirth: null,
    email: null,
    iban: null,
    dni: null,
    address: null,
    phone: null,
  };

  const changeError = (name, error) => {
    errors = {...errors, [name]: error};
  };

  const checkErrors = (e) => {
    e.preventDefault();
    let isError = false;
    Object.entries(errors).forEach(([key, value]) => {
      if (value == null || value) {
        changeError(key, value);
        isError = true;
      }
    });
    if (!isError) {
      actionForm();
    }
  };

  const changeWithRole = () => {
    if (roleId != 1) {
      history.back();
    } else {
      changeIndex();
    }
  };

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`nav-tabpanel-${index}`}
      aria-labelledby={`nav-tab-${index}`}
      data-test="laboral-data"
    >
      {value === index && (
        <form
          className={classes.form}
          noValidate
          autoComplete="off"
          aria-labelledby="save"
        >
          <Grid container alignItems="center" spacing={2}>
            <Grid item xs={12} sm={6} md={3}>
              <ValidateTextField
                name="password"
                value={employee.password}
                hasError={changeError}
                min={6}
                max={24}
                required
              >
                <TextField
                  id="password"
                  name="password"
                  label="Password"
                  value={employee.password ? employee.password : ''}
                  onChange={changeEmployee}
                  type="password"
                  variant="outlined"
                  required
                />
              </ValidateTextField>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <ValidateTextField
                name="repeatPassword"
                value={employee.repeatPassword}
                hasError={changeError}
                equalTo={employee.password}
                required
              >
                <TextField
                  id="repeatPassword"
                  name="repeatPassword"
                  label="Repeat Password"
                  value={employee.repeatPassword ? employee.repeatPassword : ''}
                  onChange={changeEmployee}
                  type="password"
                  variant="outlined"
                  required
                />
              </ValidateTextField>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <ValidateTextField
                name="name"
                value={employee.name}
                hasError={changeError}
                min={2}
                max={30}
                required
              >
                <TextField
                  id="name"
                  name="name"
                  label="Name"
                  value={employee.name ? employee.name : ''}
                  onChange={changeEmployee}
                  type="text"
                  variant="outlined"
                  required
                />
              </ValidateTextField>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <ValidateTextField
                name="lastname"
                value={employee.lastname}
                hasError={changeError}
                min={2}
                max={30}
                required
              >
                <TextField
                  id="lastname"
                  name="lastname"
                  label="Lastname"
                  value={employee.lastname ? employee.lastname : ''}
                  onChange={changeEmployee}
                  type="text"
                  variant="outlined"
                  required
                />
              </ValidateTextField>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <ValidateTextField
                name="dateBirth"
                value={employee.dateBirth}
                hasError={changeError}
                required
              >
                <TextField
                  id="dateBirth"
                  name="dateBirth"
                  label="Date Birth"
                  value={employee.dateBirth ? employee.dateBirth : ''}
                  onChange={changeEmployee}
                  type="date"
                  variant="outlined"
                  required
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </ValidateTextField>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <ValidateTextField
                name="email"
                value={employee.email}
                hasError={changeError}
                min={6}
                max={255}
                required
              >
                <TextField
                  id="email"
                  name="email"
                  label="Email"
                  value={employee.email ? employee.email : ''}
                  onChange={changeEmployee}
                  type="email"
                  variant="outlined"
                  required
                />
              </ValidateTextField>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <ValidateTextField
                name="iban"
                value={employee.iban}
                hasError={changeError}
                min={24}
                max={24}
                required
              >
                <TextField
                  id="iban"
                  name="iban"
                  label="IBAN"
                  value={employee.iban ? employee.iban : ''}
                  onChange={changeEmployee}
                  type="text"
                  variant="outlined"
                  required
                />
              </ValidateTextField>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <ValidateTextField
                name="dni"
                value={employee.dni}
                hasError={changeError}
                min={9}
                max={9}
                required
              >
                <TextField
                  id="dni"
                  name="dni"
                  label="DNI"
                  value={employee.dni ? employee.dni : ''}
                  onChange={changeEmployee}
                  type="text"
                  variant="outlined"
                  required
                />
              </ValidateTextField>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <ValidateTextField
                name="address"
                value={employee.address}
                hasError={changeError}
                min={10}
                max={100}
                required
              >
                <TextField
                  id="address"
                  name="address"
                  label="Address"
                  value={employee.address ? employee.address : ''}
                  onChange={changeEmployee}
                  type="text"
                  variant="outlined"
                  required
                />
              </ValidateTextField>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <ValidateTextField
                name="phone"
                value={employee.phone}
                hasError={changeError}
                min={9}
                max={12}
                required
              >
                <TextField
                  id="phone"
                  name="phone"
                  label="Phone"
                  value={employee.phone ? employee.phone : ''}
                  onChange={changeEmployee}
                  type="text"
                  variant="outlined"
                  required
                />
              </ValidateTextField>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <VideoConferenceEmployee
                vcsystems={employee.vcsystems}
                setError={setError}
              />
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <Button
                id="back"
                variant="contained"
                color="secondary"
                onClick={changeWithRole}
              >
                Back
              </Button>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Button
                id="save"
                variant="contained"
                color="primary"
                type="submit"
                onClick={checkErrors}
              >
                Save
              </Button>
            </Grid>
          </Grid>
        </form>
      )}
    </div>
  );
};

LaboralData.propTypes = {
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
  changeIndex: PropTypes.func.isRequired,
  actionForm: PropTypes.func.isRequired,
  setError: PropTypes.func.isRequired,
};

export default LaboralData;
