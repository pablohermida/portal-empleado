/* eslint-disable comma-dangle */
/* eslint-disable indent */
// FIXME: enable indent prettier
import React from 'react';
import PropTypes from 'prop-types';
import {useSelector, useDispatch} from 'react-redux';

import * as actions from '../../actions';
import * as selectors from '../../selectors';

import {TextField} from '@material-ui/core';

import {Selector} from '../../../videoconference';

import ValidateTextField from '../../../common/components/ValidateTextField';

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
} from '@material-ui/core';

const VideoConferenceEmployee = (props) => {
  const dispatch = useDispatch();
  const {setError} = props;
  const [open, setOpen] = React.useState(false);

  const employee = useSelector(selectors.getEmployee);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const setSuccess = () => {};

  let errors = {
    data: null,
  };

  const changeError = (name, error) => {
    errors = {...errors, [name]: error};
  };

  const changeVcSystem = (position, idVcsystem) => {
    employee.vcsystems[position].idVcsystem = idVcsystem;
    dispatch(actions.changeEmployee(['vcsystems'], employee.vcsystems));
  };

  const changeVcSystemData = (e) => {
    const position = e.target.id.substr(e.target.id.search('data') + 4);
    employee.vcsystems[position].data = e.target.value;
    dispatch(actions.changeEmployee(['vcsystems'], employee.vcsystems));
  };

  const handleNewVcSystem = () => {
    let addNew = true;
    employee.vcsystems.forEach((vce) => {
      if (vce.idVcsystem <= 0) {
        addNew = false;
      }
    });
    if (addNew) {
      employee.vcsystems.push({
        idVcsystem: 0,
        data: '',
      });
      dispatch(actions.changeEmployee(['vcsystems'], employee.vcsystems));
    }
  };

  return (
    <div data-test="vcsystems">
      <Button
        id="openVcSystems"
        variant="outlined"
        color="primary"
        onClick={handleClickOpen}
      >
        Videoconference Systems
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="vcsystems"
        fullWidth={true}
        maxWidth={'lg'}
      >
        <DialogTitle id="vcsystems">VideoConference Systems</DialogTitle>
        <DialogContent>
          {employee.vcsystems &&
            employee.vcsystems.map((vcs, pos) => (
              <Grid key={pos} container alignItems="center" spacing={2}>
                <Grid item xs={12} md={6}>
                  <Selector
                    setError={setError}
                    setSuccess={setSuccess}
                    value={vcs.idVcsystem}
                    position={pos}
                    changeVcSystem={changeVcSystem}
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <ValidateTextField
                    name="data"
                    value={vcs.data}
                    hasError={changeError}
                    min={0}
                    max={255}
                    required
                  >
                    <TextField
                      id={`data${pos}`}
                      name="data"
                      label="URL"
                      value={vcs.data}
                      onChange={changeVcSystemData}
                      type="text"
                      variant="outlined"
                      required
                    />
                  </ValidateTextField>
                </Grid>
              </Grid>
            ))}
        </DialogContent>
        <DialogActions>
          <Button id="other" onClick={handleNewVcSystem} color="primary">
            Add Other
          </Button>
          <Button id="saveAndClose" onClick={handleClose} color="primary">
            Save & Close
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

VideoConferenceEmployee.propTypes = {
  setError: PropTypes.func.isRequired,
};

export default VideoConferenceEmployee;
