import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {useHistory} from 'react-router-dom';
import PropTypes from 'prop-types';
import {useStyles} from '../../../../assets/styles/theme';

import * as selectors from '../../selectors';
import * as actions from '../../actions';

import {
  Grid,
  TextField,
  MenuItem,
  CircularProgress,
  Button,
} from '@material-ui/core';
import {Autocomplete} from '@material-ui/lab';

import WorkingHour from './WorkingHour';
import ValidateTextField from '../../../common/components/ValidateTextField';

const AdminData = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const classes = useStyles();
  const {value, index, changeIndex} = props;

  const employee = useSelector(selectors.getEmployee);
  const roleId = useSelector(selectors.getLogin).roleId;

  const changeEmployee = (e) => {
    dispatch(actions.changeEmployee([e.target.name], e.target.value));
  };

  const changeSupervisor = (e, supervisor) => {
    let username = null;
    if (supervisor) {
      username = supervisor.username;
    }
    dispatch(actions.changeEmployee(['supervisorLogin'], username));
  };

  const roles = useSelector(selectors.getRoles);
  const jobs = useSelector(selectors.getJobs);
  const workDays = useSelector(selectors.getWorkDays);
  const supervisors = useSelector(selectors.getSupervisors);

  const [showSupervisors, setShowSupervisors] = React.useState(false);
  const loadingSupervisor = showSupervisors && supervisors.length === 0;

  let errors = {
    roleId: null,
    username: null,
    jobId: null,
    duration: null,
    dateStart: null,
    workDay: null,
    addressWork: null,
  };

  const changeError = (name, error) => {
    errors = {...errors, [name]: error};
  };

  const checkErrors = (e) => {
    e.preventDefault();
    let isError = false;
    Object.entries(errors).forEach(([key, value]) => {
      if (value == null || value) {
        changeError(key, value);
        isError = true;
      }
    });
    if (!isError) {
      changeIndex();
    }
  };

  const searchByTerm = (term) => {
    if (term) {
      dispatch(actions.searchByUsername(term));
    } else {
      dispatch(actions.clearSupervisors());
    }
  };

  React.useEffect(() => {
    dispatch(actions.getRoles());
    dispatch(actions.getJobs());
  }, [dispatch]);

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`nav-tabpanel-${index}`}
      aria-labelledby={`nav-tab-${index}`}
      data-test="admin-data"
    >
      {value === index && roleId == 1 && (
        <form
          className={classes.form}
          noValidate
          autoComplete="off"
          aria-labelledby="next"
        >
          <Grid container alignItems="center" spacing={2}>
            <Grid item xs={12} sm={6} md={3}>
              <ValidateTextField
                name="roleId"
                value={employee.roleId}
                hasError={changeError}
                required
              >
                <TextField
                  id="role"
                  select
                  label="Type"
                  name="roleId"
                  value={employee.roleId}
                  onChange={changeEmployee}
                  variant="outlined"
                  required
                >
                  <MenuItem value={0}>Selecciona un rol</MenuItem>
                  {roles.length > 0 &&
                    roles.map((role) => (
                      <MenuItem key={role.id} value={role.id}>
                        {role.name}
                      </MenuItem>
                    ))}
                </TextField>
              </ValidateTextField>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <ValidateTextField
                name="username"
                value={employee.username}
                min={3}
                max={30}
                hasError={changeError}
                required
              >
                <TextField
                  id="username"
                  label="Username"
                  name="username"
                  value={employee.username}
                  onChange={changeEmployee}
                  type="text"
                  variant="outlined"
                  required
                />
              </ValidateTextField>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <ValidateTextField
                name="jobId"
                value={employee.jobId}
                hasError={changeError}
                required
              >
                <TextField
                  id="job"
                  select
                  label="Job"
                  name="jobId"
                  value={employee.jobId}
                  onChange={changeEmployee}
                  variant="outlined"
                  required
                >
                  <MenuItem value={0}>Selecciona un cargo</MenuItem>
                  {jobs.length > 0 &&
                    jobs.map((job) => (
                      <MenuItem key={job.id} value={job.id}>
                        {job.name}
                      </MenuItem>
                    ))}
                </TextField>
              </ValidateTextField>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <ValidateTextField
                name="duration"
                value={employee.duration}
                min={5}
                max={10}
                hasError={changeError}
                required
              >
                <TextField
                  id="duration"
                  label="Duration"
                  name="duration"
                  value={employee.duration}
                  onChange={changeEmployee}
                  type="text"
                  variant="outlined"
                  required
                />
              </ValidateTextField>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <ValidateTextField
                name="dateStart"
                value={employee.dateStart}
                hasError={changeError}
                required
              >
                <TextField
                  id="dateStart"
                  label="Start Date"
                  name="dateStart"
                  value={employee.dateStart}
                  onChange={changeEmployee}
                  type="date"
                  variant="outlined"
                  required
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </ValidateTextField>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <ValidateTextField
                name="workDay"
                value={employee.workDay}
                hasError={changeError}
                required
              >
                <TextField
                  id="workDay"
                  select
                  label="Work Day"
                  name="workDay"
                  value={employee.workDay}
                  onChange={changeEmployee}
                  variant="outlined"
                  required
                >
                  {workDays.map((workDay) => (
                    <MenuItem key={workDay.id} value={workDay.name}>
                      {workDay.name}
                    </MenuItem>
                  ))}
                </TextField>
              </ValidateTextField>
            </Grid>
            <Grid item xs={12} md={6}>
              <ValidateTextField
                name="addressWork"
                value={employee.addressWork}
                min={3}
                max={100}
                hasError={changeError}
                required
              >
                <TextField
                  id="work_address"
                  label="Work Address"
                  name="addressWork"
                  value={employee.addressWork}
                  onChange={changeEmployee}
                  multiline
                  rows={2}
                  variant="outlined"
                  required
                />
              </ValidateTextField>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <Autocomplete
                id="supervisor"
                value={
                  employee.supervisorLogin ? employee.supervisorLogin : null
                }
                onChange={changeSupervisor}
                open={showSupervisors}
                onOpen={() => {
                  setShowSupervisors(true);
                }}
                onClose={() => {
                  setShowSupervisors(false);
                }}
                getOptionLabel={(option) => {
                  if (option.fullname) return option.fullname;
                  return option;
                }}
                getOptionSelected={(option, value) => option.username === value}
                options={supervisors}
                loading={loadingSupervisor}
                onInputChange={(event, searchTerm) => {
                  searchByTerm(searchTerm);
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Supervisor"
                    variant="outlined"
                    InputProps={{
                      ...params.InputProps,
                      endAdornment: (
                        <React.Fragment>
                          {loadingSupervisor ? (
                            <CircularProgress color="inherit" size={20} />
                          ) : null}
                          {params.InputProps.endAdornment}
                        </React.Fragment>
                      ),
                    }}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <WorkingHour
                workHour={
                  employee.workHoursArray ? employee.workHoursArray : []
                }
              />
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <Button
                id="cancel"
                variant="contained"
                color="secondary"
                onClick={() => history.goBack()}
              >
                Cancel
              </Button>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Button
                id="next"
                variant="contained"
                color="primary"
                type="submit"
                onClick={checkErrors}
              >
                Next
              </Button>
            </Grid>
          </Grid>
        </form>
      )}
    </div>
  );
};

AdminData.propTypes = {
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
  changeIndex: PropTypes.func.isRequired,
};

export default AdminData;
