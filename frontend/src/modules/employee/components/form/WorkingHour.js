/* eslint-disable comma-dangle */
/* eslint-disable indent */
// FIXME: enable indent prettier
import React from 'react';
import PropTypes from 'prop-types';
import {useStyles} from '../../../../assets/styles/theme';

import {
  Button,
  Slider,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
  Grid,
  FormControlLabel,
  Checkbox,
} from '@material-ui/core';

const WorkingHour = (props) => {
  const classes = useStyles();
  const {workHour} = props;
  const [open, setOpen] = React.useState(false);
  const [worksHours, setWorksHours] = React.useState(workHour);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const changeIsActive = (e) => {
    const elementsIndex = worksHours.findIndex(
      (element) => element.id == e.target.name
    );
    const newArray = [...worksHours];
    newArray[elementsIndex] = {
      ...newArray[elementsIndex],
      isActive: e.target.checked,
    };
    setWorksHours(newArray);
  };

  const changeIsContinuous = (e) => {
    const elementsIndex = worksHours.findIndex(
      (element) => element.id == e.target.name
    );
    const newArray = [...worksHours];
    newArray[elementsIndex] = {
      ...newArray[elementsIndex],
      isContinuous: e.target.checked,
    };
    newArray[elementsIndex] = {
      ...newArray[elementsIndex],
      value: e.target.checked ? [5, 17] : [5, 17, 20, 25],
    };
    setWorksHours(newArray);
  };

  const changeHour = (name) => (e, newValue) => {
    const elementsIndex = worksHours.findIndex((element) => element.id == name);
    const newArray = [...worksHours];
    newArray[elementsIndex] = {...newArray[elementsIndex], value: newValue};
    setWorksHours(newArray);
  };

  const valuetext = (value) => {
    const i = value;
    const hour = Math.floor(i / 2) + 6;
    const minute = i % 2 != 0 ? ':30' : ':00';
    return hour + minute;
  };

  const hours = [];

  for (let i = 0; i <= 32; i += 1) {
    let hour = Math.floor(i / 2) + 6;
    hour = (hour < 10 ? '0' : '') + hour;
    const label = i % 2 != 0 ? '' : hour + ':00';
    hours.push({
      value: i,
      label: label,
    });
  }

  return (
    <div data-test="working-hour">
      {worksHours.length > 0 && (
        <div>
          <Button
            id="openWorkHour"
            variant="outlined"
            color="primary"
            onClick={handleClickOpen}
          >
            Choose a Work Hour
          </Button>
          <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="workingHour"
            fullWidth={true}
            maxWidth={'md'}
          >
            <DialogTitle id="workingHour">Working Hour</DialogTitle>
            <DialogContent className={classes.workHour}>
              <Grid container spacing={2}>
                {worksHours.map((day) => (
                  <Grid
                    key={day.id}
                    style={{marginBottom: '10px'}}
                    item
                    xs={12}
                  >
                    <Typography
                      variant="h6"
                      style={{display: 'inline-block', marginRight: '10px'}}
                    >
                      {day.name}
                    </Typography>
                    <FormControlLabel
                      className={'isActive'}
                      control={
                        <Checkbox
                          checked={day.isActive}
                          onChange={changeIsActive}
                          name={day.id}
                          color="primary"
                        />
                      }
                      label={'Is Active'}
                    />
                    <FormControlLabel
                      className={'isContinuous'}
                      control={
                        <Checkbox
                          checked={day.isContinuous}
                          onChange={changeIsContinuous}
                          name={day.id}
                          color="primary"
                        />
                      }
                      label={'Is Continuous'}
                    />
                    <Slider
                      orientation="horizontal"
                      name={day.id}
                      value={day.value}
                      onChange={changeHour(day.id)}
                      aria-labelledby="change-hour"
                      step={1}
                      marks={hours}
                      track={false}
                      disabled={!day.isActive}
                      min={0}
                      max={32}
                      valueLabelFormat={valuetext}
                      valueLabelDisplay="auto"
                    />
                  </Grid>
                ))}
              </Grid>
            </DialogContent>
            <DialogActions>
              <Button id="close" onClick={handleClose} color="primary">
                Save
              </Button>
            </DialogActions>
          </Dialog>
        </div>
      )}
    </div>
  );
};

WorkingHour.propTypes = {
  workHour: PropTypes.any,
};

export default WorkingHour;
