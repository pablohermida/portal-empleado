import React from 'react';
import {useSelector} from 'react-redux';
import {Grid} from '@material-ui/core';

import * as selectors from '../../selectors';

import PropTypes from 'prop-types';
import {useStyles} from '../../../../assets/styles/theme';

const MoreInfo = (props) => {
  const employee = useSelector(selectors.getEmployee);
  const classes = useStyles();

  return (
    <div data-test="more-info" id="more-info" className={classes.root}>
      <Grid container alignItems="flex-start" spacing={2}>
        <Grid item xs={12} sm={6} md={4}>
          <strong>Address </strong>
          {employee.address}
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <strong>Address Work </strong>
          {employee.addressWork}
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <strong>DNI </strong>
          {employee.dni}
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <strong>Duration </strong>
          {employee.duration}
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <strong>IBAN </strong>
          {employee.iban}
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <strong>WorkDay </strong>
          {employee.workDay}
        </Grid>
      </Grid>
    </div>
  );
};

MoreInfo.propTypes = {
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
  changeIndex: PropTypes.func.isRequired,
};

export default MoreInfo;
