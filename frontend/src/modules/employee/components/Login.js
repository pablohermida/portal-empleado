import React from 'react';
import {useDispatch} from 'react-redux';
import {useHistory} from 'react-router-dom';
import PropTypes from 'prop-types';
import {useStyles} from '../../../assets/styles/theme';

import * as actions from '../actions';

import {
  Grid,
  Card,
  CardContent,
  Typography,
  Button,
  TextField,
} from '@material-ui/core';

import ValidateTextField from '../../common/components/ValidateTextField';

const Login = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const classes = useStyles();

  const {setError} = props;

  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');

  const changeUsername = (e) => {
    setUsername(e.target.value);
  };

  const changePassword = (e) => {
    setPassword(e.target.value);
  };

  const sendLogin = (e) => {
    e.preventDefault();
    dispatch(
      actions.login(
        {username, password},
        (response) => history.push('/'),
        (errors) => {
          setError(errors);
        },
        () => {
          history.push('/login');
          //TODO: add dispatch actions logout
        }
      )
    );
  };

  return (
    <div className={classes.login} id="login" data-test="login">
      <Grid container alignItems="center" justify="center">
        <Grid item xs={12} sm={8} md={6} lg={4}>
          <Card>
            <CardContent>
              <Typography
                variant="h4"
                align="center"
                color="secondary"
                gutterBottom
              >
                Portal del Empleado
              </Typography>
              <form
                className={classes.form}
                noValidate
                autoComplete="off"
                aria-labelledby="login-btn"
              >
                <Grid container alignItems="center">
                  <Grid item xs={12}>
                    <ValidateTextField
                      name="username"
                      value={username}
                      min={3}
                      max={30}
                      required
                    >
                      <TextField
                        id="username"
                        label="Username"
                        name="username"
                        value={username}
                        onChange={changeUsername}
                        type="text"
                        variant="outlined"
                        required
                      />
                    </ValidateTextField>
                  </Grid>
                  <Grid item xs={12}>
                    <ValidateTextField
                      name="password"
                      value={password}
                      min={6}
                      max={24}
                      required
                    >
                      <TextField
                        id="password"
                        name="password"
                        label="Password"
                        value={password}
                        onChange={changePassword}
                        type="password"
                        variant="outlined"
                        required
                      />
                    </ValidateTextField>
                  </Grid>
                </Grid>
                <Grid container justify="flex-end">
                  <Grid item xs={2}>
                    <Button
                      id="login-btn"
                      variant="contained"
                      color="primary"
                      type="submit"
                      onClick={sendLogin}
                    >
                      Send
                    </Button>
                  </Grid>
                </Grid>
              </form>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </div>
  );
};

Login.propTypes = {
  setError: PropTypes.func.isRequired,
};

export default Login;
