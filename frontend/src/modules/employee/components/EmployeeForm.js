import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import PropTypes from 'prop-types';
import {useStyles} from '../../../assets/styles/theme';

import * as actions from '../actions';
import * as selectors from '../selectors';

import {Typography, Stepper, Step, StepButton} from '@material-ui/core';

import AdminData from './form/AdminData';
import LaboralData from './form/LaboralData';
import ModalConfirm from '../../common/components/ModalConfirm';

const EmployeeForm = (props) => {
  const [openModal, setOpenModal] = React.useState(false);
  const dispatch = useDispatch();
  const classes = useStyles();
  const roleId = useSelector(selectors.getLogin).roleId;
  const userLoginId = useSelector(selectors.getLogin).employeeId;

  const {setError, setSuccess, id} = props;

  const [tab, setTab] = React.useState(0);

  const changeTab = (newTab) => {
    setTab(newTab);
  };

  const nextTab = (e) => {
    setTab(tab + 1);
  };

  const prevTab = (e) => {
    setTab(tab - 1);
  };

  React.useEffect(() => {
    if (id != null) {
      dispatch(
        actions.getEmployeeById(id, (error) => {
          setError(error);
        })
      );
    } else {
      dispatch(actions.clearEmployee());
    }
  }, [id]);

  const employee = useSelector(selectors.getEmployee);

  const signUpEmployee = () => {
    let novcs = false;
    employee.vcsystems.forEach((vcs) => {
      if (vcs.idvcsystem == 0 || vcs.idvcsystem == Number.MAX_VALUE) {
        novcs = true;
      }
    });
    if (employee.vcsystems.length == 0 || novcs) {
      setOpenModal(true);
    } else {
      sendEmployee(employee);
    }
  };

  const sendEmployee = (employee) => {
    if (id != null) {
      dispatch(
        actions.editEmployee(
          id,
          employee,
          () => {
            setSuccess('Edit employee Succesfully!');
          },
          (errors) => {
            setError(errors);
          }
        )
      );
    } else {
      dispatch(
        actions.signUp(
          employee,
          () => {
            setSuccess('Sign Up Succesfully!');
          },
          (errors) => {
            setError(errors);
          }
        )
      );
    }
  };

  const closeModal = () => {
    setOpenModal(false);
  };

  const confirmEmployee = () => {
    employee.vcsystems = null;
    sendEmployee(employee);
  };

  return (
    <div id="employee-form" data-test="employee-form">
      {(userLoginId == id || roleId == 1) && (
        <div>
          <Typography variant="h4" gutterBottom align="center">
            {id != null ? 'Edit Employee' : 'Add New Employee'}
          </Typography>
          <Stepper
            className={classes.stepper}
            data-test="tabs"
            nonLinear
            alternativeLabel
            activeStep={tab}
          >
            {roleId == 1 && (
              <Step>
                <StepButton
                  id="admin"
                  data-test="admin"
                  onClick={() => changeTab(0)}
                  completed={false}
                >
                  Admin Data
                </StepButton>
              </Step>
            )}
            <Step>
              <StepButton
                id="laboral"
                data-test="laboral"
                onClick={() => changeTab(1)}
                completed={false}
              >
                Laboral Data
              </StepButton>
            </Step>
          </Stepper>
          <AdminData
            data={employee}
            value={tab}
            index={0}
            changeIndex={nextTab}
          />
          <LaboralData
            data={employee}
            value={tab}
            index={roleId != 1 ? 0 : 1}
            changeIndex={prevTab}
            actionForm={signUpEmployee}
            setError={setError}
          />
          <ModalConfirm
            open={openModal}
            onClose={closeModal}
            confirm={confirmEmployee}
            text={
              'Are you sure you want to create the ' +
              'employee without video conference systems?'
            }
          />
        </div>
      )}
    </div>
  );
};

EmployeeForm.propTypes = {
  setError: PropTypes.func.isRequired,
  setSuccess: PropTypes.func.isRequired,
  id: PropTypes.number,
};

export default EmployeeForm;
