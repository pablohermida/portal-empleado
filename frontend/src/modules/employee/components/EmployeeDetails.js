import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import PropTypes from 'prop-types';
import {useStyles} from '../../../assets/styles/theme';
import * as dateUtils from '../../common/utils/dateUtils';

import * as actions from '../actions';
import * as selectors from '../selectors';

import {Grid, Typography, Paper, Tabs, Tab} from '@material-ui/core';
import {Link} from 'react-router-dom';

import MoreInfo from './details/MoreInfo';

const EmployeeDetails = (props) => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const roleId = useSelector(selectors.getLogin).roleId;
  const userLoginId = useSelector(selectors.getLogin).employeeId;

  const {setError, id} = props;

  React.useEffect(() => {
    dispatch(actions.clearEmployee());
    if (id != null) {
      dispatch(
        actions.getEmployeeById(id, (error) => {
          setError(error);
        })
      );
    }
  }, [id]);

  const employee = useSelector(selectors.getEmployee);

  const [tab, setTab] = React.useState(0);

  const changeTab = (e, newTab) => {
    setTab(newTab);
  };

  return (
    <div
      className={classes.root}
      id="employee-details"
      data-test="employee-details"
    >
      <Grid container alignItems="center">
        <Grid item xs={12} sm={4} md={2}>
          <img
            className={classes.imgProfile}
            src={
              employee.image ? employee.image : '/src/assets/img/user-icon.jpg'
            }
            alt="Employee"
            title={employee.username}
          />
        </Grid>
        <Grid item xs={12} sm={8} md={10}>
          <Grid container alignItems="center">
            <Grid item xs={12} sm={8} md={10} className={classes.spaceProfile}>
              <Typography
                component="h1"
                variant="h3"
                className={classes.titleProfile}
              >
                {employee.name + ' ' + employee.lastname}
              </Typography>
            </Grid>
            {(roleId == 1 || id == userLoginId) && (
              <Grid item xs={12} sm={4} md={2} className={classes.spaceProfile}>
                <Link
                  className={classes.formatLink}
                  to={`/employee/edit/${id}`}
                >
                  Edit Data
                </Link>
              </Grid>
            )}
          </Grid>
          <Grid container alignItems="center">
            <Grid item xs={12} sm={6} md={3} className={classes.spaceProfile}>
              {employee.username}
            </Grid>
            <Grid item xs={12} sm={6} md={3} className={classes.spaceProfile}>
              {employee.jobName}
            </Grid>
            <Grid item xs={12} sm={6} md={3} className={classes.spaceProfile}>
              <a
                className={classes.formatLink}
                href={`mailto:${employee.email}`}
              >
                {employee.email}
              </a>
            </Grid>
            <Grid item xs={12} sm={6} md={3} className={classes.spaceProfile}>
              <a className={classes.formatLink} href={`tel:${employee.phone}`}>
                {employee.phone}
              </a>
            </Grid>
            <Grid item xs={12} sm={6} md={3} className={classes.spaceProfile}>
              {dateUtils.formatDate(employee.dateBirth)}
              <i> {dateUtils.getBirth(employee.dateBirth)} years</i>
            </Grid>
            <Grid item xs={12} sm={6} md={3} className={classes.spaceProfile}>
              {dateUtils.formatDate(employee.dateStart)}
              <i> {dateUtils.getTimeIn(employee.dateStart)}</i>
            </Grid>
            {employee.vcsystems && (
              <Grid item xs={12} sm={6} md={3} className={classes.spaceProfile}>
                {employee.vcsystems.map((vcs, index) => (
                  <a
                    className={classes.formatLink}
                    key={index}
                    href={vcs.data}
                    rel="noreferrer"
                    target="_blank"
                  >
                    {vcs.nameVcsystem}
                  </a>
                ))}
              </Grid>
            )}
          </Grid>
        </Grid>
      </Grid>
      {(roleId == 1 || id == userLoginId) && (
        <Paper square>
          <Tabs
            value={tab}
            indicatorColor="secondary"
            textColor="secondary"
            onChange={changeTab}
            aria-label="Additional Info Employee"
          >
            <Tab label="More Info" />
          </Tabs>
          <MoreInfo value={tab} index={0} changeIndex={changeTab} />
        </Paper>
      )}
    </div>
  );
};

EmployeeDetails.propTypes = {
  setError: PropTypes.func.isRequired,
  id: PropTypes.number,
};

export default EmployeeDetails;
