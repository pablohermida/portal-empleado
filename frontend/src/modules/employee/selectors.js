const getModuleState = (state) => state.employee;

export const getEmployee = (state) => getModuleState(state).employee;
export const getCurrentEmployee = (state) => getModuleState(state).crtEmployee;
export const getRoles = (state) => getModuleState(state).roles;
export const getJobs = (state) => getModuleState(state).jobs;
export const getWorkDays = (state) => getModuleState(state).workDays;
export const getSupervisors = (state) => getModuleState(state).supervisors;
export const getLogin = (state) => getModuleState(state).login;
export const getEmployeeList = (state) => getModuleState(state).list;
export const getFilters = (state) => getModuleState(state).filters;
