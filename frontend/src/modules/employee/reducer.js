import {combineReducers} from 'redux';
import * as actionTypes from './actionTypes';

const initialEmployee = {
  roleId: 0,
  username: '',
  jobId: 0,
  duration: '',
  dateStart: '',
  workDay: 'Jornada Completa',
  addressWork: '',
  supervisorLogin: null,
  workHoursArray: [
    {
      id: 'monday',
      name: 'Monday',
      isActive: true,
      isContinuous: false,
      value: [5, 17, 20, 25],
    },
    {
      id: 'tuesday',
      name: 'Tuesday',
      isActive: true,
      isContinuous: false,
      value: [5, 17, 20, 25],
    },
    {
      id: 'wednesday',
      name: 'Wednesday',
      isActive: true,
      isContinuous: false,
      value: [5, 17, 20, 25],
    },
    {
      id: 'thursday',
      name: 'Thursday',
      isActive: true,
      isContinuous: false,
      value: [5, 17, 20, 25],
    },
    {
      id: 'friday',
      name: 'Friday',
      isActive: true,
      isContinuous: true,
      value: [5, 17],
    },
    {
      id: 'saturday',
      name: 'Saturday',
      isActive: false,
      value: [5, 17, 20, 25],
    },
  ],
  vcsystems: [],
};

const initialFilters = [
  {
    name: 'username',
    label: 'Username',
    type: 'string',
    value: '',
  },
  {
    name: 'name',
    label: 'Name',
    type: 'string',
    value: '',
  },
  {
    name: 'jobsId',
    label: 'Jobs',
    type: 'multivalue',
    options: [],
    value: [],
  },
  {
    name: 'roleId',
    label: 'Type',
    type: 'select',
    zeroValue: 'Selecciona un tipo',
    options: [],
    value: 0,
  },
  {
    name: 'email',
    label: 'Email',
    type: 'string',
    value: '',
  },
];

const initialState = {
  employee: initialEmployee,
  workDays: [
    {
      id: 1,
      name: 'Media Jornada',
    },
    {
      id: 2,
      name: 'Jornada Completa',
    },
  ],
  roles: [],
  jobs: [],
  supervisors: [],
  login: null,
  crtEmployee: initialEmployee,
  list: {},
  filters: initialFilters,
};

const employee = (state = initialState.employee, action) => {
  switch (action.type) {
    case actionTypes.CHANGE_EMPLOYEE:
      return {...state, [action.key]: action.value};
    case actionTypes.GET_BY_ID:
      return action.employee;
    case actionTypes.CLEAR_EMPLOYEE:
      return initialState.employee;
    default:
      return state;
  }
};

const workDays = (state = initialState.workDays, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

const roles = (state = initialState.roles, action) => {
  switch (action.type) {
    case actionTypes.GET_ROLES:
      return action.roles;
    default:
      return state;
  }
};

const jobs = (state = initialState.jobs, action) => {
  switch (action.type) {
    case actionTypes.GET_JOBS:
      return action.jobs;
    default:
      return state;
  }
};

const supervisors = (state = initialState.supervisors, action) => {
  switch (action.type) {
    case actionTypes.GET_BY_USERNAME:
      return action.employees;
    case actionTypes.CLEAR_SUPERVISORS:
      return state;
    default:
      return state;
  }
};

const login = (state = initialState.login, action) => {
  switch (action.type) {
    case actionTypes.LOGIN_COMPLETED:
      return action.login;
    case actionTypes.CLEAR_LOGIN:
      return initialState.login;
    default:
      return state;
  }
};

const crtEmployee = (state = initialState.crtEmployee, action) => {
  switch (action.type) {
    case actionTypes.CLEAR_EMPLOYEE:
      return initialState.crtEmployee;
    default:
      return state;
  }
};

const list = (state = initialState.list, action) => {
  switch (action.type) {
    case actionTypes.GET_BY_FILTERS:
      return action.list;
    default:
      return state;
  }
};

const filters = (state = initialState.filters, action) => {
  switch (action.type) {
    case actionTypes.CHANGE_FILTER:
      state[action.key] = action.value;
      return state;
    default:
      return state;
  }
};

const reducer = combineReducers({
  employee,
  workDays,
  roles,
  jobs,
  supervisors,
  login,
  crtEmployee,
  list,
  filters,
});

export default reducer;
