const getModuleState = (state) => state.videoconference;

export const getVideoconferenceSystem = (state) =>
  getModuleState(state).vcsystem;

export const getVideoconferenceSystems = (state) =>
  getModuleState(state).vcsystems;
