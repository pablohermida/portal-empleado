import * as actionTypes from './actionTypes';
import * as vcSystemService from '../../api/services/vcSystemService';

export const changeVideoconference = (key, value) => ({
  type: actionTypes.CHANGE_VIDEOCONFERENCE,
  key,
  value,
});

export const getVcSystemsCompleted = (vcsystems) => ({
  type: actionTypes.GET_VIDEOCONFERENCES,
  vcsystems,
});

export const getVcSystems = () => (dispatch) => {
  vcSystemService.getVideoconferenceSystems((response) => {
    dispatch(getVcSystemsCompleted(response));
  });
};

export const addVcSystem = (vcsystem, onSuccess, onError) => () => {
  vcSystemService.addVideoconference(
    vcsystem,
    (response) => {
      onSuccess(response);
    },
    (error) => {
      onError(error);
    }
  );
};
