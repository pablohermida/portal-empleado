import {combineReducers} from 'redux';
import * as actionTypes from './actionTypes';

const initialState = {
  vcsystem: {
    platform: '',
  },
  vcsystems: [],
};

const vcsystem = (state = initialState.vcsystem, action) => {
  switch (action.type) {
    case actionTypes.CHANGE_VIDEOCONFERENCE:
      return {...state, [action.key]: action.value};
    default:
      return state;
  }
};

const vcsystems = (state = initialState.vcsystems, action) => {
  switch (action.type) {
    case actionTypes.GET_VIDEOCONFERENCES:
      return action.vcsystems;
    default:
      return state;
  }
};

const reducer = combineReducers({
  vcsystem,
  vcsystems,
});

export default reducer;
