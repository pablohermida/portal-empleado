import * as selectors from './selectors';
import reducer from './reducer';
import * as actionTypes from './actionTypes';
import * as actions from './actions';

export {default as Selector} from './components/Selector';

export default {selectors, reducer, actionTypes, actions};
