import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import PropTypes from 'prop-types';
import {useStyles} from '../../../assets/styles/theme';

import * as actions from '../actions';
import * as selectors from '../selectors';
import * as selectorsEmployee from '../../employee/selectors';
import Add from './form/Add';

import {Grid, TextField, MenuItem} from '@material-ui/core';

const Selector = (props) => {
  const dispatch = useDispatch();
  const classes = useStyles();

  const {setError, setSuccess, value, changeVcSystem, position} = props;

  const vcsystems = useSelector(selectors.getVideoconferenceSystems);
  const roleId = useSelector(selectorsEmployee.getLogin).roleId;

  const selectVcSystem = (e) => {
    setVcSystemSelected(e.target.value);
    changeVcSystem(position, e.target.value);
  };

  React.useEffect(() => {
    dispatch(actions.getVcSystems());
  }, [dispatch]);

  const successAddVcs = (data, response) => {
    setSuccess(response);
    dispatch(actions.getVcSystems());
    setVcSystemSelected(data);
    changeVcSystem(position, data);
  };

  const [vcSystemSelected, setVcSystemSelected] = React.useState(value);

  return (
    <div id="selectorVcSystem" data-test="selectorVcSystem">
      <form className={classes.form} noValidate autoComplete="off">
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <TextField
              id={`vcsystem${position}`}
              select
              label="Videoconferences Systems"
              name="vcsystem"
              value={vcSystemSelected ? vcSystemSelected : 0}
              onChange={selectVcSystem}
              variant="outlined"
              required
            >
              <MenuItem value={0} selected>
                Selecciona un sistema
              </MenuItem>
              {roleId == 1 && (
                <MenuItem value={Number.MAX_VALUE}>Dar de alta nuevo</MenuItem>
              )}
              {vcsystems.length > 0 &&
                vcsystems.map((vcs) => (
                  <MenuItem key={vcs.id} value={vcs.id}>
                    {vcs.platform}
                  </MenuItem>
                ))}
            </TextField>
          </Grid>
          {vcSystemSelected === Number.MAX_VALUE && (
            <Grid item xs={12}>
              <Add setError={setError} setSuccess={successAddVcs} />
            </Grid>
          )}
        </Grid>
      </form>
    </div>
  );
};

Selector.propTypes = {
  setError: PropTypes.func.isRequired,
  setSuccess: PropTypes.func.isRequired,
  value: PropTypes.number.isRequired,
  changeVcSystem: PropTypes.func.isRequired,
  position: PropTypes.number.isRequired,
};

export default Selector;
