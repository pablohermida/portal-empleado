import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import PropTypes from 'prop-types';

import * as actions from '../../actions';
import * as selectors from '../../selectors';
import ValidateTextField from '../../../common/components/ValidateTextField';

import {Grid, TextField, Button} from '@material-ui/core';

const Add = (props) => {
  const dispatch = useDispatch();

  const {setError, setSuccess} = props;

  const vcsystem = useSelector(selectors.getVideoconferenceSystem);

  const changeVcSystem = (e) => {
    dispatch(actions.changeVideoconference([e.target.name], e.target.value));
  };

  const addVideoconference = () => {
    dispatch(
      actions.addVcSystem(
        vcsystem,
        (response) => {
          setSuccess(response.id, 'Add platform Succesfully!');
        },
        (errors) => {
          setError(errors);
        }
      )
    );
  };

  let errors = {
    platform: null,
  };

  const changeError = (name, error) => {
    errors = {...errors, [name]: error};
  };

  const checkErrors = () => {
    let isError = false;
    Object.entries(errors).forEach(([key, value]) => {
      if (value == null || value) {
        changeError(key, value);
        isError = true;
      }
    });
    if (!isError) {
      addVideoconference();
    }
  };

  return (
    <div id="addVcSystem" data-test="addVcSystem">
      <Grid container alignItems="center" spacing={2}>
        <Grid item xs={12} sm={6}>
          <ValidateTextField
            name="platform"
            value={vcsystem.platform}
            hasError={changeError}
            min={2}
            max={255}
            required
          >
            <TextField
              id="platform"
              name="platform"
              label="Platform"
              value={vcsystem.platform}
              onChange={changeVcSystem}
              type="text"
              variant="outlined"
              required
            />
          </ValidateTextField>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Button
            id="add"
            variant="contained"
            color="primary"
            onClick={checkErrors}
          >
            Add
          </Button>
        </Grid>
      </Grid>
    </div>
  );
};

Add.propTypes = {
  setError: PropTypes.func.isRequired,
  setSuccess: PropTypes.func.isRequired,
};

export default Add;
