import React from 'react';
import {Switch, Route} from 'react-router-dom';

import {Errors} from '../../common';
import Success from '../../common/components/Success';
import Login from '../../employee/components/Login';
import RouterEmployee from '../../employee/components/Router';

import AppGlobalComponents from './AppGlobalComponents';

import {useStyles} from '../../../assets/styles/theme';

const Body = () => {
  const classes = useStyles();
  const [formErrors, setFormErrors] = React.useState(null);
  const [success, setSuccess] = React.useState(null);

  // TODO: Create 404 page
  return (
    <div data-test="body" className={classes.body}>
      <Errors errors={formErrors} onClose={() => setFormErrors(null)} />
      <Success message={success} onClose={() => setSuccess(null)} />
      <AppGlobalComponents />
      <Switch>
        <Route exact path="/"></Route>
        <Route path="/login">
          <Login setError={setFormErrors} />
        </Route>
        <Route path="/employee">
          <RouterEmployee setError={setFormErrors} setSuccess={setSuccess} />
        </Route>
        <Route>
          <div>404 Not Found</div>
        </Route>
      </Switch>
    </div>
  );
};

export default Body;
