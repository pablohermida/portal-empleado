import React from 'react';
import PropTypes from 'prop-types';
import {useSelector} from 'react-redux';
import {useHistory} from 'react-router-dom';

import employee from '../../employee';

const MenuItems = require('../../common/MenuItems');

import {
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Divider,
  Typography,
} from '@material-ui/core';

import {useStyles} from '../../../assets/styles/theme';

const MenuApp = (props) => {
  const history = useHistory();
  const classes = useStyles();
  const {open, onClose} = props;

  const loginData = useSelector(employee.selectors.getLogin);

  const changeRoute = (route) => {
    history.push(route);
    onClose();
  };

  return (
    <div data-test="menu">
      <Drawer variant="persistent" open={open}>
        <List className={classes.mainMenu}>
          {MenuItems.content.map((blockMenu, i) => (
            <div key={i}>
              <Typography className={classes.menuCenter} variant="h6">
                {blockMenu.title}
              </Typography>
              {blockMenu.items.map(
                (menuItem, j) =>
                  menuItem.roles.find((role) => role == loginData.roleId) && (
                    <ListItem
                      id={menuItem.id}
                      onClick={() => changeRoute(menuItem.route)}
                      name={menuItem.route}
                      button
                      key={j}
                    >
                      <ListItemIcon>{menuItem.icon}</ListItemIcon>
                      <ListItemText primary={menuItem.title} />
                    </ListItem>
                  )
              )}
              <Divider />
            </div>
          ))}
        </List>
      </Drawer>
    </div>
  );
};

MenuApp.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default MenuApp;
