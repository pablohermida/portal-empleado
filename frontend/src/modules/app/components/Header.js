import React from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {Link, useHistory} from 'react-router-dom';

import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Menu,
  MenuItem,
  Grid,
  Button,
  Hidden,
} from '@material-ui/core';

import {useStyles} from '../../../assets/styles/theme';

import employee from '../../employee';

import {
  Menu as MenuIcon,
  AccountCircle,
  Close as CloseIcon,
} from '@material-ui/icons';
import MenuApp from './MenuApp';

const Header = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const classes = useStyles();
  const [openUserMenu, setOpenUserMenu] = React.useState(false);
  const [openMenu, setOpenMenu] = React.useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);

  React.useEffect(() => {
    dispatch(
      employee.actions.loginFromServiceToken(() => history.push('/login'))
    );
  }, [dispatch]);

  const loginData = useSelector(employee.selectors.getLogin);

  const logout = () => {
    dispatch(employee.actions.logout());
    history.push('/login');
  };

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
    setOpenUserMenu(!openUserMenu);
  };

  const handleMainMenu = () => {
    setOpenMenu(!openMenu);
  };

  return (
    <nav data-test="header" className={classes.header}>
      {loginData && (
        <div>
          <AppBar className={classes.appBar} position="fixed">
            <Toolbar>
              <Grid container alignItems="center">
                <Grid item xs={2} sm={1}>
                  <IconButton
                    edge="start"
                    color="inherit"
                    aria-label="menu"
                    id="main-menu"
                    onClick={handleMainMenu}
                  >
                    {openMenu ? <CloseIcon /> : <MenuIcon />}
                  </IconButton>
                </Grid>
                <Grid item xs={9} sm={4} md={3}>
                  <Typography variant="h6" className={classes.uppercase}>
                    <Link to="/" className={classes.title}>
                      Portal del Empleado
                    </Link>
                  </Typography>
                </Grid>
                <Grid
                  item
                  xs={1}
                  sm={7}
                  md={8}
                  onClick={handleMenu}
                  className={classes.menuRight}
                >
                  <Button
                    size="large"
                    color="inherit"
                    startIcon={<AccountCircle />}
                    id="user-menu"
                  >
                    <Hidden only="xs">{loginData.username}</Hidden>
                  </Button>
                  <Menu
                    getContentAnchorEl={null}
                    anchorEl={anchorEl}
                    anchorOrigin={{
                      vertical: 'bottom',
                      horizontal: 'right',
                    }}
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    keepMounted
                    open={openUserMenu}
                    onClose={handleMenu}
                  >
                    <MenuItem
                      onClick={() =>
                        history.push(
                          `/employee/details/${loginData.employeeId}`
                        )
                      }
                      id="view-profile-lnk"
                    >
                      View profile
                    </MenuItem>
                    <MenuItem
                      onClick={() =>
                        history.push(`/employee/edit/${loginData.employeeId}`)
                      }
                      id="edit-profile-lnk"
                    >
                      Edit data
                    </MenuItem>
                    <MenuItem id="logout" onClick={logout}>
                      Logout
                    </MenuItem>
                  </Menu>
                </Grid>
              </Grid>
            </Toolbar>
          </AppBar>
          <MenuApp open={openMenu} onClose={handleMainMenu} />
        </div>
      )}
    </nav>
  );
};

export default Header;
