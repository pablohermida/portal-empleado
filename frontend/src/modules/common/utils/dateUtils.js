export const formatDate = (dateString) => {
  const date = new Date(dateString);
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();

  return `${day < 10 ? `0${day}` : `${day}`}-${
    month < 10 ? `0${month}` : `${month}`
  }-${year}`;
};

export const getBirth = (dateBirthString) => {
  const date = new Date();
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();

  const dateBirth = new Date(dateBirthString);
  const dayBirth = dateBirth.getDate();
  const monthBirth = dateBirth.getMonth() + 1;
  const yearBirth = dateBirth.getFullYear();

  let yearsTotal = year - yearBirth;

  if (month < monthBirth || (month == monthBirth && dayBirth < day)) {
    yearsTotal--;
  }

  return yearsTotal;
};

export const getTimeIn = (dateStartString) => {
  const date = new Date();
  const dateStart = new Date(dateStartString);

  let result = '';

  const diffDates = date.getTime() - dateStart.getTime();
  const daysDiff = (diffDates / (1000 * 3600 * 24)).toFixed(0);
  const monthsDiff = (daysDiff / 30).toFixed(0);
  const yearsDiff = (monthsDiff / 12).toFixed(0);

  if (monthsDiff > 12) {
    result += `${yearsDiff} years `;
  }

  if (daysDiff > 30) {
    result += `${monthsDiff} months `;
  }

  if (daysDiff > 0) {
    result += `${daysDiff - monthsDiff * 30} days`;
  }

  return result;
};
