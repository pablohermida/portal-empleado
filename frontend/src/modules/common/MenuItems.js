import React from 'react';
import {
  AddCircle as AddCircleIcon,
  Group as GroupIcon,
} from '@material-ui/icons';

export const content = [
  {
    title: 'Employees',
    items: [
      {
        id: 'signup-item',
        title: 'New Employee',
        icon: <AddCircleIcon />,
        route: '/employee/signUp',
        roles: [1],
      },
      {
        id: 'employeeList-item',
        title: 'List Employees',
        icon: <GroupIcon />,
        route: '/employee/list',
        roles: [1, 2],
      },
    ],
  },
];
