import React from 'react';
import PropTypes from 'prop-types';

const ValidateTextField = (props) => {
  const {name, value, min, max, required, hasError, equalTo, children} = props;

  let error = '';

  if (value != null && value != 0) {
    if (required && value.toString().trim() === '') {
      error = name.charAt(0).toUpperCase() + name.slice(1) + ' is mandatory';
    } else if (min && value.length < min) {
      error = 'Size is smaller than ' + min;
    } else if (max && value.length > max) {
      error = 'Size is greater than ' + max;
    } else if (equalTo && equalTo !== value) {
      error = name + ' isn´t correct';
    } else {
      error = '';
    }
  } else if (required) {
    error = name.charAt(0).toUpperCase() + name.slice(1) + ' is mandatory';
  }

  const childrenWithProps = React.Children.map(children, (child) => {
    if (React.isValidElement(child) && error.trim() != '') {
      hasError(name, true);
      return React.cloneElement(child, {error: true, helperText: error});
    }
    hasError(name, false);
    return React.cloneElement(child, {error: false});
  });

  return <div data-test="children">{childrenWithProps}</div>;
};

ValidateTextField.defaultProps = {
  name: '',
  value: null,
  min: 0,
  max: 0,
  required: false,
  hasError: () => false,
  equalTo: null,
};

ValidateTextField.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.any,
  min: PropTypes.number,
  max: PropTypes.number,
  required: PropTypes.bool,
  hasError: PropTypes.func,
  equalTo: PropTypes.any,
  children: PropTypes.any,
};

export default ValidateTextField;
