import React from 'react';
import PropTypes from 'prop-types';

import {useStyles} from '../../../assets/styles/theme';

import {
  Grid,
  TextField,
  MenuItem,
  Button,
  FormGroup,
  FormControl,
  FormLabel,
  FormControlLabel,
  Checkbox,
} from '@material-ui/core';

const FilterList = (props) => {
  const {filters, changeValue, onFilter, open} = props;
  const classes = useStyles();

  return (
    open && (
      <div id="filter" data-test="filter" className={classes.filters}>
        <form
          className={classes.form}
          id="listFilterForm"
          noValidate
          autoComplete="off"
          aria-labelledby="filter-btn"
        >
          <Grid container alignItems="center" spacing={2}>
            {filters.map((filter, index) => (
              <Grid key={index} item xs={12} sm={6} md={3}>
                {filter.type === 'string' && (
                  <TextField
                    id={filter.name}
                    label={filter.label}
                    name={filter.name}
                    onChange={(e) => changeValue(index, e.target.value)}
                    type="text"
                    variant="outlined"
                  />
                )}
                {filter.type === 'select' && (
                  <TextField
                    id={filter.name}
                    label={filter.label}
                    name={filter.name}
                    defaultValue={0}
                    onChange={(e) => changeValue(index, e.target.value)}
                    select
                    variant="outlined"
                  >
                    <MenuItem key={0} value={0} selected>
                      {filter.zeroValue}
                    </MenuItem>
                    {filter.options.length > 0 &&
                      filter.options.map((opt) => (
                        <MenuItem key={opt.id} value={opt.id}>
                          {opt.name}
                        </MenuItem>
                      ))}
                  </TextField>
                )}
                {filter.type === 'multivalue' && (
                  <FormControl component="fieldset">
                    <FormLabel component="legend">{filter.label}</FormLabel>
                    <FormGroup>
                      {filter.options.length > 0 &&
                        filter.options.map((opt) => (
                          <FormControlLabel
                            key={opt.id}
                            control={
                              <Checkbox
                                checked={opt.checked}
                                onChange={(e) =>
                                  changeValue(
                                    index,
                                    e.target.value,
                                    e.target.checked
                                  )
                                }
                                name={opt.name}
                                value={opt.id}
                              />
                            }
                            label={opt.name}
                          />
                        ))}
                    </FormGroup>
                  </FormControl>
                )}
              </Grid>
            ))}
          </Grid>
          {filters.length > 0 && (
            <Grid container alignItems="center" spacing={5}>
              <Grid item xs={2}>
                <Button
                  id="filter-btn"
                  variant="contained"
                  color="primary"
                  type="submit"
                  onClick={(e) => {
                    e.preventDefault();
                    onFilter();
                  }}
                >
                  Filter
                </Button>
              </Grid>
            </Grid>
          )}
        </form>
      </div>
    )
  );
};

FilterList.propTypes = {
  filters: PropTypes.any.isRequired,
  changeValue: PropTypes.func.isRequired,
  onFilter: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};

export default FilterList;
