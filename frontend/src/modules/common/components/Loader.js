import React from 'react';
import PropTypes from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';

const Loader = ({loading}) =>
  loading && <CircularProgress data-test="loader" />;

Loader.propTypes = {
  loading: PropTypes.bool.isRequired,
};

export default Loader;
