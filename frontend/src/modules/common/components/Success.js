import React from 'react';
import PropTypes from 'prop-types';

import Alert from '@material-ui/lab/Alert';
import IconButton from '@material-ui/core/IconButton';
import Collapse from '@material-ui/core/Collapse';
import CloseIcon from '@material-ui/icons/Close';

const Success = ({message, onClose}) => {
  if (!message) {
    return null;
  }

  return (
    <div data-test="success">
      <Collapse in={message != null} timeout={600}>
        <Alert
          variant="filled"
          severity="success"
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() => {
                onClose();
              }}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
        >
          {message ? message : ''}
        </Alert>
      </Collapse>
    </div>
  );
};

Success.propTypes = {
  message: PropTypes.string,
  onClose: PropTypes.func.isRequired,
};

export default Success;
