/* eslint-disable operator-linebreak */
// FIXME: enable operator linebreak to ternary condition
import React from 'react';
import PropTypes from 'prop-types';
import {useIntl} from 'react-intl';

import {NetworkError} from '../../../api';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const ErrorDialog = ({error, onClose}) => {
  const [open, setOpen] = React.useState(true);

  const handleClose = () => {
    setOpen(false);
    onClose();
  };

  const intl = useIntl();

  if (error == null) {
    return null;
  }

  const message =
    error instanceof NetworkError
      ? intl.formatMessage({id: 'project.global.exceptions.NetworkError'})
      : error.message;

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      data-test="error-dialog"
    >
      <DialogTitle id="alert-dialog-title">
        {intl.formatMessage({
          id: 'project.common.ErrorDialog.title',
        })}
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {message}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary" autoFocus>
          {intl.formatMessage({
            id: 'project.global.buttons.close',
          })}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

ErrorDialog.propTypes = {
  error: PropTypes.object,
  onClose: PropTypes.func.isRequired,
};

export default ErrorDialog;
