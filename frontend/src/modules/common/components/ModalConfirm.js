/* eslint-disable operator-linebreak */
// FIXME: enable operator linebreak to ternary condition
import React from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const ModalConfirm = (props) => {
  const {open, confirm, text, onClose} = props;

  const handleConfirm = () => {
    onClose();
    confirm();
  };

  return (
    <Dialog
      open={open}
      onClose={onClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      data-test="confirm-dialog"
    >
      <DialogTitle id="alert-dialog-title">Confirm</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {text}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button id="close" onClick={onClose} color="primary">
          Close
        </Button>
        <Button id="confirm" onClick={handleConfirm} color="primary" autoFocus>
          Confirm
        </Button>
      </DialogActions>
    </Dialog>
  );
};

ModalConfirm.propTypes = {
  open: PropTypes.bool,
  confirm: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
};

export default ModalConfirm;
