import React from 'react';
import PropTypes from 'prop-types';
import {useIntl} from 'react-intl';

import Alert from '@material-ui/lab/Alert';
import IconButton from '@material-ui/core/IconButton';
import Collapse from '@material-ui/core/Collapse';
import CloseIcon from '@material-ui/icons/Close';

const Errors = ({errors, onClose}) => {
  const intl = useIntl();

  if (!errors) {
    return null;
  }

  let code;
  let fieldErrors;

  if (errors.code) {
    code = errors.code;
  } else if (errors.fieldErrors) {
    fieldErrors = [];
    errors.fieldErrors.forEach((e) => {
      const fieldName = intl.formatMessage({
        id: `project.global.fields.${e.fieldName}`,
      });
      fieldErrors.push(`${fieldName}: ${e.message}`);
    });
  }

  return (
    <div data-test="errors">
      <Collapse in={errors != null} timeout={600}>
        <Alert
          variant="filled"
          severity="error"
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() => {
                onClose();
              }}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
        >
          {code ? code : ''}

          {fieldErrors ? (
            <ul>
              {fieldErrors.map((fieldError, index) => (
                <li key={index}>{fieldError}</li>
              ))}
            </ul>
          ) : (
            ''
          )}
        </Alert>
      </Collapse>
    </div>
  );
};

Errors.propTypes = {
  errors: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  onClose: PropTypes.func.isRequired,
};

export default Errors;
