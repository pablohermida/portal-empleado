import {config, appFetch} from '../appFetch';

export const addVideoconference = (vcSystem, onSuccess, onErrors) =>
  appFetch(`/videoconference`, config('POST', vcSystem), onSuccess, onErrors);

export const getVideoconferenceSystems = (onSuccess) =>
  appFetch(`/videoconference`, config('GET'), onSuccess);
