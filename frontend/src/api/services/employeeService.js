import {
  config,
  appFetch,
  setServiceToken,
  setReauthenticationCallback,
  removeServiceToken,
} from '../appFetch';

export const login = (
  loginData,
  onSuccess,
  onErrors,
  reauthenticationCallback
) =>
  appFetch(
    `/employee/login`,
    config('POST', loginData),
    (authenticatedUser) => {
      setServiceToken(authenticatedUser.token);
      setReauthenticationCallback(reauthenticationCallback);
      onSuccess(authenticatedUser);
    },
    onErrors
  );

export const loginFromServiceToken = (
  onSuccess,
  onErrors,
  reauthenticationCallback
) =>
  appFetch(
    `/employee/loginFromServiceToken`,
    config('POST'),
    (authenticatedUser) => {
      setServiceToken(authenticatedUser.token);
      setReauthenticationCallback(reauthenticationCallback);
      onSuccess(authenticatedUser);
    },
    onErrors
  );

export const logout = () => removeServiceToken();

export const signUp = (employee, onSuccess, onErrors) =>
  appFetch(`/employee/signUp`, config('POST', employee), onSuccess, onErrors);

export const unsuscribe = (id, onSuccess, onErrors) =>
  appFetch(
    `/employee/unsuscribe/${id}`,
    config('POST', null),
    onSuccess,
    onErrors
  );

export const editEmployee = (id, employee, onSuccess, onErrors) =>
  appFetch(`/employee/${id}`, config('PUT', employee), onSuccess, onErrors);

export const getRoles = (onSuccess) =>
  appFetch(`/employee/roles`, config('GET'), onSuccess);

export const getJobs = (onSuccess) =>
  appFetch(`/employee/jobs`, config('GET'), onSuccess);

export const searchByUsername = (username, onSuccess) =>
  appFetch(`/employee/search/${username}`, config('GET'), onSuccess);

export const getById = (employeeId, onSuccess) =>
  appFetch(`/employee/${employeeId}`, config('GET'), onSuccess);

export const getByFilters = (filters, onSuccess) =>
  appFetch(`/employee/search${filters}`, config('GET'), onSuccess);
