import {init} from './appFetch';
import * as employeeService from './services/employeeService';

export {default as NetworkError} from './NetworkError';

export default {init, employeeService};
