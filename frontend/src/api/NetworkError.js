/* eslint-disable require-jsdoc */
class NetworkError extends Error {
  constructor() {
    super('Network error');
  }
}

export default NetworkError;
