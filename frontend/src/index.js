/* eslint-disable comma-dangle */
/* eslint-disable indent */
import React from 'react';
import ReactDOM from 'react-dom';

import 'core-js/stable';
import 'regenerator-runtime/runtime';

import {Provider} from 'react-redux';
import {IntlProvider} from 'react-intl';

import {ThemeProvider} from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import {theme} from './assets/styles/theme';

import configureStore from './store';
import {App} from './modules/app';
import * as actions from './modules/app/actions';
import api from './api';
import {NetworkError} from './api';
import {initReactIntl} from './i18n';

let worker;
let refreshing = false;

const store = configureStore();

api.init((error) => store.dispatch(actions.error(new NetworkError())));

const {locale, messages} = initReactIntl();

ReactDOM.render(
  <Provider store={store}>
    <IntlProvider locale={locale} messages={messages}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <App />
      </ThemeProvider>
    </IntlProvider>
  </Provider>,
  document.getElementById('root')
);

// document.getElementById('reload').addEventListener('click', () => {
//   worker.postMessage({action: 'skipWaiting'});
// });

// // Service Worker installation
// if ('serviceWorker' in navigator && process.env.NODE_ENV === 'production') {
//   window.addEventListener('load', () => {
//     navigator.serviceWorker.register('./serviceWorker.js').then(
//       (registration) => {
//         registration.addEventListener('updatefound', () => {
//           const worker = registration.installing;
//           worker.addEventListener('statechange', () => {
//             if (worker.state === 'installed') {
//               const updateApp = document.getElementById('updateApplication');
//               updateApp.classList.add('show');
//             }
//           });
//         });
//       },
//       (err) => {
//         console.error('Service worker failed', err);
//       }
//     );
//   });

//   navigator.serviceWorker.addEventListener('controllerchange', () => {
//     if (!refreshing) {
//       window.location.reload();
//       refreshing = true;
//     }
//   });
// }
