import {createMuiTheme, makeStyles} from '@material-ui/core/styles';
import cyan from '@material-ui/core/colors/cyan';
import brown from '@material-ui/core/colors/brown';

export const theme = createMuiTheme({
  palette: {
    primary: {
      main: cyan[800],
      light: '#4fb3bf',
      dark: '#005662',
    },
    secondary: {
      main: brown[400],
      light: '#be9c91',
      dark: '#5f4339',
    },
  },
});

export const useStyles = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(3),
  },
  formatLink: {
    color: theme.palette.secondary.main,
    textDecoration: 'none',
  },
  body: {
    marginTop: '70px',
  },
  header: {
    marginBottom: '100px',
  },
  title: {
    color: '#FFF',
    textDecoration: 'none',
  },
  form: {
    'margin': theme.spacing(5),
    'textAlign': 'center',
    'marginTop': theme.spacing(3),
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '100%',
    },
  },
  workHour: {
    minHeight: '500px',
  },
  login: {
    marginTop: '150px',
  },
  appBar: {
    zIndex: '9999999999!important',
  },
  userMenu: {
    width: '150px',
  },
  menuRight: {
    textAlign: 'right',
  },
  menuCenter: {
    textAlign: 'center',
  },
  mainMenu: {
    paddingTop: '70px!important',
    width: '200px',
  },
  uppercase: {
    textTransform: 'uppercase',
  },
  stepper: {
    backgroundColor: 'transparent!important',
    maxWidth: '500px',
    margin: 'auto',
  },
  dataGrid: {
    height: '550px',
    margin: theme.spacing(3),
    marginTop: '100px',
  },
  dataGridSvg: {
    '& .MuiSvgIcon-root': {
      display: 'block',
    },
  },
  dataGridActions: {
    '& .MuiSvgIcon-root': {
      margin: theme.spacing(1),
    },
  },
  filters: {
    marginBottom: '20px',
  },
  imgProfile: {
    maxWidth: '80%',
  },
  titleProfile: {
    textTransform: 'uppercase',
  },
  spaceProfile: {
    paddingBottom: '25px',
  },
}));
