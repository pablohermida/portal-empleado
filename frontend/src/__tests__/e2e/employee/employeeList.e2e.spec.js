describe('Employee List', () => {
  beforeEach(() => {
    cy.visit('/login');
    cy.get('#username').type('admin');
    cy.get('#password').type('portal');
    cy.get('#login-btn').click();
  });

  it('Check the Component', () => {
    cy.get('#main-menu').click();
    cy.get('#employeeList-item').click();
    cy.get('#list').should('have.attr', 'data-test', 'list');
    cy.get('.MuiDataGrid-viewport .MuiDataGrid-row')
      .its('length')
      .should('be.gte', 1);
    //TODO: Check view all components correctly
  });

  it('Filter List', () => {
    cy.get('#main-menu').click();
    cy.get('#employeeList-item').click();
    cy.get('#filter-show').click();
    cy.get('#filter').should('have.attr', 'data-test', 'filter');
    cy.get('#username').type('fail');
    cy.get('#filter-btn').click();
    cy.get('.MuiDataGrid-viewport .MuiDataGrid-row').should('not.exist');
    cy.get('#filter').should('not.exist');
  });

  it('Click Edit Employee', () => {
    cy.get('#main-menu').click();
    cy.get('#employeeList-item').click();
    cy.get('.MuiDataGrid-viewport .MuiDataGrid-row')
      .first()
      .find('[data-field="id"] a#edit-form-btn')
      .click();
    cy.url().should('eq', 'http://localhost:9999/employee/edit/1');
  });

  it('Click View Employee', () => {
    cy.get('#main-menu').click();
    cy.get('#employeeList-item').click();
    cy.get('.MuiDataGrid-viewport .MuiDataGrid-row')
      .first()
      .find('[data-field="id"] a#view-details-btn')
      .click();
    cy.url().should('eq', 'http://localhost:9999/employee/details/1');
  });

  it('Click Unsuscribe Employee', () => {
    cy.get('#main-menu').click();
    cy.get('#employeeList-item').click();
    cy.get('.MuiDataGrid-viewport .MuiDataGrid-row')
      .first()
      .find('[data-field="id"] svg#unsub-btn')
      .click();
    cy.get('#close').click();
    cy.get('.MuiDataGrid-viewport .MuiDataGrid-row')
      .first()
      .find('[data-field="id"] svg#unsub-btn')
      .click();
    cy.get('#confirm').click();

    cy.url().should('eq', 'http://localhost:9999/');
    cy.get('.MuiAlert-filledSuccess').should('be.visible');
    cy.get('.MuiAlert-filledSuccess .MuiAlert-action button').click();
    cy.get('#main-menu').click();
    cy.get('#employeeList-item').click();
    cy.get('.MuiDataGrid-viewport .MuiDataGrid-row')
      .its('length')
      .should('eq', 2);
  });
});
