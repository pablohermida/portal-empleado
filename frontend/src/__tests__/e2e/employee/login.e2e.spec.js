describe('Login', () => {
  beforeEach(() => {
    cy.visit('/login');
  });

  it('Check the Component', () => {
    cy.get('#login').should('have.attr', 'data-test', 'login');
    cy.get('.MuiCard-root .MuiCardContent-root .MuiTypography-root').should(
      'be.visible'
    );
  });

  it('Incorrect Username to Login', () => {
    cy.get('#username').type('error');
    cy.get('#password').type('portal');
    cy.get('#login-btn').click();
    cy.get('.MuiAlert-filledError').should('be.visible');
  });

  it('Incorrect Password to Login', () => {
    cy.get('#username').type('admin');
    cy.get('#password').type('errorr');
    cy.get('#login-btn').click();
    cy.get('.MuiAlert-filledError').should('be.visible');
  });

  it('Correct Login', () => {
    cy.get('#username').type('admin');
    cy.get('#password').type('portal');
    cy.get('#login-btn').click();
    cy.url().should('eq', 'http://localhost:9999/');
  });

  it('Correct Logout', () => {
    cy.get('#username').type('admin');
    cy.get('#password').type('portal');
    cy.get('#login-btn').click();
    cy.get('#user-menu').click();
    cy.get('#logout').click();
    cy.url().should('eq', 'http://localhost:9999/login');
  });
});
