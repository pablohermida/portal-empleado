describe('Employee Form ', () => {
  beforeEach(() => {
    cy.visit('/login');
    cy.get('#username').type('admin');
    cy.get('#password').type('portal');
    cy.get('#login-btn').click();
  });

  it('Check the Component', () => {
    cy.get('#main-menu').click();
    cy.get('#signup-item').click();
    cy.get('#employee-form').should('have.attr', 'data-test', 'employee-form');
    cy.get('.MuiStepper-root').should('be.visible');
    cy.get('.MuiStepper-root')
      .find('#admin .MuiStepLabel-labelContainer .MuiStepLabel-label')
      .contains('Admin Data');
    cy.get('.MuiStepper-root')
      .find('#laboral .MuiStepLabel-labelContainer .MuiStepLabel-label')
      .contains('Laboral Data');
    cy.get('#cancel').click();
    cy.url().should('eq', 'http://localhost:9999/');
  });

  it('Check Work Hours Selector', () => {
    cy.get('#main-menu').click();
    cy.get('#signup-item').click();
    cy.get('#openWorkHour').click();
    cy.get('.isActive input').first().click();
    cy.get('.MuiSlider-root')
      .first()
      .should('satisfy', hasAtLeastOneClass(['Mui-disabled']));
    cy.get('.MuiSlider-thumb').should('have.length', 22);
    cy.get('.isActive input').first().click();
    cy.get('.isContinuous input').first().click();
    cy.get('.MuiSlider-root')
      .first()
      .get('.MuiSlider-thumb')
      .first()
      .invoke('val', 5)
      .trigger('change');
    cy.get('.MuiSlider-root')
      .first()
      .get('.MuiSlider-thumb')
      .first()
      .should('have.attr', 'aria-valuenow', 5);
    cy.get('#close').click();
  });

  it('Incorrect SignUp', () => {
    cy.get('#main-menu').click();
    cy.get('#signup-item').click();
    cy.get('.MuiStepper-root').find('#laboral').click();
    cy.get('#save').click();
    cy.get('#back').click();

    cy.get('#role').click();
    cy.get('#menu-roleId ul').find('li').last().click();
    cy.get('#username').type('test');
    cy.get('#job').click();
    cy.get('#menu-jobId ul').find('li').last().click();
    cy.get('#duration').type('Indefinido');
    cy.get('#dateStart').type('2021-01-01');
    cy.get('#workDay').click();
    cy.get('#menu-workDay ul').find('li').last().click();
    cy.get('#work_address').type('test');
    cy.get('#next').click();
    cy.get('#password').type('test1234');
    cy.get('#repeatPassword').type('test1234');
    cy.get('#name').type('test');
    cy.get('#lastname').type('test');
    cy.get('#dateBirth').type('1994-12-12');
    cy.get('#email').type('test@test.es');
    cy.get('#iban').type('132456789123456789123456');
    cy.get('#dni').type('12345678A');
    cy.get('#address').type('testing address');
    cy.get('#phone').type('132456789');
    cy.get('#save').click();
    cy.get('#confirm').click();
    cy.get('.MuiAlert-filledError').should('be.visible');
  });

  it('Correct SignUp', () => {
    cy.get('#main-menu').click();
    cy.get('#signup-item').click();
    cy.get('#role').click();
    cy.get('#menu-roleId ul').find('li').last().click();
    cy.get('#username').type('test');
    cy.get('#job').click();
    cy.get('#menu-jobId ul').find('li').last().click();
    cy.get('#duration').type('Indefinido');
    cy.get('#dateStart').type('2021-01-01');
    cy.get('#workDay').click();
    cy.get('#menu-workDay ul').find('li').last().click();
    cy.get('#supervisor').type('t');
    cy.get('#work_address').type('test');
    cy.get('#next').click();
    cy.get('#password').type('test1234');
    cy.get('#repeatPassword').type('test1234');
    cy.get('#name').type('test');
    cy.get('#lastname').type('test');
    cy.get('#dateBirth').type('1994-12-12');
    cy.get('#email').type('test@test.es');
    cy.get('#iban').type('132456789123456789123456');
    cy.get('#dni').type('12345678Z');
    cy.get('#address').type('testing address');
    cy.get('#phone').type('132456789');
    cy.get('#save').click();
    cy.get('#close').click();
    cy.get('#save').click();
    cy.get('#confirm').click();
    cy.get('.MuiAlert-filledSuccess').should('be.visible');
    cy.url().should('eq', 'http://localhost:9999/');
  });

  it('Correct SignUp With Videoconference System', () => {
    cy.get('#main-menu').click();
    cy.get('#signup-item').click();
    cy.get('#role').click();
    cy.get('#menu-roleId ul').find('li').last().click();
    cy.get('#username').type('test2');
    cy.get('#job').click();
    cy.get('#menu-jobId ul').find('li').last().click();
    cy.get('#duration').type('Indefinido');
    cy.get('#dateStart').type('2021-01-01');
    cy.get('#workDay').click();
    cy.get('#menu-workDay ul').find('li').last().click();
    cy.get('#supervisor').type('t');
    cy.get('#work_address').type('test');
    cy.get('#next').click();
    cy.get('#password').type('test1234');
    cy.get('#repeatPassword').type('test1234');
    cy.get('#name').type('test');
    cy.get('#lastname').type('test');
    cy.get('#dateBirth').type('1994-12-12');
    cy.get('#email').type('test@test.es');
    cy.get('#iban').type('132456789123456789123456');
    cy.get('#dni').type('34630475L');
    cy.get('#address').type('testing address');
    cy.get('#phone').type('132456789');
    cy.get('#openVcSystems').click();
    cy.get('#other').click();
    cy.get('#vcsystem0').click();
    cy.get('#menu-vcsystem ul').find('li').last().click();
    cy.get('#data0').type('test');
    cy.get('#other').click();
    cy.get('#vcsystem1').click();
    cy.get('#menu-vcsystem ul')
      .find('li[data-value="' + Number.MAX_VALUE + '"]')
      .click();
    cy.get('#platform').type('vctest');
    cy.get('#add').click();
    cy.get('#data1').type('vctest');
    cy.get('#saveAndClose').click();
    cy.get('#save').click();
    cy.get('.MuiAlert-filledSuccess').should('be.visible');
    cy.url().should('eq', 'http://localhost:9999/');
  });

  it('Correct Edit Employee', () => {
    cy.get('#user-menu').click();
    cy.get('#edit-profile-lnk').click();
    cy.get('#next').click();
    cy.get('#password').clear().type('portal');
    cy.get('#repeatPassword').clear().type('portal');
    cy.get('#name').clear().type('edit');
    cy.get('#lastname').clear().type('edit');
    cy.get('#openVcSystems').click();
    cy.get('#other').click();
    cy.get('#vcsystem0').click();
    cy.get('#menu-vcsystem ul').find('li').last().click();
    cy.get('#data0').type('test');
    cy.get('#saveAndClose').click();
    cy.get('#save').click();
    cy.get('.MuiAlert-filledSuccess').should('be.visible');
    cy.url().should('eq', 'http://localhost:9999/');
  });

  it('Correct Edit Employee With Videoconference System', () => {
    cy.get('#user-menu').click();
    cy.get('#edit-profile-lnk').click();
    cy.get('#next').click();
    cy.get('#password').clear().type('portal');
    cy.get('#repeatPassword').clear().type('portal');
    cy.get('#openVcSystems').click();
    cy.get('#other').click();
    cy.get('#data0').clear().type('edit');
    cy.get('#other').click();
    cy.get('#vcsystem1').click();
    cy.get('#menu-vcsystem ul').find('li').last().click();
    cy.get('#data1').type('test');
    cy.get('#saveAndClose').click();
    cy.get('#save').click();
    cy.get('.MuiAlert-filledSuccess').should('be.visible');
    cy.url().should('eq', 'http://localhost:9999/');
  });
});

const hasAtLeastOneClass = (expectedClasses) => {
  return ($el) => {
    const classList = Array.from($el[0].classList);
    return expectedClasses.some((expectedClass) =>
      classList.includes(expectedClass)
    );
  };
};
