/* eslint-disable comma-dangle */
/* eslint-disable indent */
import {
  addVideoconference,
  getVideoconferenceSystems,
} from '../../../../api/services/vcSystemService';

global.fetch = jest.fn(() => Promise.resolve({}));

describe('VideoConference System Service Unit', () => {
  beforeEach(() => {
    jest.spyOn(global, 'fetch').mockClear();
  });

  it('Should add videconference system without errors', () => {
    const testOnSuccess = (response) => {
      expect(response).toBeDefined();
      expect(response.test).toBe('success');
    };
    const mockSuccessResponse = {test: 'success'};
    const mockJsonPromise = Promise.resolve(mockSuccessResponse);
    const mockFetchPromise = Promise.resolve({
      json: () => mockJsonPromise,
      headers: {
        get: jest.fn(() => {
          return 'application/json';
        }),
      },
      ok: true,
    });
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise);

    addVideoconference({}, testOnSuccess);
    expect(fetch).toHaveBeenCalled();
  });

  it('Should get all videoconference systems without errors', () => {
    const testOnSuccess = (response) => {
      expect(response).toBeDefined();
      expect(response.test).toBe('success');
    };
    const mockSuccessResponse = {test: 'success'};
    const mockJsonPromise = Promise.resolve(mockSuccessResponse);
    const mockFetchPromise = Promise.resolve({
      json: () => mockJsonPromise,
      headers: {
        get: jest.fn(() => {
          return 'application/json';
        }),
      },
      ok: true,
    });
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise);

    getVideoconferenceSystems(testOnSuccess);
    expect(fetch).toHaveBeenCalled();
  });
});
