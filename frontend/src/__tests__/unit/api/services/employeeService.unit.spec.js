/* eslint-disable comma-dangle */
/* eslint-disable indent */
import {
  signUp,
  getJobs,
  getRoles,
  searchByUsername,
} from '../../../../api/services/employeeService';

global.fetch = jest.fn(() => Promise.resolve({}));

describe('Employee Service Unit', () => {
  beforeEach(() => {
    jest.spyOn(global, 'fetch').mockClear();
  });

  it('Should sign up employee without errors', () => {
    const testOnSuccess = (response) => {
      expect(response).toBeDefined();
      expect(response.test).toBe('success');
    };
    const mockSuccessResponse = {test: 'success'};
    const mockJsonPromise = Promise.resolve(mockSuccessResponse);
    const mockFetchPromise = Promise.resolve({
      json: () => mockJsonPromise,
      headers: {
        get: jest.fn(() => {
          return 'application/json';
        }),
      },
      ok: true,
    });
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise);

    signUp({}, testOnSuccess);
    expect(fetch).toHaveBeenCalled();
  });

  it('Should get roles without errors', () => {
    const testOnSuccess = (response) => {
      expect(response).toBeDefined();
      expect(response.test).toBe('success');
    };
    const mockSuccessResponse = {test: 'success'};
    const mockJsonPromise = Promise.resolve(mockSuccessResponse);
    const mockFetchPromise = Promise.resolve({
      json: () => mockJsonPromise,
      headers: {
        get: jest.fn(() => {
          return 'application/json';
        }),
      },
      ok: true,
    });
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise);

    getRoles(testOnSuccess);
    expect(fetch).toHaveBeenCalled();
  });

  it('Should get jobs without errors', () => {
    const testOnSuccess = (response) => {
      expect(response).toBeDefined();
      expect(response.test).toBe('success');
    };
    const mockSuccessResponse = {test: 'success'};
    const mockJsonPromise = Promise.resolve(mockSuccessResponse);
    const mockFetchPromise = Promise.resolve({
      json: () => mockJsonPromise,
      headers: {
        get: jest.fn(() => {
          return 'application/json';
        }),
      },
      ok: true,
    });
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise);

    getJobs(testOnSuccess);
    expect(fetch).toHaveBeenCalled();
  });

  it('Should get employees by username without errors', () => {
    const testOnSuccess = (response) => {
      expect(response).toBeDefined();
      expect(response.test).toBe('success');
    };
    const mockSuccessResponse = {test: 'success'};
    const mockJsonPromise = Promise.resolve(mockSuccessResponse);
    const mockFetchPromise = Promise.resolve({
      json: () => mockJsonPromise,
      headers: {
        get: jest.fn(() => {
          return 'application/json';
        }),
      },
      ok: true,
    });
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise);

    searchByUsername('test', testOnSuccess);
    expect(fetch).toHaveBeenCalled();
  });
});
