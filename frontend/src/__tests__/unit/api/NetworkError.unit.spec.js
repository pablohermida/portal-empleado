import NetworkError from '../../../api/NetworkError';

describe('Network Error Unit', () => {
  it('Should return network error without errors', () => {
    const error = new NetworkError();
    expect(error.message).toBe('Network error');
  });
});
