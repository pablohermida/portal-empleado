/* eslint-disable comma-dangle */
/* eslint-disable indent */
import {
  config,
  setServiceToken,
  getServiceToken,
  removeServiceToken,
  setReauthenticationCallback,
  init,
  appFetch,
} from '../../../api/appFetch';

import NetworkError from '../../../api/NetworkError';

global.fetch = jest.fn(() => Promise.resolve({}));

describe('App Fetch Unit', () => {
  beforeEach(() => {
    jest.spyOn(global, 'fetch').mockClear();
  });
  it('Should return config without errors', () => {
    let configExpected = config('GET');
    expect(configExpected.method).toBe('GET');
    expect(configExpected.body).toBeUndefined();

    configExpected = config(null);
    expect(configExpected.method).toBeNull();

    configExpected = config(null, new FormData());
    expect(configExpected.body).toBeDefined();

    const testObject = {test: null};
    configExpected = config(null, testObject);
    expect(configExpected.body).toBe(JSON.stringify(testObject));
  });

  it('Should return response from app fetch without errors', () => {
    const testOnSuccess = (response) => {
      expect(response).toBeDefined();
      expect(response.test).toBe('success');
    };
    const mockSuccessResponse = {test: 'success'};
    const mockJsonPromise = Promise.resolve(mockSuccessResponse);
    const mockFetchPromise = Promise.resolve({
      json: () => mockJsonPromise,
      headers: {
        get: jest.fn(() => {
          return 'application/json';
        }),
      },
      ok: true,
    });
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise);

    appFetch('/test', config('GET'), testOnSuccess);
    expect(fetch).toHaveBeenCalled();
  });

  it('Should return response from app fetch no content without errors', () => {
    const testOnSuccess = () => {
      expect(fetch).toHaveBeenCalled();
    };
    const mockSuccessResponse = {};
    const mockJsonPromise = Promise.resolve(mockSuccessResponse);
    const mockFetchPromise = Promise.resolve({
      json: () => mockJsonPromise,
      headers: {
        get: jest.fn(() => {
          return 'application/json';
        }),
      },
      ok: true,
      status: 204,
    });
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise);

    appFetch('/test', config('GET'), testOnSuccess);
  });

  it('Should return response from app fetch success not defined', () => {
    const testOnError = () => {
      expect(fetch).toHaveBeenCalled();
    };
    const mockSuccessResponse = {test: 'error'};
    const mockJsonPromise = Promise.resolve(mockSuccessResponse);
    const mockFetchPromise = Promise.resolve({
      json: () => mockJsonPromise,
      headers: {
        get: jest.fn(() => {
          return 'application/json';
        }),
      },
      ok: true,
    });
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise);

    appFetch('/test', config('GET'), false, testOnError);
  });

  it('Should return response from app fetch error 500', () => {
    const testOnError = () => {
      expect(fetch).toHaveBeenCalled();
    };
    const mockSuccessResponse = {test: 'error'};
    const mockJsonPromise = Promise.resolve(mockSuccessResponse);
    const mockFetchPromise = Promise.resolve({
      json: () => mockJsonPromise,
      headers: {
        get: jest.fn(() => {
          return 'application/json';
        }),
      },
      status: 500,
    });
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise);
    try {
      appFetch('/test', config('GET'), false, testOnError);
    } catch (error) {
      expect(error.instance).toBe(NetworkError);
    }
  });

  it('Should return response from app fetch error 400 with code', () => {
    const testOnError = (error) => {
      expect(error.code).toBe('error');
    };
    const mockSuccessResponse = {code: 'error'};
    const mockJsonPromise = Promise.resolve(mockSuccessResponse);
    const mockFetchPromise = Promise.resolve({
      json: () => mockJsonPromise,
      headers: {
        get: jest.fn(() => {
          return 'application/json';
        }),
      },
      status: 400,
    });
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise);

    appFetch('/test', config('GET'), false, testOnError);
  });

  it('Should return response from app fetch error 400 with fieldsError', () => {
    const testOnError = (error) => {
      expect(error.fieldsErrors).toBe('error');
    };
    const mockSuccessResponse = {fieldsErrors: 'error'};
    const mockJsonPromise = Promise.resolve(mockSuccessResponse);
    const mockFetchPromise = Promise.resolve({
      json: () => mockJsonPromise,
      headers: {
        get: jest.fn(() => {
          return 'application/json';
        }),
      },
      status: 400,
    });
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise);

    appFetch('/test', config('GET'), false, testOnError);
  });

  it('Should return response from app fetch error 400 empty', () => {
    const testOnError = (error) => {
      const expected = new NetworkError();
      expect(fetch).toHaveBeenCalled();
      expect(expected.message).toMatch(error.message);
    };
    const mockSuccessResponse = {test: 'error'};
    const mockJsonPromise = Promise.resolve(mockSuccessResponse);
    const mockFetchPromise = Promise.resolve({
      json: () => mockJsonPromise,
      headers: {
        get: jest.fn(() => {
          return 'application/text';
        }),
      },
      status: 400,
    });
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise);

    appFetch('/test', config('GET'), false, testOnError);
  });

  it('Should load reauthentication callback', () => {
    setReauthenticationCallback(jest.fn());

    const testOnError = (error) => {
      expect(error.fieldsErrors).toBe('error');
    };
    const mockSuccessResponse = {fieldsErrors: 'error'};
    const mockJsonPromise = Promise.resolve(mockSuccessResponse);
    const mockFetchPromise = Promise.resolve({
      json: () => mockJsonPromise,
      headers: {
        get: jest.fn(() => {
          return 'application/json';
        }),
      },
      status: 401,
    });
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise);

    appFetch('/test', config('GET'), false, testOnError);
  });

  it('Should load network error callback', () => {
    init(jest.fn());

    const testOnError = () => {
      expect(fetch).toHaveBeenCalled();
    };
    const mockSuccessResponse = {test: 'error'};
    const mockJsonPromise = Promise.resolve(mockSuccessResponse);
    const mockFetchPromise = Promise.resolve({
      json: () => mockJsonPromise,
      headers: {
        get: jest.fn(() => {
          return 'application/json';
        }),
      },
      status: 500,
    });
    jest.spyOn(global, 'fetch').mockImplementation(() => mockFetchPromise);
    try {
      appFetch('/test', config('GET'), false, testOnError);
    } catch (error) {
      expect(error.instance).toBe(NetworkError);
    }
  });
});
