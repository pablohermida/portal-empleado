import {createStore} from 'redux';

import reducer from '../../../../modules/videoconference/reducer';
import vcsystem from '../../../../modules/videoconference';

let reducerApp;

describe('Actions Videoconference System Unit', () => {
  beforeEach(() => {
    reducerApp = createStore(reducer);
  });

  it('should change videoconference without errors', () => {
    reducerApp.dispatch(
      vcsystem.actions.changeVideoconference('platform', 'test')
    );

    expect(reducerApp.getState().vcsystem.platform).toEqual('test');
  });

  it('should get videoconference systems completed without errors', () => {
    const state = [{id: 1, platform: 'test'}];
    reducerApp.dispatch(vcsystem.actions.getVcSystemsCompleted(state));

    expect(reducerApp.getState().vcsystems).toEqual(state);
  });
});
