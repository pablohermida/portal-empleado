import React from 'react';
import '../../../../../setupTests';

import {shallow} from 'enzyme';
import {useSelector} from 'react-redux';

import {findByTestAtrr} from '../../../../../utils/test';
import Selector from '../../../../../modules/videoconference/components/Selector';

import {TextField} from '@material-ui/core';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

const setUp = (props = {}) => {
  useSelector.mockImplementation(() => []);
  const component = shallow(<Selector {...props} />);
  return component;
};

describe('Selector Videoconference Unit', () => {
  let component;
  beforeEach(() => {
    component = setUp({
      setSuccess: jest.fn(),
      setError: jest.fn(),
      changeVcSystem: jest.fn(),
      value: 0,
      position: 0,
    });
  });

  it('Should render without errors', () => {
    const wrapper = findByTestAtrr(component, 'selectorVcSystem');
    expect(wrapper.length).toBe(1);

    expect(component.find(TextField)).toHaveLength(1);
  });
});
