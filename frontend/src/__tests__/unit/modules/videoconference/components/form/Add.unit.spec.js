import React from 'react';
import '../../../../../../setupTests';

import {shallow} from 'enzyme';
import {useSelector} from 'react-redux';

import {findByTestAtrr} from '../../../../../../utils/test';
import Add from '../../../../../../modules/videoconference/components/form/Add';
import {TextField, Button} from '@material-ui/core';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

const setUp = (props = {}) => {
  useSelector.mockImplementation(() => []);
  const component = shallow(<Add {...props} />);
  return component;
};

describe('Add Videoconference Unit', () => {
  let component;
  beforeEach(() => {
    component = setUp({
      setSuccess: jest.fn(),
      setError: jest.fn(),
    });
  });

  it('Should render without errors', () => {
    const wrapper = findByTestAtrr(component, 'addVcSystem');
    expect(wrapper.length).toBe(1);

    expect(component.find(TextField)).toHaveLength(1);
    expect(component.find(Button)).toHaveLength(1);
  });
});
