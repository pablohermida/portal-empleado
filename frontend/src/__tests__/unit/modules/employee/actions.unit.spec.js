import {createStore} from 'redux';

import reducer from '../../../../modules/employee/reducer';
import employee from '../../../../modules/employee';

let reducerApp;

describe('Actions Employee Unit', () => {
  beforeEach(() => {
    reducerApp = createStore(reducer);
  });

  it('should changeEmployee without errors', () => {
    reducerApp.dispatch(employee.actions.changeEmployee('name', 'test'));

    expect(reducerApp.getState().employee.name).toEqual('test');
  });

  it('should get roles completed without errors', () => {
    const state = [{id: 1, name: 'test'}];
    reducerApp.dispatch(employee.actions.getRolesCompleted(state));

    expect(reducerApp.getState().roles).toEqual(state);
  });

  it('should get jobs completed without errors', () => {
    const state = [{id: 1, name: 'test'}];
    reducerApp.dispatch(employee.actions.getJobsCompleted(state));

    expect(reducerApp.getState().jobs).toEqual(state);
  });

  it('should search by username completed without errors', () => {
    const state = [{id: 1, name: 'test'}];
    reducerApp.dispatch(employee.actions.searchByUsernameCompleted(state));

    expect(reducerApp.getState().supervisors).toEqual(state);
  });

  it('should clear supervisors without errors', () => {
    reducerApp.dispatch(employee.actions.clearSupervisors());

    expect(reducerApp.getState().supervisors).toEqual([]);
  });

  it('should login without errors', () => {
    const state = 'test';
    reducerApp.dispatch(employee.actions.loginCompleted(state));

    expect(reducerApp.getState().login).toEqual(state);
  });

  it('should logout without errors', () => {
    reducerApp.dispatch(employee.actions.logout());

    expect(reducerApp.getState().login).toBeNull();
  });

  it('should get id without errors', () => {
    const state = {
      employeeId: 1,
      username: 'test',
      roleId: 1,
    };
    reducerApp.dispatch(employee.actions.loginCompleted(state));

    expect(reducerApp.getState().login).toEqual(state);
  });

  it('should get employee without errors', () => {
    const state = {test: 'test'};
    reducerApp.dispatch(employee.actions.getEmployeeByIdCompleted(state));

    expect(reducerApp.getState().employee).toEqual(state);
  });

  it('should get employees list without errors', () => {
    const state = {test: 'test'};
    reducerApp.dispatch(employee.actions.getEmployeesByFiltersCompleted(state));

    expect(reducerApp.getState().list).toEqual(state);
  });
});
