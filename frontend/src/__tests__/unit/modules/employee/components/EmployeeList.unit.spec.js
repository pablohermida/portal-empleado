import React from 'react';
import '../../../../../setupTests';

import {shallow} from 'enzyme';
import {useSelector} from 'react-redux';

import {findByTestAtrr} from '../../../../../utils/test';
import EmployeeList from '../../../../../modules/employee/components/EmployeeList';
import FilterList from '../../../../../modules/common/components/FilterList';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

const setUp = (props = {}) => {
  useSelector.mockImplementation(() => []);
  const component = shallow(<EmployeeList {...props} />);
  return component;
};

describe('Employee List Unit', () => {
  let component;
  beforeEach(() => {
    component = setUp({});
  });

  it('Should render without errors', () => {
    const wrapper = findByTestAtrr(component, 'list');
    expect(wrapper.length).toBe(1);

    expect(component.find(FilterList)).toHaveLength(1);
  });
});
