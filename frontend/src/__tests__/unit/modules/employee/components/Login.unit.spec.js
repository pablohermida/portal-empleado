import React from 'react';
import '../../../../../setupTests';

import {shallow} from 'enzyme';
import {useSelector} from 'react-redux';

import {findByTestAtrr} from '../../../../../utils/test';
import Login from '../../../../../modules/employee/components/Login';
import {Card, CardContent, Button} from '@material-ui/core';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

const setUp = (props = {}) => {
  useSelector.mockImplementation(() => []);
  const component = shallow(<Login {...props} />);
  return component;
};

describe('Login Unit', () => {
  let component;
  beforeEach(() => {
    component = setUp({
      setError: jest.fn(),
    });
  });

  it('Should render without errors', () => {
    const wrapper = findByTestAtrr(component, 'login');
    expect(wrapper.length).toBe(1);

    expect(component.find(Card)).toHaveLength(1);
    expect(component.find(CardContent)).toHaveLength(1);
    expect(component.find(Button)).toHaveLength(1);
  });
});
