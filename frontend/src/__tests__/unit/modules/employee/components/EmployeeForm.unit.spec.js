import React from 'react';
import '../../../../../setupTests';

import {shallow} from 'enzyme';
import {useSelector} from 'react-redux';

import {findByTestAtrr} from '../../../../../utils/test';
import EmployeeForm from '../../../../../modules/employee/components/EmployeeForm';
import {Stepper, Step} from '@material-ui/core';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

const setUp = (props = {}) => {
  useSelector.mockImplementation(() => []);
  const component = shallow(<EmployeeForm {...props} />);
  return component;
};

describe('Employee Form Unit', () => {
  let component;
  beforeEach(() => {
    component = setUp({
      setSuccess: jest.fn(),
      setError: jest.fn(),
      id: null,
    });
  });

  it('Should render without errors', () => {
    const wrapper = findByTestAtrr(component, 'employee-form');
    expect(wrapper.length).toBe(1);

    expect(component.find(Stepper)).toHaveLength(1);
    expect(component.find(Step)).toHaveLength(1);
  });
});
