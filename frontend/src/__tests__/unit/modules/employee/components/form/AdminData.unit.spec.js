/* eslint-disable max-len */
/* FIXME: Separate imports too long */
import React from 'react';
import '../../../../../../setupTests';

import {shallow} from 'enzyme';
import {useSelector, useDispatch} from 'react-redux';

import {findByTestAtrr} from '../../../../../../utils/test';
import AdminData from '../../../../../../modules/employee/components/form/AdminData';
import {Button, TextField} from '@material-ui/core';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

const setUp = (props = {}) => {
  useSelector.mockImplementation(() => []);
  useDispatch.mockImplementation(() => []);
  const component = shallow(<AdminData {...props} />);
  return component;
};

describe('Admin Form Unit', () => {
  let component;
  beforeEach(() => {
    component = setUp({
      index: 0,
      value: 0,
      data: {},
      changeIndex: jest.fn(),
    });
  });

  it('Should render without errors', () => {
    const wrapper = findByTestAtrr(component, 'admin-data');
    expect(wrapper.length).toBe(1);
  });

  it('Should no render', () => {
    component = setUp({
      index: 0,
      value: 1,
      data: {},
      changeIndex: jest.fn(),
    });

    const wrapper = findByTestAtrr(component, 'admin-data');
    expect(wrapper.length).toBe(1);
    expect(wrapper.find(TextField)).toHaveLength(0);
  });
});
