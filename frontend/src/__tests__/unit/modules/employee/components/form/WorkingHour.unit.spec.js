/* eslint-disable comma-dangle */
/* eslint-disable indent */
import React from 'react';
import '../../../../../../setupTests';

import {shallow} from 'enzyme';
import {useSelector} from 'react-redux';

import {findByTestAtrr} from '../../../../../../utils/test';
import WorkingHour from '../../../../../../modules/employee/components/form/WorkingHour';
import {
  Button,
  FormControlLabel,
  Dialog,
  Slider,
  Typography,
} from '@material-ui/core';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
}));

const setUp = (props = {}) => {
  useSelector.mockImplementation(() => []);
  const component = shallow(<WorkingHour {...props} />);
  return component;
};

describe('Working Hour Unit', () => {
  let component;
  beforeEach(() => {
    component = setUp({
      workHour: [
        {
          id: 1,
        },
      ],
    });
  });

  it('Should render without errors', () => {
    const wrapper = findByTestAtrr(component, 'working-hour');
    expect(wrapper.length).toBe(1);
    expect(wrapper.find(Button)).toHaveLength(2);
    expect(wrapper.find(Dialog)).toHaveLength(1);
    expect(wrapper.find(FormControlLabel)).toHaveLength(2);
    expect(wrapper.find(Slider)).toHaveLength(1);
  });

  it('Should no render', () => {
    component = setUp({
      workHour: [],
    });
    const wrapper = findByTestAtrr(component, 'working-hour');
    expect(wrapper.length).toBe(1);
    expect(wrapper.find(Button)).toHaveLength(0);
  });

  it('Should render with data correctly', () => {
    const workHourTest = [
      {
        id: 1,
        name: 'test 1',
        isActive: true,
        isContinuous: true,
      },
      {
        id: 2,
        name: 'test 2',
        isActive: false,
        isContinuous: false,
      },
    ];
    component = setUp({
      workHour: workHourTest,
    });
    const wrapper = findByTestAtrr(component, 'working-hour');
    expect(wrapper.length).toBe(1);

    expect(wrapper.find(Typography)).toHaveLength(workHourTest.length);

    const titles = wrapper.find(Typography);
    titles.forEach((title, i) => {
      expect(title.text()).toBe(workHourTest[i].name);
    });

    const controlLabels = wrapper.find(FormControlLabel);
    controlLabels.forEach((control, i) => {
      const checkbox = shallow(control.props().control);
      expect(checkbox.props().name).toBe(workHourTest[i > 1 ? 1 : 0].id);
      if (i % 2 == 0) {
        expect(checkbox.props().checked).toBe(
          workHourTest[i > 1 ? 1 : 0].isActive
        );
      } else {
        expect(checkbox.props().checked).toBe(
          workHourTest[i > 1 ? 1 : 0].isContinuous
        );
      }
    });
  });
});
