/* eslint-disable comma-dangle */
/* eslint-disable indent */
import React from 'react';
import '../../../../../../setupTests';

import {shallow} from 'enzyme';
import {useSelector, useDispatch} from 'react-redux';

import {findByTestAtrr} from '../../../../../../utils/test';
import VideoConferenceEmployee from '../../../../../../modules/employee/components/form/VideoConferenceEmployee';
import {Button, Dialog} from '@material-ui/core';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

const setUp = (props = {}) => {
  useSelector.mockImplementation(() => []);
  const component = shallow(<VideoConferenceEmployee {...props} />);
  return component;
};

describe('VideoConference Employee Unit', () => {
  let component;
  beforeEach(() => {
    component = setUp({
      setError: jest.fn(),
    });
  });

  it('Should render without errors', () => {
    const wrapper = findByTestAtrr(component, 'vcsystems');
    expect(wrapper.length).toBe(1);
    expect(wrapper.find(Button)).toHaveLength(3);
    expect(wrapper.find(Dialog)).toHaveLength(1);
  });
});
