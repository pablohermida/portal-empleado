/* eslint-disable max-len */
/* FIXME: Separate imports too long */
import React from 'react';
import '../../../../../../setupTests';

import {shallow} from 'enzyme';
import {useSelector, useDispatch} from 'react-redux';

import {findByTestAtrr} from '../../../../../../utils/test';
import LaboralData from '../../../../../../modules/employee/components/form/LaboralData';
import {Button, TextField} from '@material-ui/core';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

const setUp = (props = {}) => {
  useSelector.mockImplementation(() => []);
  useDispatch.mockImplementation(() => []);
  const component = shallow(<LaboralData {...props} />);
  return component;
};

describe('Laboral Form Unit', () => {
  let component;
  beforeEach(() => {
    component = setUp({
      index: 1,
      value: 1,
      data: {},
      changeIndex: jest.fn(),
      actionForm: jest.fn(),
      setError: jest.fn(),
    });
  });

  it('Should render without errors', () => {
    const wrapper = findByTestAtrr(component, 'laboral-data');
    expect(wrapper.length).toBe(1);
    expect(wrapper.find(TextField)).toHaveLength(10);
    expect(wrapper.find(Button)).toHaveLength(2);
  });

  it('Should no render', () => {
    component = setUp({
      index: 1,
      value: 0,
      data: {},
      changeIndex: jest.fn(),
      actionForm: jest.fn(),
      setError: jest.fn(),
    });

    const wrapper = findByTestAtrr(component, 'laboral-data');
    expect(wrapper.length).toBe(1);
    expect(wrapper.find(TextField)).toHaveLength(0);
  });
});
