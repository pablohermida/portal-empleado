import {createStore} from 'redux';

import reducer from '../../../../modules/employee/reducer';

let reducerApp;

describe('Reducer Employee Unit', () => {
  beforeEach(() => {
    reducerApp = createStore(reducer);
  });

  it('should load state default without errors', () => {
    const action = {type: null};
    const initialState = reducerApp.getState();

    reducerApp.dispatch(action);

    expect(reducerApp.getState()).toEqual(initialState);
  });
});
