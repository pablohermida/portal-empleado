import {createStore} from 'redux';

import reducer from '../../../../modules/app/reducer';
import * as actionTypes from '../../../../modules/app/actionTypes';

let reducerApp;

describe('Reducer App Unit', () => {
  beforeEach(() => {
    reducerApp = createStore(reducer);
  });

  it('should load state default without errors', () => {
    const action = {type: null};
    const initialState = reducerApp.getState();

    reducerApp.dispatch(action);

    expect(reducerApp.getState()).toEqual(initialState);
  });

  it('should load state error without errors', () => {
    const action = {type: actionTypes.ERROR, error: 'test'};
    const initialState = reducerApp.getState();

    reducerApp.dispatch(action);

    initialState.loading = false;
    initialState.error = 'test';

    expect(reducerApp.getState()).toEqual(initialState);
  });

  it('should load state loading without errors', () => {
    const action = {type: actionTypes.LOADING};
    const initialState = reducerApp.getState();

    reducerApp.dispatch(action);

    initialState.loading = true;

    expect(reducerApp.getState()).toEqual(initialState);
  });

  it('should load state loaded without errors', () => {
    const action = {type: actionTypes.LOADED};
    const initialState = reducerApp.getState();

    reducerApp.dispatch(action);

    initialState.loading = false;

    expect(reducerApp.getState()).toEqual(initialState);
  });
});
