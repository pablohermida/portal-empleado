import React from 'react';
import '../../../../../setupTests';

import {shallow} from 'enzyme';

import {findByTestAtrr} from '../../../../../utils/test';

import Footer from '../../../../../modules/app/components/Footer';

const setUp = (props = {}) => {
  const component = shallow(<Footer {...props} />);
  return component;
};

describe('Footer Unit', () => {
  let component;
  beforeEach(() => {
    component = setUp();
  });

  it('Should render without errors', () => {
    const wrapper = findByTestAtrr(component, 'footer');
    expect(wrapper.length).toBe(1);
  });
});
