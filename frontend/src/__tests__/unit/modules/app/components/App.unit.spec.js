import React from 'react';
import '../../../../../setupTests';

import {shallow} from 'enzyme';

import {findByTestAtrr} from '../../../../../utils/test';

import App from '../../../../../modules/app/components/App';
import Header from '../../../../../modules/app/components/Header';
import Body from '../../../../../modules/app/components/Body';
import Footer from '../../../../../modules/app/components/Footer';
import {BrowserRouter as Router} from 'react-router-dom';

const setUp = (props = {}) => {
  const component = shallow(<App {...props} />);
  return component;
};

describe('App Unit', () => {
  let component;
  beforeEach(() => {
    component = setUp();
  });

  it('Should render without errors', () => {
    const wrapper = findByTestAtrr(component, 'app');
    expect(wrapper.length).toBe(1);

    expect(component.find(Router)).toHaveLength(1);
    expect(component.find(Router).find(Header)).toHaveLength(1);
    expect(component.find(Router).find(Body)).toHaveLength(1);
    expect(component.find(Footer)).toHaveLength(1);
  });
});
