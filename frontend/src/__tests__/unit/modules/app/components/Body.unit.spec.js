import React from 'react';
import '../../../../../setupTests';

import {shallow} from 'enzyme';

import {findByTestAtrr} from '../../../../../utils/test';

import AppGlobalComponents from '../../../../../modules/app/components/AppGlobalComponents';
import {Switch} from 'react-router-dom';
import Body from '../../../../../modules/app/components/Body';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

const setUp = (props = {}) => {
  const component = shallow(<Body {...props} />);
  return component;
};

describe('Body Unit', () => {
  let component;
  beforeEach(() => {
    component = setUp();
  });

  it('Should render without errors', () => {
    const wrapper = findByTestAtrr(component, 'body');
    expect(wrapper.length).toBe(1);

    expect(component.find(AppGlobalComponents)).toHaveLength(1);
    expect(component.find(Switch)).toHaveLength(1);
  });
});
