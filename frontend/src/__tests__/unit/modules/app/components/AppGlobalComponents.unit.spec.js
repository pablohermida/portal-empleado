import React from 'react';
import '../../../../../setupTests';

import {shallow} from 'enzyme';
import {useSelector, useDispatch} from 'react-redux';

import {findByTestAtrr} from '../../../../../utils/test';

import AppGlobalComponents, {
  ConnectedErrorDialog,
  ConnectedLoader,
} from '../../../../../modules/app/components/AppGlobalComponents';
import {ErrorDialog, Loader} from '../../../../../modules/common';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(() => {}),
}));

const setUp = (props = {}) => {
  useSelector.mockImplementation(() => {});
  useDispatch.mockReturnValue(jest.fn());
  const component = shallow(<AppGlobalComponents {...props} />);
  return component;
};

describe('App Global Components Unit', () => {
  let component;
  beforeEach(() => {
    component = setUp();
  });

  it('Should render without errors', () => {
    const wrapper = findByTestAtrr(component, 'app-global-components');
    expect(wrapper.length).toBe(1);

    expect(wrapper.find(ConnectedErrorDialog)).toHaveLength(1);
    expect(wrapper.find(ConnectedLoader)).toHaveLength(1);
  });

  it('Should render Connected Error Dialog', () => {
    const wrapper = shallow(<ConnectedErrorDialog />);
    expect(wrapper.find(ErrorDialog)).toHaveLength(1);
  });

  it('Should render Connected Loader', () => {
    useSelector.mockImplementation(() => true);
    const wrapper = shallow(<ConnectedLoader />);
    expect(wrapper.find(Loader)).toHaveLength(1);
  });
});
