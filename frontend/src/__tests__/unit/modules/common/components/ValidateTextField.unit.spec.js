import React from 'react';
import '../../../../../setupTests';

import {shallow} from 'enzyme';

import {findByTestAtrr} from '../../../../../utils/test';
// eslint-disable-next-line max-len
import ValidateTextField from '../../../../../modules/common/components/ValidateTextField';

const setUp = (props = {}) => {
  const component = shallow(<ValidateTextField {...props} />);
  return component;
};

describe('Validate Text Field Unit', () => {
  let component;
  beforeEach(() => {
    const props = {
      name: 'test',
      value: 'test',
      min: 0,
      max: 10,
      required: true,
      hasError: jest.fn(),
      equalTo: null,
      children: [<div id="testChild" key="test" />],
    };
    component = setUp(props);
  });

  it('Should render without errors', () => {
    const wrapper = findByTestAtrr(component, 'children');
    expect(wrapper.length).toBe(1);
    expect(wrapper.find('#testChild')).toHaveLength(1);
    expect(wrapper.find('#testChild').prop('error')).toBe(false);
  });

  it('Should render with error value', () => {
    const props = {
      name: 'test',
      value: ' ',
      min: 0,
      max: 10,
      required: true,
      hasError: jest.fn(),
      equalTo: null,
      children: [<div id="testChild" key="test" />],
    };
    component = setUp(props);
    expect(component.length).toBe(1);
    expect(component.find('#testChild').prop('error')).toBe(true);
  });

  it('Should render with not exist value', () => {
    const props = {
      name: 'test',
      value: 0,
      min: 0,
      max: 10,
      required: true,
      hasError: jest.fn(),
      equalTo: null,
      children: [<div id="testChild" key="test" />],
    };
    component = setUp(props);
    expect(component.length).toBe(1);
    expect(component.find('#testChild').prop('error')).toBe(true);
  });

  it('Should render with not satisfy min value', () => {
    const props = {
      name: 'test',
      value: 't',
      min: 2,
      max: 10,
      required: true,
      hasError: jest.fn(),
      equalTo: null,
      children: [<div id="testChild" key="test" />],
    };
    component = setUp(props);
    expect(component.length).toBe(1);
    expect(component.find('#testChild').prop('error')).toBe(true);
  });

  it('Should render with not satisfy max value', () => {
    const props = {
      name: 'test',
      value: 'test',
      min: 0,
      max: 3,
      required: true,
      hasError: jest.fn(),
      equalTo: null,
      children: [<div id="testChild" key="test" />],
    };
    component = setUp(props);
    expect(component.length).toBe(1);
    expect(component.find('#testChild').prop('error')).toBe(true);
  });

  it('Should render with not satisfy equalTo value', () => {
    const props = {
      name: 'test',
      value: 'test',
      min: 0,
      max: 10,
      required: true,
      hasError: jest.fn(),
      equalTo: 'error',
      children: [<div id="testChild" key="test" />],
    };
    component = setUp(props);
    expect(component.length).toBe(1);
    expect(component.find('#testChild').prop('error')).toBe(true);
  });
});
