import React from 'react';
import '../../../../../setupTests';

import {shallow} from 'enzyme';

import {findByTestAtrr} from '../../../../../utils/test';

import FilterList from '../../../../../modules/common/components/FilterList';

const setUp = (props = {}) => {
  const component = shallow(<FilterList {...props} />);
  return component;
};

describe('Filter List Unit', () => {
  let component;

  it('Should render global Filter List', () => {
    const props = {
      filters: [],
      open: true,
      changeValue: jest.fn(),
      onFilter: jest.fn(),
    };
    component = setUp(props);
    const wrapper = findByTestAtrr(component, 'filter');
    expect(wrapper.length).toBe(1);
  });
});
