import React from 'react';
import '../../../../../setupTests';
import {useIntl} from 'react-intl';

import {shallow} from 'enzyme';

import {findByTestAtrr} from '../../../../../utils/test';

import Success from '../../../../../modules/common/components/Success';

jest.mock('react-intl', () => ({
  useIntl: jest.fn(),
}));

const setUp = (props = {}) => {
  useIntl.mockImplementation(() => {
    return {formatMessage: ({defaultMessage}) => defaultMessage};
  });
  const component = shallow(<Success {...props} />);
  return component;
};

describe('Success Unit', () => {
  let component;

  it('Should render global success', () => {
    const props = {
      message: 'test',
      onClose: jest.fn(),
    };
    component = setUp(props);
    const wrapper = findByTestAtrr(component, 'success');
    expect(wrapper.length).toBe(1);
    expect(wrapper.text()).toContain('test');
  });

  it('Should no render success', () => {
    const props = {
      onClose: jest.fn(),
    };
    component = setUp(props);
    const wrapper = findByTestAtrr(component, 'success');

    expect(wrapper.length).toBe(0);
  });
});
