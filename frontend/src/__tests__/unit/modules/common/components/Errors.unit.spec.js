import React from 'react';
import '../../../../../setupTests';
import {useIntl} from 'react-intl';

import {shallow} from 'enzyme';

import {findByTestAtrr} from '../../../../../utils/test';

import Errors from '../../../../../modules/common/components/Errors';

jest.mock('react-intl', () => ({
  useIntl: jest.fn(),
}));

const setUp = (props = {}) => {
  useIntl.mockImplementation(() => {
    return {formatMessage: ({defaultMessage}) => defaultMessage};
  });
  const component = shallow(<Errors {...props} />);
  return component;
};

describe('Errors Unit', () => {
  let component;

  it('Should render global errors', () => {
    const props = {
      errors: {code: 'test'},
      onClose: jest.fn(),
    };
    component = setUp(props);
    const wrapper = findByTestAtrr(component, 'errors');
    expect(wrapper.length).toBe(1);
    expect(wrapper.text()).toContain('test');
  });

  it('Should render field errors', () => {
    const props = {
      errors: {
        fieldErrors: [
          {
            fieldName: 'test',
            message: 'test',
          },
        ],
      },
      onClose: jest.fn(),
    };
    component = setUp(props);
    const wrapper = findByTestAtrr(component, 'errors');
    expect(wrapper.length).toBe(1);
  });

  it('Should no render', () => {
    const props = {
      onClose: jest.fn(),
    };
    component = setUp(props);
    const wrapper = findByTestAtrr(component, 'errors');

    expect(wrapper.length).toBe(0);
  });
});
