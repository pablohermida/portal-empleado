import React from 'react';
import '../../../../../setupTests';

import {shallow} from 'enzyme';

import {findByTestAtrr} from '../../../../../utils/test';

import ModalConfirm from '../../../../../modules/common/components/ModalConfirm';

const setUp = (props = {}) => {
  const component = shallow(<ModalConfirm {...props} />);
  return component;
};

describe('ModalConfirm Unit', () => {
  let component;

  it('Should render global Modal Confirm', () => {
    const props = {
      open: true,
      confirm: jest.fn(),
      onClose: jest.fn(),
    };
    component = setUp(props);
    const wrapper = findByTestAtrr(component, 'confirm-dialog');
    expect(wrapper.length).toBe(1);
  });
});
