import React from 'react';
import '../../../../../setupTests';

import {shallow} from 'enzyme';

import {findByTestAtrr} from '../../../../../utils/test';
import Loader from '../../../../../modules/common/components/Loader';
import {CircularProgress} from '@material-ui/core';

const setUp = (props = {}) => {
  const component = shallow(<Loader {...props} />);
  return component;
};

describe('Loader Unit', () => {
  let component;
  beforeEach(() => {
    const props = {
      loading: true,
    };
    component = setUp(props);
  });

  it('Should render without errors', () => {
    const wrapper = findByTestAtrr(component, 'loader');
    expect(wrapper.length).toBe(1);
    expect(wrapper.find(CircularProgress)).toHaveLength(1);
  });

  it('Should render without errors', () => {
    const props = {
      loading: false,
    };
    component = setUp(props);
    const wrapper = findByTestAtrr(component, 'loader');
    expect(wrapper.length).toBe(0);
  });
});
