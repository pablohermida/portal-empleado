import React from 'react';
import '../../../../../setupTests';
import {useIntl} from 'react-intl';

import {shallow} from 'enzyme';

import {findByTestAtrr} from '../../../../../utils/test';

import ErrorDialog from '../../../../../modules/common/components/ErrorDialog';
import {DialogContentText} from '@material-ui/core';
import {NetworkError} from '../../../../../api';

jest.mock('react-intl', () => ({
  useIntl: jest.fn(),
}));

const setUp = (props = {}) => {
  useIntl.mockImplementation(() => {
    return {formatMessage: ({defaultMessage}) => defaultMessage};
  });
  const component = shallow(<ErrorDialog {...props} />);
  return component;
};

describe('Error Dialog Unit', () => {
  let component;
  beforeEach(() => {
    const props = {
      error: {message: 'test'},
      onClose: jest.fn(),
    };
    component = setUp(props);
  });

  it('Should render without errors', () => {
    const wrapper = findByTestAtrr(component, 'error-dialog');
    expect(wrapper.length).toBe(1);
    expect(wrapper.find(DialogContentText).text()).toBe('test');
  });

  it('Should no render', () => {
    const props = {
      onClose: jest.fn(),
    };
    component = setUp(props);
    const wrapper = findByTestAtrr(component, 'error-dialog');
    expect(wrapper.length).toBe(0);
  });

  it('Should render network error', () => {
    const props = {
      error: new NetworkError(),
      onClose: jest.fn(),
    };
    component = setUp(props);
    const wrapper = findByTestAtrr(component, 'error-dialog');
    expect(wrapper.length).toBe(1);
    expect(wrapper.find(DialogContentText)).toBeDefined();
  });
});
