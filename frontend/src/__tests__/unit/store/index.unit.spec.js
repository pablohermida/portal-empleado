import configureStore from '../../../store/index';

let store;

describe('Reducer Unit', () => {
  beforeEach(() => {
    store = configureStore();
  });

  it('Should return state without errors', () => {
    expect(store.getState()).toBeDefined();
    expect(store.getState().app).toBeDefined();
    expect(store.getState().employee).toBeDefined();
  });
});
