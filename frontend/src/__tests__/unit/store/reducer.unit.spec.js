import {createStore} from 'redux';
import reducer from '../../../store/reducer';

let store;

describe('Reducer Unit', () => {
  beforeEach(() => {
    store = createStore(reducer);
  });

  it('Should return state without errors', () => {
    expect(store.getState().app).toBeDefined();
    expect(store.getState().employee).toBeDefined();
  });

  it('Should return app initial state correctly', () => {
    expect(store.getState().app).toMatchObject({error: null});
    expect(store.getState().app).toMatchObject({loading: false});
  });

  it('Should return employee initial state correctly', () => {
    expect(store.getState().employee.employee).toBeDefined();
    expect(store.getState().employee.workDays).toBeDefined();
    expect(store.getState().employee).toMatchObject({roles: []});
    expect(store.getState().employee).toMatchObject({jobs: []});
  });
});
