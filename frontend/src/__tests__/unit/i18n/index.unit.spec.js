import {initReactIntl} from '../../../i18n/index';

let intl;
let languageMock;
let languagesMock;

describe('I18n Unit', () => {
  beforeEach(() => {
    languageMock = jest.spyOn(navigator, 'language', 'get');
    languagesMock = jest.spyOn(navigator, 'languages', 'get');
  });

  it('Should return intl without errors', () => {
    languageMock.mockReturnValue(undefined);
    languagesMock.mockReturnValue(undefined);
    intl = initReactIntl();
    expect(intl.locale).toContain('en');
    expect(intl.messages).toBeDefined();

    languageMock.mockReturnValue('gl');
    languagesMock.mockReturnValue(undefined);
    intl = initReactIntl();
    expect(intl.locale).toContain('gl');

    languageMock.mockReturnValue(undefined);
    languagesMock.mockReturnValue(['de']);
    intl = initReactIntl();
    expect(intl.locale).toContain('en');
  });
});
