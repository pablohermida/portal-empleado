const {merge} = require('webpack-merge');

const commonConfig = require('./config/webpack/webpack.common.config');

module.exports = () => {
  let mode = 'dev';
  if (process.env.NODE_ENV == 'production') {
    mode = 'prod';
  }
  const config = require('./config/webpack/webpack.' + mode + '.config');
  return merge(commonConfig, config);
};
