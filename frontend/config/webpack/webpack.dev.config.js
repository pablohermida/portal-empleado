const path = require('path');
const webpack = require('webpack');

module.exports = {
  mode: 'development',
  entry: path.resolve(__dirname, '../../src/index.js'),
  output: {
    filename: '[name].js',
  },
  devtool: 'eval-source-map',
  devServer: {
    overlay: true,
    historyApiFallback: true,
    serveIndex: true,
    port: 9000,
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
        API_URL: JSON.stringify('http://localhost:8080'),
        HASH_PASS: JSON.stringify('portalempleado'),
      },
    }),
  ],
};
