const webpack = require('webpack');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const WorkboxPlugin = require('workbox-webpack-plugin');

module.exports = {
  mode: 'production',
  entry: {
    sw: './src/serviceWorker.js',
    main: './src/index.js',
    vendor: ['react', 'react-dom', 'react-router-dom', 'core-js'],
    redux: ['redux', 'react-redux', 'redux-logger', 'redux-thunk'],
  },
  output: {
    filename: '[name].[chunkhash:16].js',
  },
  optimization: {
    usedExports: true,
    sideEffects: true,
    runtimeChunk: {
      name: 'runtime',
    },
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: 'vendor',
          name: 'vendor',
          enforce: true,
          chunks: 'all',
        },
      },
    },
  },
  devtool: 'hidden-source-map',
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, '../../public'),
        },
      ],
    }),
    new WorkboxPlugin.InjectManifest({
      swSrc: './src/serviceWorker.js',
      chunks: ['*.chunk.js'],
      exclude: [
        /\.(?:png|jpg|jpeg|svg|ico)$/,
        /\.map$/,
        /manifest\.json$/,
        /serviceWorker\.js$/,
      ],
      include: [path.resolve(process.cwd(), 'build')],
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
        API_URL: JSON.stringify('http://localhost:8080'),
        HASH_PASS: JSON.stringify('!x^L!AI*2^zw$E#7jP@m'),
      },
    }),
  ],
};
