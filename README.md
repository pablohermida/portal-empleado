# Employee Portal

Presentantion of the final degree project of Software Engenieering. The
necessary functionalities are implemented for an employee portal that
facilitates the incorporation of teleworking.

## Requirements

| Software     | Version               |
| ------------ | --------------------- |
| Java         | 1.8.0_41              |
| Apache Maven | 3.6.3                 |
| Yarn         | 1.22.4                |
| MySQL        | 5.5 (10.4.13-MariaDB) |

## Installation

Build frontend:

```bash
cd frontend
yarn install
```

## Usage

It is important to remember that you must have MySQL started on port 3306 in
order to run the application properly.

```bash
cd backend
mvn spring-boot:run

cd frontend
yarn start
```

## Testing

You can run the unit and integration tests:

```bash
cd backend
mvn verify org.pitest:pitest-maven:mutationCoverage -P h2
mvn spring-boot:run -P h2

cd frontend
yarn test:verify
```

The results will be stored in /backend/target/site/jacoco/index.html and
/backend/target/site/pit/index.html for the backend and
/frontend/coverage/lcov-report/index.html for the frontend.

In addition, analysis tests are integrated into the frontend:

```bash
yarn analyze-dev %generate .json from development dependencies
yarn analyze-prod %generate .json from production dependencies
```

These .json have to be entered [in this tool]
(https://webpack.github.io/analyse/) to verify the analyzes.

## License

[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)
