package es.udc.fic.pablohermida.portalempleado.test.unit.model.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.emory.mathcs.backport.java.util.Arrays;
import es.udc.fic.pablohermida.portalempleado.model.daos.employee.EmployeeDao;
import es.udc.fic.pablohermida.portalempleado.model.daos.employee.JobDao;
import es.udc.fic.pablohermida.portalempleado.model.daos.employee.RoleDao;
import es.udc.fic.pablohermida.portalempleado.model.daos.employee.VideoConferenceEmployeeDao;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Employee;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.EmployeeAuth;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.EmployeeSecurityData;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Job;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Role;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.VideoConferenceEmployee;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.VideoConferenceEmployeeKey;
import es.udc.fic.pablohermida.portalempleado.model.services.Block;
import es.udc.fic.pablohermida.portalempleado.model.services.employee.EmployeeServiceImpl;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.AlreadyUnsuscribeException;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.FieldErrorException;
import es.udc.fic.pablohermida.portalempleado.utils.filters.EmployeeFilter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

/**
 * The type Employee service test.
 */
@SpringBootTest
@DisplayName("Employee Service")
public class EmployeeServiceUnitTest {
  /**
   * The Employee service.
   */
  @InjectMocks
  private EmployeeServiceImpl employeeService;

  /**
   * The Employee dao.
   */
  @Mock
  private EmployeeDao employeeDao;

  /**
   * The Role dao.
   */
  @Mock
  private RoleDao roleDao;

  /**
   * The Job dao.
   */
  @Mock
  private JobDao jobDao;

  @Mock
  private VideoConferenceEmployeeDao vcemployeeDao;

  private List<Job> jobs;

  private List<Role> roles;

  private static List<Role> createRoles(final String... names) {
    Role role;
    final List<Role> roles = new ArrayList<>();
    for (final String name : names) {
      role = new Role();
      role.setName(name);
      roles.add(role);
    }
    return roles;
  }

  private static List<Job> createJobs(final String... names) {
    Job job;
    final List<Job> jobs = new ArrayList<>();
    for (final String name : names) {
      job = new Job();
      job.setName(name);
      jobs.add(job);
    }
    return jobs;
  }

  /**
   * Create test employee.
   *
   * @return the test employee
   */
  private Employee.EmployeeBuilder createTestEmployee() {
    final Employee.EmployeeBuilder employee = new Employee.EmployeeBuilder();
    employee.name("Name").lastname("Lastname").image("src/image.jpg")
        .dateBirth(LocalDate.now()).email("email@email.es").dni("47386285Y")
        .address("Address").addressWork("Address Work").phone("123456789")
        .duration("Duration").dateStart(LocalDate.now()).workDay("Work Day")
        .workHours("Work Hours").job(this.jobs.get(0)).role(this.roles.get(0))
        .employeeAuth(
            new EmployeeAuth("username", StringUtils.repeat("a", 96), null))
        .employeeSecurityData(
            new EmployeeSecurityData(StringUtils.repeat("a", 24), true))
        .vcsystems(null);

    return employee;
  }

  /**
   * Sets up.
   */
  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);

    this.jobs = createJobs("JOB1", "JOB2", "JOB3");
    this.roles = createRoles("ADMIN", "EMPLOYEE");
  }

  /**
   * When sign up employee then retrieved employee is correct.
   *
   * @throws InstanceAlreadyExistsException the instance already
   *                                        exists
   *
   *                                        exception
   * @throws FieldErrorException            the field error exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("Sign Up Correct")
  public void whenSignUpEmployeeThenRetrievedEmployeeIsCorrect()
      throws InstanceAlreadyExistsException, FieldErrorException,
      InstanceNotFoundException {
    final Employee.EmployeeBuilder employee = this.createTestEmployee();
    final Long idExpected = 1L;
    Mockito.when(this.employeeDao.save(employee.build()))
        .thenReturn(employee.id(idExpected).build());
    final Long employeeId = this.employeeService.signUp(employee.build());
    Mockito.verify(this.employeeDao).save(employee.build());
    assertEquals(idExpected, employeeId);
  }

  /**
   * When sign up employee with supervisor then retrieved employee is correct.
   *
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws FieldErrorException            the field error exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("Sign Up With Supervisor Correct")
  public void whenSignUpEmployeeWithSupervisorThenRetrievedEmployeeIsCorrect()
      throws InstanceAlreadyExistsException, FieldErrorException,
      InstanceNotFoundException {
    final Employee.EmployeeBuilder employee = this.createTestEmployee();
    final Long idExpected = 1L;
    final Optional<Employee> supervisor = Optional.of(employee.build());
    employee.supervisor(new Employee());
    Mockito.when(this.employeeDao.save(employee.build()))
        .thenReturn(employee.id(idExpected).build());
    Mockito.when(this.employeeDao.findById(null)).thenReturn(supervisor);
    final Long employeeId = this.employeeService.signUp(employee.build());
    Mockito.verify(this.employeeDao).save(employee.build());
    assertEquals(idExpected, employeeId);
  }

  /**
   * When sign up employee then retrieved employee is not valid.
   *
   * @throws InstanceAlreadyExistsException the instance already
   *                                        exists
   *
   *                                        exception
   * @throws FieldErrorException            the field error exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("Sign Up Incomplete")
  public void whenSignUpEmployeeThenRetrievedEmployeeIsCorrectButIncomplete()
      throws InstanceAlreadyExistsException, FieldErrorException,
      InstanceNotFoundException {
    final Employee.EmployeeBuilder employee = this.createTestEmployee();
    final Long idExpected = 1L;

    employee.image(null).vcsystems(null);
    Mockito.when(this.employeeDao.save(employee.build()))
        .thenReturn(employee.id(idExpected).build());
    final Long employeeId = this.employeeService.signUp(employee.build());
    Mockito.verify(this.employeeDao).save(employee.build());
    assertEquals(idExpected, employeeId);
  }

  /**
   * When sign up employee then retrieved employee is already exists.
   */
  @Test
  @DisplayName("Sign Up AlreadyExists")
  public void whenSignUpEmployeeThenRetrievedEmployeeIsAlreadyExists() {
    final Employee.EmployeeBuilder employee = this.createTestEmployee();
    final String username = employee.build().getEmployeeAuth().getUsername();
    Mockito.when(this.employeeDao.existsByEmployeeAuthUsername(username))
        .thenReturn(true);
    assertThrows(InstanceAlreadyExistsException.class, () -> {
      this.employeeService.signUp(employee.build());
    });
    Mockito.verify(this.employeeDao).existsByEmployeeAuthUsername(username);
  }

  /**
   * When sign up employee then retrieved employee is not valid.
   */
  @Test
  @DisplayName("Sign Up Invalid")
  public void whenSignUpEmployeeThenRetrievedEmployeeIsNotValid() {
    final Employee.EmployeeBuilder employee = this.createTestEmployee();
    employee.name("a");

    assertThrows(FieldErrorException.class, () -> {
      this.employeeService.signUp(employee.build());
    });
  }

  /**
   * When sign up employee then retrieved supervisor is not found.
   */
  @Test
  @DisplayName("Sign Up Supervisor Not Found")
  public void whenSignUpEmployeeThenRetrievedSupervisorIsNotFound() {
    final Employee.EmployeeBuilder employee = this.createTestEmployee();
    final Employee.EmployeeBuilder supervisor = this.createTestEmployee();
    final Long supervisorId = 1L;
    supervisor.id(supervisorId);
    employee.supervisor(supervisor.build());
    Mockito.when(
        this.employeeDao.existsByEmployeeAuthUsername(Mockito.anyString()))
        .thenReturn(false);
    Mockito.when(this.employeeDao.findById(supervisorId))
        .thenReturn(Optional.empty());

    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeService.signUp(employee.build());
    });

    Mockito.verify(this.employeeDao)
        .existsByEmployeeAuthUsername(Mockito.anyString());
    Mockito.verify(this.employeeDao).findById(supervisorId);
  }

  /**
   * When get all roles then retrieved list role is correct.
   */
  @Test
  @DisplayName("Get Employee By Username")
  public void whenGetEmployeeByUsernameThenRetrievedListEmployeeIsCorrect() {
    final List<Employee> employees = new ArrayList<>();
    List<Employee> employeesFound = new ArrayList<>();
    employees.add(new Employee());
    Mockito.when(this.employeeDao
        .findAllByEmployeeAuthUsernameIgnoreCaseContainingOrderByEmployeeAuthUsernameDesc(
            Mockito.anyString())).thenReturn(employees);
    employeesFound =
        this.employeeService.searchEmployeesByUsername(Mockito.anyString());
    Mockito.verify(this.employeeDao)
        .findAllByEmployeeAuthUsernameIgnoreCaseContainingOrderByEmployeeAuthUsernameDesc(
            Mockito.anyString());
    assertEquals(employees, employeesFound);
  }

  /**
   * When get all roles then retrieved list role is correct.
   */
  @Test
  @DisplayName("Get All Roles")
  public void whenGetAllRolesThenRetrievedListRoleIsCorrect() {
    Mockito.when(this.roleDao.findAllByOrderByNameAsc()).thenReturn(this.roles);
    final List<Role> rolesFound = this.employeeService.getAllRoles();
    Mockito.verify(this.roleDao).findAllByOrderByNameAsc();
    assertEquals(this.roles, rolesFound);
  }

  /**
   * When get role by id then retrieved role is correct.
   *
   * @throws InstanceNotFoundException the instance not found exception
   */
  @Test
  @DisplayName("Get Role By Id")
  public void whenGetRoleByIdThenRetrievedRoleIsCorrect()
      throws InstanceNotFoundException {
    final Long id = 1L;
    Mockito.when(this.roleDao.findById(id))
        .thenReturn(Optional.of(this.roles.get(0)));
    final Role roleFound = this.employeeService.getRoleById(id);
    Mockito.verify(this.roleDao).findById(id);
    assertEquals(this.roles.get(0), roleFound);
  }

  /**
   * When get role by id then retrieved role is incorrect.
   */
  @Test
  @DisplayName("Get Role By Id Not Exist")
  public void whenGetRoleByIdThenRetrievedRoleIsIncorrect() {
    final Long id = 1L;
    Mockito.when(this.roleDao.findById(id)).thenReturn(Optional.empty());
    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeService.getRoleById(id);
    });
    Mockito.verify(this.roleDao).findById(id);
  }

  /**
   * When get all jobs then retrieved list job is correct.
   */
  @Test
  @DisplayName("Get All Jobs")
  public void whenGetAllJobsThenRetrievedListJobIsCorrect() {
    Mockito.when(this.jobDao.findAllByOrderByNameDesc()).thenReturn(this.jobs);
    final List<Job> jobsFound = this.employeeService.getAllJobs();
    Mockito.verify(this.jobDao).findAllByOrderByNameDesc();
    assertEquals(this.jobs, jobsFound);
  }

  /**
   * When get job by id then retrieved job is correct.
   *
   * @throws InstanceNotFoundException the instance not found exception
   */
  @Test
  @DisplayName("Get Job By Id")
  public void whenGetJobByIdThenRetrievedJobIsCorrect()
      throws InstanceNotFoundException {
    final Long id = 1L;
    Mockito.when(this.jobDao.findById(id))
        .thenReturn(Optional.of(this.jobs.get(0)));
    final Job jobActual = this.employeeService.getJobById(id);
    Mockito.verify(this.jobDao).findById(id);
    assertEquals(this.jobs.get(0), jobActual);
  }

  /**
   * When get job by id then retrieved job is incorrect.
   */
  @Test
  @DisplayName("Get Job By Id Not Exist")
  public void whenGetJobByIdThenRetrievedJobIsIncorrect() {
    final Long id = 1L;
    Mockito.when(this.jobDao.findById(id)).thenReturn(Optional.empty());
    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeService.getJobById(id);
    });
    Mockito.verify(this.jobDao).findById(id);
  }

  /**
   * When sign up employee with vc system then retrieved employee is correct.
   *
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws FieldErrorException            the field error exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("Sign Up With VCSystems Correct")
  public void whenSignUpEmployeeWithVCSystemThenRetrievedEmployeeIsCorrect()
      throws InstanceAlreadyExistsException, FieldErrorException,
      InstanceNotFoundException {
    final VideoConferenceEmployee vce =
        new VideoConferenceEmployee(new VideoConferenceEmployeeKey(null, 1L),
            "test");
    final Set<VideoConferenceEmployee> vcsystems = new HashSet<>();
    vcsystems.add(vce);
    final Employee.EmployeeBuilder employee = this.createTestEmployee();
    final Long idExpected = 1L;
    employee.vcsystems(vcsystems);
    Mockito.when(this.employeeDao.save(employee.build()))
        .thenReturn(employee.id(idExpected).build());
    Mockito.when(this.vcemployeeDao.save(vce)).thenReturn(vce);
    final Long employeeId = this.employeeService.signUp(employee.build());
    Mockito.verify(this.employeeDao).save(employee.build());
    Mockito.verify(this.vcemployeeDao).save(vce);
    assertEquals(idExpected, employeeId);
  }

  /**
   * When get employee by username then retrieved employee auth is correct.
   *
   * @throws InstanceNotFoundException the instance not found exception
   */
  @Test
  @DisplayName("Get Employee By Username")
  public void whenGetEmployeeByUsernameThenRetrievedEmployeeIsCorrect()
      throws InstanceNotFoundException {
    final String username = "username";
    Mockito.when(this.employeeDao.findByEmployeeAuthUsername(username))
        .thenReturn(Optional.of(this.createTestEmployee().build()));
    final Employee result =
        this.employeeService.getEmployeeByUsername(username);
    assertEquals(result.getEmployeeAuth().getUsername(), username);
    Mockito.verify(this.employeeDao).findByEmployeeAuthUsername(username);
  }

  /**
   * When get employee by username then retrieved instance not found.
   */
  @Test
  @DisplayName("Get Employee By Username No Exist")
  public void whenGetEmployeeByUsernameThenRetrievedInstanceNotFound() {
    final String username = "test";
    Mockito.when(this.employeeDao.findByEmployeeAuthUsername(username))
        .thenReturn(Optional.empty());
    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeService.getEmployeeByUsername(username);
    });
    Mockito.verify(this.employeeDao).findByEmployeeAuthUsername(username);
  }

  /**
   * When get employee by id then retrieved employee is correct.
   *
   * @throws InstanceNotFoundException the instance not found exception
   */
  @Test
  @DisplayName("Get Employee By Id")
  public void whenGetEmployeeByIdThenRetrievedEmployeeIsCorrect()
      throws InstanceNotFoundException {
    final Long userId = 1L;
    Mockito.when(this.employeeDao.findById(userId))
        .thenReturn(Optional.of(this.createTestEmployee().build()));
    final Employee result = this.employeeService.getEmployeeById(userId);
    assertNotNull(result);
    Mockito.verify(this.employeeDao).findById(userId);
  }

  /**
   * When get employee by id then retrieved instance not found.
   */
  @Test
  @DisplayName("Get Employee By Id No Exist")
  public void whenGetEmployeeByIdThenRetrievedInstanceNotFound() {
    final Long userId = 1L;
    Mockito.when(this.employeeDao.findById(userId))
        .thenReturn(Optional.empty());
    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeService.getEmployeeById(userId);
    });
    Mockito.verify(this.employeeDao).findById(userId);
  }

  /**
   * When edit employee then employee is edited.
   *
   * @throws FieldErrorException       the field error exception
   * @throws InstanceNotFoundException the instance not found exception
   */
  @Test
  @DisplayName("Edit Employee")
  public void whenEditEmployeeThenEmployeeIsEdited()
      throws FieldErrorException, InstanceNotFoundException {
    final Long employeeId = 1L;
    final Employee.EmployeeBuilder employee = this.createTestEmployee();
    employee.id(employeeId);
    Mockito.when(this.employeeDao.findById(employeeId))
        .thenReturn(Optional.of(this.createTestEmployee().build()));
    this.employeeService.editEmployee(employee.build());
    Mockito.verify(this.employeeDao).findById(employeeId);
    Mockito.verify(this.employeeDao).save(employee.build());
  }

  /**
   * When edit employee with vc systems then employee is edited.
   *
   * @throws FieldErrorException       the field error exception
   * @throws InstanceNotFoundException the instance not found exception
   */
  @Test
  @DisplayName("Edit Employee with VcSystems")
  public void whenEditEmployeeWithVCSystemsThenEmployeeIsEdited()
      throws FieldErrorException, InstanceNotFoundException {
    final Long employeeId = 1L;
    final VideoConferenceEmployee vce =
        new VideoConferenceEmployee(new VideoConferenceEmployeeKey(null, 1L),
            "test");
    final Set<VideoConferenceEmployee> vcsystems = new HashSet<>();
    vcsystems.add(vce);
    final Employee.EmployeeBuilder employee = this.createTestEmployee();
    employee.id(employeeId);
    employee.vcsystems(vcsystems);
    Employee edit = employee.build();
    Mockito.when(this.employeeDao.findById(employeeId))
        .thenReturn(Optional.of(edit));
    Mockito.when(this.vcemployeeDao.save(vce)).thenReturn(vce);
    this.employeeService.editEmployee(edit);
    Mockito.verify(this.employeeDao).findById(employeeId);
    Mockito.verify(this.employeeDao).save(edit);
    Mockito.verify(this.vcemployeeDao).save(vce);
  }

  /**
   * When edit employee then retrieved field error exception.
   */
  @Test
  @DisplayName("Edit Employee Field Error Exception")
  public void whenEditEmployeeThenRetrievedFieldErrorException() {
    final Employee.EmployeeBuilder employee = this.createTestEmployee();
    employee.role(null);
    assertThrows(FieldErrorException.class, () -> {
      this.employeeService.editEmployee(employee.build());
    });
  }

  /**
   * When edit employee then retrieved instance not found exception.
   */
  @Test
  @DisplayName("Edit Employee Instance Not Found Exception")
  public void whenEditEmployeeThenRetrievedInstanceNotFoundException() {
    final Long employeeId = 1L;
    final Employee.EmployeeBuilder employee = this.createTestEmployee();
    employee.id(employeeId);
    Mockito.when(this.employeeDao.findById(employeeId))
        .thenReturn(Optional.empty());
    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeService.editEmployee(employee.build());
    });
    Mockito.verify(this.employeeDao).findById(employeeId);
  }

  /**
   * When list employee filters then retrieved block employees.
   *
   * @throws FieldErrorException the field error exception
   */
  @Test
  @DisplayName("List Employee By Filters")
  public void whenListEmployeeFiltersThenRetrievedBlockEmployees()
      throws FieldErrorException {
    final Employee.EmployeeBuilder employee = this.createTestEmployee();
    final List<Employee> list = Arrays.asList(
        new Employee[] {employee.build(), employee.build(), employee.build()});
    final PageRequest page =
        PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
    Mockito.when(
        this.employeeDao.findAllByFilters("test", null, null, null, null, page))
        .thenReturn(new PageImpl<>(list));
    final Block<Employee> block = this.employeeService
        .searchEmployees(new EmployeeFilter("test", null, null, null, null),
            page);
    assertEquals(list, block.getItems());
    assertFalse(block.getExistMoreItems());
    assertEquals(1, block.getTotalPages());
    Mockito.verify(this.employeeDao)
        .findAllByFilters("test", null, null, null, null, page);
  }

  /**
   * When list employee filters empty then retrieved block employees.
   *
   * @throws FieldErrorException the field error exception
   */
  @Test
  @DisplayName("List Employee By Filters Empty")
  public void whenListEmployeeFiltersEmptyThenRetrievedBlockEmployees()
      throws FieldErrorException {
    final Employee.EmployeeBuilder employee = this.createTestEmployee();
    final List<Employee> list = Arrays.asList(
        new Employee[] {employee.build(), employee.build(), employee.build()});
    final PageRequest page =
        PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
    Mockito.when(
        this.employeeDao.findAllByFilters(null, null, null, null, null, page))
        .thenReturn(new PageImpl<>(list));
    final Block<Employee> block =
        this.employeeService.searchEmployees(new EmployeeFilter(), page);
    assertEquals(list, block.getItems());
    assertFalse(block.getExistMoreItems());
    assertEquals(1, block.getTotalPages());
    Mockito.verify(this.employeeDao)
        .findAllByFilters(null, null, null, null, null, page);
  }

  /**
   * When list employee filters then retrieved field error exception.
   */
  @Test
  @DisplayName("List Employee By Filters Order Attribute Not Exist")
  public void whenListEmployeeFiltersThenRetrievedFieldErrorException() {
    final PageRequest page = PageRequest.of(0, 10);
    assertThrows(FieldErrorException.class, () -> {
      this.employeeService.searchEmployees(new EmployeeFilter(), page);
    });
  }

  /**
   * When unsuscribe employee then change employee.
   *
   * @throws AlreadyUnsuscribeException the already unsuscribe exception
   * @throws InstanceNotFoundException  the instance not found exception
   */
  @Test
  @DisplayName("Unsuscribe Employee")
  public void whenUnsuscribeEmployeeThenChangeEmployee()
      throws AlreadyUnsuscribeException, InstanceNotFoundException {
    final Long employeeId = 1L;
    final Employee.EmployeeBuilder employee = this.createTestEmployee();
    Mockito.when(this.employeeDao.findById(employeeId))
        .thenReturn(Optional.of(employee.build()));
    this.employeeService.unsuscribeEmployee(employeeId);
    final Employee result = employee.build();
    result.setEmployeeSecurityData(new EmployeeSecurityData(
        result.getEmployeeSecurityData().getEmployeeId(),
        result.getEmployeeSecurityData().getIban(), false));
    Mockito.verify(this.employeeDao).findById(employeeId);
    Mockito.verify(this.employeeDao).save(result);
  }

  /**
   * When unsuscribe employee then instance not found exception.
   */
  @Test
  @DisplayName("Unsuscribe Employee Not Exist")
  public void whenUnsuscribeEmployeeThenInstanceNotFoundException() {
    final Long employeeId = 1L;
    Mockito.when(this.employeeDao.findById(employeeId))
        .thenReturn(Optional.empty());
    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeService.unsuscribeEmployee(employeeId);
    });
    Mockito.verify(this.employeeDao).findById(employeeId);
  }

  /**
   * When unsuscribe employee then already unsuscribe exception.
   */
  @Test
  @DisplayName("Unsuscribe Employee Unsuscribed")
  public void whenUnsuscribeEmployeeThenAlreadyUnsuscribeException() {
    final Long employeeId = 1L;
    final Employee.EmployeeBuilder employee = this.createTestEmployee();
    employee.employeeSecurityData(new EmployeeSecurityData("test", false));
    Mockito.when(this.employeeDao.findById(employeeId))
        .thenReturn(Optional.of(employee.build()));
    assertThrows(AlreadyUnsuscribeException.class, () -> {
      this.employeeService.unsuscribeEmployee(employeeId);
    });
    Mockito.verify(this.employeeDao).findById(employeeId);
  }
}
