package es.udc.fic.pablohermida.portalempleado.test.unit.model.entities;

import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Employee;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.EmployeeAuth;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.EmployeeSecurityData;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Job;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Role;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.VideoConferenceEmployee;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.VideoConferenceEmployeeKey;
import java.time.LocalDate;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * The type VideoConference entities test.
 */
@SpringBootTest
@DisplayName("VideoConference Entities")
public class VideoConferenceEntitiesTest {

  private static Employee.EmployeeBuilder createTestEmployee1() {
    final Employee.EmployeeBuilder employee = new Employee.EmployeeBuilder();
    employee.name("Name").lastname("Lastname").image("src/image.jpg")
        .dateBirth(LocalDate.now()).email("email@email.es").dni("47386285Y")
        .address("Address").addressWork("Address Work").phone("123456789")
        .duration("Duration").dateStart(LocalDate.now()).workDay("Work Day")
        .workHours("Work Hours").job(new Job("job")).role(new Role("role"))
        .employeeAuth(
            new EmployeeAuth("username", StringUtils.repeat("a", 96), null))
        .employeeSecurityData(
            new EmployeeSecurityData(StringUtils.repeat("a", 24), true));

    return employee;
  }

  private static Employee.EmployeeBuilder createTestEmployee2() {
    final Employee.EmployeeBuilder employee = new Employee.EmployeeBuilder();
    employee.name("test").lastname("test").image("src/test.jpg")
        .dateBirth(LocalDate.now()).email("test@test.es").dni("47386284M")
        .address("test").addressWork("Test Work").phone("1234567890")
        .duration("test").dateStart(LocalDate.now().plusDays(1))
        .workDay("Test Day").workHours("Test Hours").job(new Job("test"))
        .role(new Role("test")).employeeAuth(
        new EmployeeAuth("test", StringUtils.repeat("b", 96), null))
        .employeeSecurityData(
            new EmployeeSecurityData(StringUtils.repeat("b", 24), true));

    return employee;
  }

  /**
   * Equals video conference employee contract.
   */
  @Test
  @DisplayName("VideoConference Employee Equals")
  public void equalsVideoConferenceEmployeeContract() {
    EqualsVerifier.forClass(VideoConferenceEmployee.class).usingGetClass()
        .withPrefabValues(Employee.class,
            VideoConferenceEntitiesTest.createTestEmployee1().build(),
            VideoConferenceEntitiesTest.createTestEmployee2().build())
        .suppress(Warning.ALL_FIELDS_SHOULD_BE_USED)
        .withIgnoredFields("employee", "vcsystem").verify();
  }

  /**
   * Equals video conference employee key contract.
   */
  @Test
  @DisplayName("VideoConference Employee Key Equals")
  public void equalsVideoConferenceEmployeeKeyContract() {
    EqualsVerifier.forClass(VideoConferenceEmployeeKey.class).usingGetClass()
        .verify();
  }
}
