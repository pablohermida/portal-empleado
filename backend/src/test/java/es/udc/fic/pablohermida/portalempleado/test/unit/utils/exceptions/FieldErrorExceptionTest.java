package es.udc.fic.pablohermida.portalempleado.test.unit.utils.exceptions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import es.udc.fic.pablohermida.portalempleado.utils.exceptions.FieldErrorException;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.FieldErrorReason;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * The type Field error exception test.
 */
@SpringBootTest
@DisplayName("Field Error Exception")
public class FieldErrorExceptionTest {

  /**
   * Initialize exception test.
   */
  @Test
  @DisplayName("Initialize Field Error Exception")
  public void initializeExceptionTest() {
    final String fieldName = "test";
    final FieldErrorReason fer = FieldErrorReason.IS_BLANK;
    final FieldErrorException exceptionExpected =
        new FieldErrorException(fieldName, fer);

    assertEquals(exceptionExpected.getField(), fieldName);
    assertEquals(exceptionExpected.getReason(), fer);
    assertEquals(exceptionExpected.getMessage(),
        fieldName + " " + fer.getMessage());
  }
}
