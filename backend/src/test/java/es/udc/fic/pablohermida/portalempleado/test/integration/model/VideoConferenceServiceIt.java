package es.udc.fic.pablohermida.portalempleado.test.integration.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import es.udc.fic.pablohermida.portalempleado.model.entities.employee.VideoConferenceSystem;
import es.udc.fic.pablohermida.portalempleado.model.services.employee.VideoConferenceSystemService;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.FieldErrorException;
import java.util.List;
import javax.management.InstanceAlreadyExistsException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

/**
 * The type Video conference service it.
 */
@SpringBootTest
@ActiveProfiles("test")
@DisplayName("VideoConference System Service Integration")
@Transactional
public class VideoConferenceServiceIt {
  @Autowired
  private VideoConferenceSystemService vcsystemsService;

  /**
   * Create vc system test.
   *
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws FieldErrorException            the field error exception
   */
  @Test
  @DisplayName("Create and Get all VCSystems")
  public void createVcSystemTest()
      throws InstanceAlreadyExistsException, FieldErrorException {
    final String platformTest = "test";
    final VideoConferenceSystem vcs = new VideoConferenceSystem(platformTest);
    this.vcsystemsService.createVideoConferenceSystem(vcs);

    final List<VideoConferenceSystem> vcSystemsResult =
        this.vcsystemsService.getVideoConferenceSystems();

    assertTrue(
        vcSystemsResult.contains(new VideoConferenceSystem(platformTest)));
  }

  /**
   * Create vc system already exist test.
   *
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws FieldErrorException            the field error exception
   */
  @Test
  @DisplayName("Create Already Exists VCSystem")
  public void createVcSystemAlreadyExistTest()
      throws InstanceAlreadyExistsException, FieldErrorException {
    final String platformTest = "test";
    final VideoConferenceSystem vcs = new VideoConferenceSystem(platformTest);
    this.vcsystemsService.createVideoConferenceSystem(vcs);

    assertThrows(InstanceAlreadyExistsException.class, () -> {
      this.vcsystemsService.createVideoConferenceSystem(vcs);
    });
  }

  /**
   * Create vc system incorrect test.
   */
  @Test
  @DisplayName("Create Incorrect VCSystem")
  public void createVcSystemIncorrectTest() {
    final String platformTest = "a";
    final VideoConferenceSystem vcs = new VideoConferenceSystem(platformTest);

    assertThrows(FieldErrorException.class, () -> {
      this.vcsystemsService.createVideoConferenceSystem(vcs);
    });
  }

  /**
   * Gets all vc systems empty test.
   */
  @Test
  @DisplayName("Get all empty VCSystems")
  public void getAllVcSystemsEmptyTest() {
    final List<VideoConferenceSystem> vcSystemsResult =
        this.vcsystemsService.getVideoConferenceSystems();

    assertEquals(0, vcSystemsResult.size());
  }
}
