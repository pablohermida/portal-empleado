package es.udc.fic.pablohermida.portalempleado.test.unit.rest.dtos;

import es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceEmployeeDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceSystemCreateDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceSystemDto;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * The type Video conference system dtos test.
 */
@SpringBootTest
@DisplayName("VideoConference Dtos")
public class VideoConferenceDtosTest {
  /**
   * Equals video conference system dto contract.
   */
  @Test
  @DisplayName("VideoConference System Dto Equals")
  public void equalsVideoConferenceSystemDtoContract() {
    EqualsVerifier.forClass(VideoConferenceSystemDto.class).usingGetClass()
        .withIgnoredFields("id").verify();
  }

  /**
   * Equals video conference system create dto contract.
   */
  @Test
  @DisplayName("VideoConference System Create Dto Equals")
  public void equalsVideoConferenceSystemCreateDtoContract() {
    EqualsVerifier.forClass(VideoConferenceSystemCreateDto.class)
        .usingGetClass().suppress(Warning.NONFINAL_FIELDS).verify();
  }

  /**
   * Equals video conference employee dto contract.
   */
  @Test
  @DisplayName("VideoConference System Employee Dto Equals")
  public void equalsVideoConferenceSystemEmployeeDtoContract() {
    EqualsVerifier.forClass(VideoConferenceEmployeeDto.class).usingGetClass()
        .verify();
  }
}
