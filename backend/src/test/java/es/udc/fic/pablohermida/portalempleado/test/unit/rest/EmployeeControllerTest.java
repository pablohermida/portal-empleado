package es.udc.fic.pablohermida.portalempleado.test.unit.rest;

import static es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeConversor.toEmployee;
import static es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeConversor.toEmployeeDto;
import static es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeConversor.toEmployeeFilters;
import static es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeConversor.toEmployeeLimitDto;
import static es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeConversor.toEmployeesListDto;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;

import es.udc.fic.pablohermida.portalempleado.config.JwtGenerator;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Employee;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.EmployeeAuth;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.EmployeeSecurityData;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Job;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Role;
import es.udc.fic.pablohermida.portalempleado.model.services.Block;
import es.udc.fic.pablohermida.portalempleado.model.services.employee.EmployeeService;
import es.udc.fic.pablohermida.portalempleado.rest.controllers.EmployeeController;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeAuthDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeFiltersDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeListDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeLoginDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeSearchDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.JobDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.RoleDto;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.AlreadyUnsuscribeException;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.FieldErrorException;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.PasswordNotMatchException;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.PermissionException;
import es.udc.fic.pablohermida.portalempleado.utils.filters.EmployeeFilter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * The type Employee controller test.
 */
@SpringBootTest
@DisplayName("Employee Controller")
public class EmployeeControllerTest {
  private List<Role> initRoles;
  private List<Job> initJobs;

  @InjectMocks
  private EmployeeController employeeController;

  @Mock
  private EmployeeService employeeService;

  @Mock
  private BCryptPasswordEncoder passwordEncoder;

  @Mock
  private JwtGenerator jwtGenerator;

  private static EmployeeDto createEmployeeDto(final String username,
                                               final String supervisorLogin) {
    return new EmployeeDto("Pablo", "Hermida Mourelle", null,
        LocalDate.of(1994, 5, 25), "info@pablohermida.com", "47376285Y",
        "C/Prueba 15 2D 15006 A Coruña", "C/Prueba 15 2D 15006 A Coruña",
        "+34123456789", "Indefinido", LocalDate.of(2017, 1, 24),
        "Jornada completa", "Infinito", 1L, 1L, "test", supervisorLogin,
        username, "test", "ES000000000000000000", null);
  }

  private Employee.EmployeeBuilder createTestEmployee(final String username,
                                                      final String password) {
    final Employee.EmployeeBuilder employee = new Employee.EmployeeBuilder();
    employee.name("Name").lastname("Lastname").image("src/image.jpg")
        .dateBirth(LocalDate.now()).email("email@email.es").dni("12345678Z")
        .address("Address").addressWork("Address Work").phone("123456789")
        .duration("Duration").dateStart(LocalDate.now()).workDay("Work Day")
        .workHours("Work Hours").job(this.initJobs.get(0))
        .role(this.initRoles.get(0))
        .employeeAuth(new EmployeeAuth(username, password, null))
        .employeeSecurityData(
            new EmployeeSecurityData(StringUtils.repeat("a", 24), true))
        .vcsystems(null);

    return employee;
  }

  private void setInitRoles() {
    this.initRoles = new ArrayList<>();
    this.initRoles.add(new Role(1L, "ADMIN"));
    this.initRoles.add(new Role(2L, "EMPLOYEE"));
  }

  private void setInitJobs() {
    this.initJobs = new ArrayList<>();
    this.initJobs.add(new Job(1L, "Job 1"));
    this.initJobs.add(new Job(2L, "Job 2"));
  }

  /**
   * Sets up.
   *
   * @throws PasswordNotMatchException the password not match exception
   * @throws InstanceNotFoundException the instance not found exception
   */
  @BeforeEach
  void setUp() throws PasswordNotMatchException, InstanceNotFoundException {
    MockitoAnnotations.openMocks(this);

    this.setInitRoles();
    this.setInitJobs();
  }

  /**
   * Gets all roles test.
   */
  @Test
  @DisplayName("Get All Roles")
  public void getAllRolesTest() {
    Mockito.when(this.employeeService.getAllRoles()).thenReturn(this.initRoles);
    final List<RoleDto> roles = this.employeeController.getRoles();

    assertEquals(this.initRoles.size(), roles.size());

    for (final RoleDto role : roles) {
      assertNotNull(role.getName());
    }
    Mockito.verify(this.employeeService).getAllRoles();
  }

  /**
   * Gets all roles empty test.
   */
  @Test
  @DisplayName("Get All Roles Empty")
  public void getAllRolesEmptyTest() {
    Mockito.when(this.employeeService.getAllRoles())
        .thenReturn(new ArrayList<>());
    final List<RoleDto> roles = this.employeeController.getRoles();

    assertEquals(0, roles.size());
    Mockito.verify(this.employeeService).getAllRoles();
  }

  /**
   * Gets all jobs test.
   */
  @Test
  @DisplayName("Get All Jobs")
  public void getAllJobsTest() {
    Mockito.when(this.employeeService.getAllJobs()).thenReturn(this.initJobs);
    final List<JobDto> jobs = this.employeeController.getJobs();

    assertEquals(this.initJobs.size(), jobs.size());

    for (final JobDto job : jobs) {
      assertNotNull(job.getName());
    }
    Mockito.verify(this.employeeService).getAllJobs();
  }

  /**
   * Gets all jobs empty test.
   */
  @Test
  @DisplayName("Get All Jobs Empty")
  public void getAllJobsEmptyTest() {
    Mockito.when(this.employeeService.getAllJobs())
        .thenReturn(new ArrayList<>());
    final List<JobDto> jobs = this.employeeController.getJobs();

    assertEquals(0, jobs.size());
    Mockito.verify(this.employeeService).getAllJobs();
  }

  /**
   * Sign up test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("Sign Up")
  public void signUpTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException {
    final EmployeeDto employeeDto = createEmployeeDto("phermida", null);
    final String token = "test";

    Mockito.when(this.employeeService.getRoleById(Mockito.anyLong()))
        .thenReturn(new Role("role"));
    Mockito.when(this.employeeService.getJobById(Mockito.anyLong()))
        .thenReturn(new Job("job"));
    Mockito.when(this.passwordEncoder.encode(Mockito.any())).thenReturn("test");
    Mockito.when(this.employeeService.signUp(Mockito.any())).thenReturn(1L);
    Mockito.when(this.jwtGenerator.generate(Mockito.any())).thenReturn(token);

    final EmployeeAuthDto authDto = this.employeeController.signUp(employeeDto);

    assertEquals(employeeDto.getUsername(), authDto.getUsername());
    assertEquals(token, authDto.getToken());

    Mockito.verify(this.employeeService).getRoleById(Mockito.anyLong());
    Mockito.verify(this.employeeService).getJobById(Mockito.anyLong());
    Mockito.verify(this.passwordEncoder).encode(Mockito.any());
    Mockito.verify(this.employeeService).signUp(Mockito.any());
    Mockito.verify(this.jwtGenerator).generate(Mockito.any());
  }

  /**
   * Sign up field error test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("Sign Up Field Error")
  public void signUpFieldErrorTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException {
    final EmployeeDto employeeDto = createEmployeeDto("phermida", null);

    Mockito.when(this.employeeService.getRoleById(Mockito.anyLong()))
        .thenReturn(new Role("role"));
    Mockito.when(this.employeeService.getJobById(Mockito.anyLong()))
        .thenReturn(new Job("job"));
    Mockito.when(this.passwordEncoder.encode(Mockito.any())).thenReturn("test");
    Mockito.when(this.employeeService.signUp(Mockito.any()))
        .thenThrow(FieldErrorException.class);

    assertThrows(FieldErrorException.class, () -> {
      this.employeeController.signUp(employeeDto);
    });

    Mockito.verify(this.employeeService).getRoleById(Mockito.anyLong());
    Mockito.verify(this.employeeService).getJobById(Mockito.anyLong());
    Mockito.verify(this.passwordEncoder).encode(Mockito.any());
    Mockito.verify(this.employeeService).signUp(Mockito.any());
  }

  /**
   * Sign up already exists test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("Sign Up Already Exists")
  public void signUpAlreadyExistsTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException {
    final EmployeeDto employeeDto = createEmployeeDto("phermida", null);

    Mockito.when(this.employeeService.getRoleById(Mockito.anyLong()))
        .thenReturn(new Role("role"));
    Mockito.when(this.employeeService.getJobById(Mockito.anyLong()))
        .thenReturn(new Job("job"));
    Mockito.when(this.passwordEncoder.encode(Mockito.any())).thenReturn("test");
    Mockito.when(this.employeeService.signUp(Mockito.any()))
        .thenThrow(InstanceAlreadyExistsException.class);

    assertThrows(InstanceAlreadyExistsException.class, () -> {
      this.employeeController.signUp(employeeDto);
    });

    Mockito.verify(this.employeeService).getRoleById(Mockito.anyLong());
    Mockito.verify(this.employeeService).getJobById(Mockito.anyLong());
    Mockito.verify(this.passwordEncoder).encode(Mockito.any());
    Mockito.verify(this.employeeService).signUp(Mockito.any());
  }

  /**
   * Sign up role not found test.
   *
   * @throws InstanceNotFoundException the instance not found exception
   */
  @Test
  @DisplayName("Sign Up Role Not Found")
  public void signUpRoleNotFoundTest() throws InstanceNotFoundException {
    final EmployeeDto employeeDto = createEmployeeDto("phermida", null);

    Mockito.when(this.employeeService.getRoleById(Mockito.anyLong()))
        .thenThrow(InstanceNotFoundException.class);

    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeController.signUp(employeeDto);
    });

    Mockito.verify(this.employeeService).getRoleById(Mockito.anyLong());
  }

  /**
   * Sign up job not found test.
   *
   * @throws InstanceNotFoundException the instance not found exception
   */
  @Test
  @DisplayName("Sign Up Job Not Found")
  public void signUpJobNotFoundTest() throws InstanceNotFoundException {
    final EmployeeDto employeeDto = createEmployeeDto("phermida", null);

    Mockito.when(this.employeeService.getRoleById(Mockito.anyLong()))
        .thenReturn(new Role("role"));
    Mockito.when(this.employeeService.getJobById(Mockito.anyLong()))
        .thenThrow(InstanceNotFoundException.class);

    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeController.signUp(employeeDto);
    });

    Mockito.verify(this.employeeService).getRoleById(Mockito.anyLong());
  }

  /**
   * Sign up with supervisor test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("Sign Up With Supervisor")
  public void signUpWithSupervisorTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException {
    final EmployeeDto employeeDto = createEmployeeDto("test", null);
    final List<Employee> supervisors = Arrays.asList(new Employee());
    final String token = "test";

    Mockito.when(this.employeeService.getRoleById(Mockito.anyLong()))
        .thenReturn(new Role("role"));
    Mockito.when(this.employeeService.getJobById(Mockito.anyLong()))
        .thenReturn(new Job("job"));
    Mockito.when(this.passwordEncoder.encode(Mockito.any())).thenReturn("test");
    Mockito.when(this.jwtGenerator.generate(Mockito.any())).thenReturn(token);
    Mockito.when(this.employeeService.signUp(Mockito.any())).thenReturn(1L);
    Mockito.when(this.employeeService.searchEmployeesByUsername("test"))
        .thenReturn(supervisors);
    final EmployeeAuthDto authDto = this.employeeController
        .signUp(createEmployeeDto(employeeDto.getUsername(), "test"));

    assertEquals(employeeDto.getUsername(), authDto.getUsername());
    assertEquals(token, authDto.getToken());

    Mockito.verify(this.employeeService).getRoleById(Mockito.anyLong());
    Mockito.verify(this.employeeService).getJobById(Mockito.anyLong());
    Mockito.verify(this.employeeService).signUp(Mockito.any());
    Mockito.verify(this.passwordEncoder).encode(Mockito.any());
    Mockito.verify(this.jwtGenerator).generate(Mockito.any());
    Mockito.verify(this.employeeService).searchEmployeesByUsername("test");
  }

  /**
   * Sign up with supervisor not exit test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("Sign Up With Supervisor Not Exist")
  public void signUpWithSupervisorNotExitTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException {
    final EmployeeDto employeeDto = createEmployeeDto("test", null);

    Mockito.when(this.employeeService.getRoleById(Mockito.anyLong()))
        .thenReturn(new Role("role"));
    Mockito.when(this.employeeService.getJobById(Mockito.anyLong()))
        .thenReturn(new Job("job"));

    final List<Employee> supervisors = Arrays.asList();
    Mockito.when(this.employeeService.searchEmployeesByUsername("test"))
        .thenReturn(supervisors);
    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeController.signUp(createEmployeeDto("phermida", "test"));
    });

    Mockito.verify(this.employeeService).getRoleById(Mockito.anyLong());
    Mockito.verify(this.employeeService).getJobById(Mockito.anyLong());
    Mockito.verify(this.employeeService).searchEmployeesByUsername("test");
  }

  /**
   * Search by user name test.
   */
  @Test
  @DisplayName("Search by Username")
  public void searchByUserNameTest() {
    final EmployeeDto employeeDto = createEmployeeDto("test", null);
    final List<Employee> employeesInit = new ArrayList<>();
    employeesInit.add(
        toEmployee(employeeDto, this.initRoles.get(0), this.initJobs.get(0),
            null, this.passwordEncoder));
    Mockito.when(
        this.employeeService.searchEmployeesByUsername(Mockito.anyString()))
        .thenReturn(employeesInit);
    final List<EmployeeSearchDto> employees =
        this.employeeController.searchByUserName(Mockito.anyString());

    assertEquals(employeesInit.size(), employees.size());

    for (final EmployeeSearchDto employeeFound : employees) {
      assertEquals(employeeDto.getUsername(), employeeFound.getUsername());
      assertEquals(employeeDto.getName() + " " + employeeDto.getLastname(),
          employeeFound.getFullname());
    }

    Mockito.verify(this.employeeService)
        .searchEmployeesByUsername(Mockito.anyString());
  }

  /**
   * Login test.
   *
   * @throws PasswordNotMatchException the password not match exception
   * @throws InstanceNotFoundException the instance not found exception
   */
  @Test
  @DisplayName("Login Without Errors")
  public void loginTest()
      throws PasswordNotMatchException, InstanceNotFoundException {
    final String username = "test";
    final EmployeeLoginDto employeeLoginDto =
        new EmployeeLoginDto(username, username);
    final Employee employee =
        this.createTestEmployee(username, username).build();
    final String token = "test";

    Mockito.when(this.employeeService.getEmployeeByUsername(username))
        .thenReturn(employee);
    Mockito.when(this.passwordEncoder.matches(Mockito.any(), Mockito.any()))
        .thenReturn(true);
    Mockito.when(this.jwtGenerator.generate(Mockito.any())).thenReturn(token);

    final EmployeeAuthDto authDto =
        this.employeeController.login(employeeLoginDto);

    assertEquals(employeeLoginDto.getUsername(), authDto.getUsername());
    assertEquals(token, authDto.getToken());

    Mockito.verify(this.employeeService).getEmployeeByUsername(username);
    Mockito.verify(this.passwordEncoder).matches(Mockito.any(), Mockito.any());
    Mockito.verify(this.jwtGenerator).generate(Mockito.any());
  }

  /**
   * Login password not match test.
   *
   * @throws InstanceNotFoundException the instance not found exception
   */
  @Test
  @DisplayName("Login Password Not Match")
  public void loginPasswordNotMatchTest() throws InstanceNotFoundException {
    final String username = "test";
    final EmployeeLoginDto employeeLoginDto =
        new EmployeeLoginDto(username, username);
    final Employee employee = this.createTestEmployee(username, null).build();

    Mockito.when(this.employeeService.getEmployeeByUsername(username))
        .thenReturn(employee);
    Mockito.when(this.passwordEncoder.matches(Mockito.any(), Mockito.any()))
        .thenReturn(false);

    assertThrows(PasswordNotMatchException.class, () -> {
      this.employeeController.login(employeeLoginDto);
    });

    Mockito.verify(this.employeeService).getEmployeeByUsername(username);
    Mockito.verify(this.passwordEncoder).matches(Mockito.any(), Mockito.any());
  }

  /**
   * Login instance not found test.
   *
   * @throws InstanceNotFoundException the instance not found exception
   */
  @Test
  @DisplayName("Login Username Not Found")
  public void loginInstanceNotFoundTest() throws InstanceNotFoundException {
    final String username = "test";
    final EmployeeLoginDto employeeLoginDto =
        new EmployeeLoginDto(username, username);

    Mockito.when(this.employeeService.getEmployeeByUsername(username))
        .thenThrow(InstanceNotFoundException.class);

    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeController.login(employeeLoginDto);
    });

    Mockito.verify(this.employeeService).getEmployeeByUsername(username);
  }

  /**
   * Login from service token test.
   *
   * @throws InstanceNotFoundException the instance not found exception
   */
  @Test
  @DisplayName("Login From Service Token Without Errors")
  public void loginFromServiceTokenTest() throws InstanceNotFoundException {
    final Long userId = 1L;
    final Employee employee = this.createTestEmployee("test", "test").build();
    final String token = "test";

    Mockito.when(this.employeeService.getEmployeeById(userId))
        .thenReturn(employee);

    final EmployeeAuthDto authDto =
        this.employeeController.loginFromServiceToken(userId, token);

    assertEquals("test", authDto.getUsername());
    assertEquals(token, authDto.getToken());

    Mockito.verify(this.employeeService).getEmployeeById(userId);
  }

  /**
   * Login from service token instance not found test.
   *
   * @throws InstanceNotFoundException the instance not found exception
   */
  @Test
  @DisplayName("Login From Service Token Username Not Found")
  public void loginFromServiceTokenInstanceNotFoundTest()
      throws InstanceNotFoundException {
    final Long userId = 1L;
    final String token = "test";

    Mockito.when(this.employeeService.getEmployeeById(userId))
        .thenThrow(InstanceNotFoundException.class);

    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeController.loginFromServiceToken(userId, token);
    });

    Mockito.verify(this.employeeService).getEmployeeById(userId);
  }

  /**
   * Gets employeeby id test.
   *
   * @throws InstanceNotFoundException the instance not found exception
   */
  @Test
  @DisplayName("Get Employee By Id")
  public void getEmployeebyIdTest() throws InstanceNotFoundException {
    final Long userId = 1L;
    final Employee employee = this.createTestEmployee("test", "test").build();
    Mockito.when(this.employeeService.getEmployeeById(userId))
        .thenReturn(employee);

    final EmployeeDto employeeDto =
        this.employeeController.getEmployeeById(userId, userId);

    assertEquals(toEmployeeDto(employee), employeeDto);

    Mockito.verify(this.employeeService, times(2)).getEmployeeById(userId);
  }

  /**
   * Gets employeeby id admin employee test.
   *
   * @throws InstanceNotFoundException the instance not found exception
   */
  @Test
  @DisplayName("Get Employee By Id Admin Get Self Data")
  public void getEmployeebyIdAdminEmployeeTest()
      throws InstanceNotFoundException {
    final Long adminId = 1L;
    final Long userId = 2L;
    final Employee admin = this.createTestEmployee("test", "test").build();
    final Employee employee =
        this.createTestEmployee("test", "test").role(this.initRoles.get(1))
            .build();
    Mockito.when(this.employeeService.getEmployeeById(adminId))
        .thenReturn(admin);
    Mockito.when(this.employeeService.getRoleById(admin.getRole().getId()))
        .thenReturn(admin.getRole());
    Mockito.when(this.employeeService.getEmployeeById(userId))
        .thenReturn(employee);

    final EmployeeDto employeeDto =
        this.employeeController.getEmployeeById(adminId, userId);

    assertEquals(toEmployeeDto(employee), employeeDto);

    Mockito.verify(this.employeeService).getEmployeeById(adminId);
    Mockito.verify(this.employeeService).getEmployeeById(userId);
    Mockito.verify(this.employeeService).getRoleById(admin.getRole().getId());
  }

  /**
   * Gets employeeby id random employee test.
   *
   * @throws InstanceNotFoundException the instance not found exception
   */
  @Test
  @DisplayName("Get Employee By Id Random Employee")
  public void getEmployeebyIdRandomEmployeeTest()
      throws InstanceNotFoundException {
    final Long user1id = 1L;
    final Long user2id = 2L;
    final Employee employee1 =
        this.createTestEmployee("test", "test").role(new Role(2L, "test"))
            .build();
    Mockito.when(this.employeeService.getEmployeeById(user1id))
        .thenReturn(employee1);
    Mockito.when(this.employeeService.getRoleById(employee1.getRole().getId()))
        .thenReturn(employee1.getRole());
    Mockito.when(this.employeeService.getEmployeeById(user2id))
        .thenReturn(employee1);

    final EmployeeDto employeeDto =
        this.employeeController.getEmployeeById(user1id, user2id);

    assertEquals(toEmployeeLimitDto(employee1), employeeDto);

    Mockito.verify(this.employeeService).getEmployeeById(user1id);
    Mockito.verify(this.employeeService).getEmployeeById(user2id);
    Mockito.verify(this.employeeService)
        .getRoleById(employee1.getRole().getId());
  }

  /**
   * Gets employeeby id if not exist test.
   *
   * @throws InstanceNotFoundException the instance not found exception
   */
  @Test
  @DisplayName("Get Employee By Id If Not Exist")
  public void getEmployeebyIdIfNotExistTest() throws InstanceNotFoundException {
    final Long userId = 1L;
    final Employee employee = this.createTestEmployee("test", "test").build();
    Mockito.when(this.employeeService.getEmployeeById(userId))
        .thenThrow(InstanceNotFoundException.class);

    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeController.getEmployeeById(userId, userId);
    });

    Mockito.verify(this.employeeService).getEmployeeById(userId);
  }

  /**
   * Upload employee test.
   *
   * @throws InstanceNotFoundException the instance not found exception
   * @throws PermissionException       the permission exception
   * @throws FieldErrorException       the field error exception
   */
  @Test
  @DisplayName("Upload Employee")
  public void uploadEmployeeTest()
      throws InstanceNotFoundException, PermissionException,
      FieldErrorException {
    final Long userId = 1L;
    final Employee employee = this.createTestEmployee("test", "test").build();
    Mockito.when(this.employeeService.getEmployeeById(userId))
        .thenReturn(employee);
    Mockito.when(this.employeeService.getRoleById(Mockito.anyLong()))
        .thenReturn(new Role("role"));
    Mockito.when(this.employeeService.getJobById(Mockito.anyLong()))
        .thenReturn(new Job("job"));
    Mockito.when(this.passwordEncoder.encode(Mockito.any())).thenReturn("test");

    this.employeeController
        .uploadEmployee(userId, userId, toEmployeeDto(employee));

    Mockito.verify(this.employeeService).getEmployeeById(userId);
    Mockito.verify(this.employeeService).getRoleById(Mockito.anyLong());
    Mockito.verify(this.employeeService).getJobById(Mockito.anyLong());
    Mockito.verify(this.passwordEncoder).encode(Mockito.any());
  }

  /**
   * Upload employee with supervisor test.
   *
   * @throws InstanceNotFoundException the instance not found exception
   * @throws PermissionException       the permission exception
   * @throws FieldErrorException       the field error exception
   */
  @Test
  @DisplayName("Upload Employee With Supervisor")
  public void uploadEmployeeWithSupervisorTest()
      throws InstanceNotFoundException, PermissionException,
      FieldErrorException {
    final Long adminId = 1L;
    final Long editId = 2L;
    final Employee admin = this.createTestEmployee("test", "test").build();
    final Employee edit =
        this.createTestEmployee("test", "test").role(this.initRoles.get(1))
            .supervisor(admin).build();
    Mockito.when(this.employeeService.getEmployeeById(adminId))
        .thenReturn(admin);
    Mockito.when(this.employeeService.getRoleById(admin.getRole().getId()))
        .thenReturn(admin.getRole());
    Mockito.when(this.employeeService.getRoleById(edit.getRole().getId()))
        .thenReturn(edit.getRole());
    Mockito.when(this.employeeService.getJobById(edit.getJob().getId()))
        .thenReturn(edit.getJob());
    Mockito.when(this.employeeService
        .getEmployeeByUsername(admin.getEmployeeAuth().getUsername()))
        .thenReturn(admin);
    Mockito.when(this.passwordEncoder.encode(Mockito.any())).thenReturn("test");

    this.employeeController
        .uploadEmployee(adminId, editId, toEmployeeDto(edit));

    Mockito.verify(this.employeeService).getEmployeeById(adminId);
    Mockito.verify(this.employeeService).getRoleById(admin.getRole().getId());
    Mockito.verify(this.employeeService).getRoleById(edit.getRole().getId());
    Mockito.verify(this.employeeService).getRoleById(edit.getRole().getId());
    Mockito.verify(this.employeeService).getJobById(edit.getJob().getId());
    Mockito.verify(this.employeeService)
        .getEmployeeByUsername(admin.getEmployeeAuth().getUsername());
    Mockito.verify(this.passwordEncoder).encode(Mockito.any());
  }

  /**
   * Upload employee with supervisor instance not found exception test.
   *
   * @throws InstanceNotFoundException the instance not found exception
   */
  @Test
  @DisplayName("Upload Employee With Supervisor Not Found")
  public void uploadEmployeeWithSupervisorInstanceNotFoundExceptionTest()
      throws InstanceNotFoundException {
    final Long adminId = 1L;
    final Long editId = 2L;
    final Employee admin = this.createTestEmployee("test", "test").build();
    final Employee edit =
        this.createTestEmployee("test", "test").role(this.initRoles.get(1))
            .supervisor(admin).build();
    Mockito.when(this.employeeService.getEmployeeById(adminId))
        .thenReturn(admin);
    Mockito.when(this.employeeService.getRoleById(admin.getRole().getId()))
        .thenReturn(admin.getRole());
    Mockito.when(this.employeeService.getRoleById(edit.getRole().getId()))
        .thenReturn(edit.getRole());
    Mockito.when(this.employeeService.getJobById(edit.getJob().getId()))
        .thenReturn(edit.getJob());
    Mockito.when(this.employeeService
        .getEmployeeByUsername(admin.getEmployeeAuth().getUsername()))
        .thenThrow(InstanceNotFoundException.class);

    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeController
          .uploadEmployee(adminId, editId, toEmployeeDto(edit));
    });

    Mockito.verify(this.employeeService).getEmployeeById(adminId);
    Mockito.verify(this.employeeService).getRoleById(admin.getRole().getId());
    Mockito.verify(this.employeeService).getRoleById(edit.getRole().getId());
    Mockito.verify(this.employeeService).getRoleById(edit.getRole().getId());
    Mockito.verify(this.employeeService).getJobById(edit.getJob().getId());
    Mockito.verify(this.employeeService)
        .getEmployeeByUsername(admin.getEmployeeAuth().getUsername());
  }

  /**
   * Upload employee instance not found test.
   *
   * @throws InstanceNotFoundException the instance not found exception
   */
  @Test
  @DisplayName("Upload Employee By Id If User Not Exist")
  public void uploadEmployeeInstanceNotFoundTest()
      throws InstanceNotFoundException {
    final Long userId = 1L;
    final Employee employee = this.createTestEmployee("test", "test").build();
    Mockito.when(this.employeeService.getEmployeeById(userId))
        .thenThrow(InstanceNotFoundException.class);

    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeController
          .uploadEmployee(userId, userId, toEmployeeDto(employee));
    });

    Mockito.verify(this.employeeService).getEmployeeById(userId);
  }

  /**
   * Upload employee permission exception test.
   *
   * @throws InstanceNotFoundException the instance not found exception
   */
  @Test
  @DisplayName("Upload Employee Without Permission")
  public void uploadEmployeePermissionExceptionTest()
      throws InstanceNotFoundException {
    final Long userId1 = 1L;
    final Long userId2 = 2L;
    final Employee employee1 =
        this.createTestEmployee("test", "test").role(this.initRoles.get(1))
            .build();
    final Employee employee2 =
        this.createTestEmployee("test", "test").role(this.initRoles.get(1))
            .build();
    Mockito.when(this.employeeService.getEmployeeById(userId1))
        .thenReturn(employee1);
    Mockito.when(this.employeeService.getRoleById(employee1.getRole().getId()))
        .thenReturn(employee1.getRole());

    assertThrows(PermissionException.class, () -> {
      this.employeeController
          .uploadEmployee(userId1, userId2, toEmployeeDto(employee2));
    });

    Mockito.verify(this.employeeService).getEmployeeById(userId1);
    Mockito.verify(this.employeeService)
        .getRoleById(employee1.getRole().getId());
  }

  /**
   * List employee by filters test.
   *
   * @throws FieldErrorException the field error exception
   */
  @Test
  @DisplayName("List employee by Filters")
  public void listEmployeeByFiltersTest() throws FieldErrorException {
    final Employee.EmployeeBuilder employee =
        this.createTestEmployee("test", "test");
    final List<Employee> list =
        Arrays.asList(employee.build(), employee.build(), employee.build());

    final PageRequest page = PageRequest
        .of(0, 10, Sort.by(Sort.Direction.ASC, "employeeAuth.username"));
    final Block<Employee> block = new Block<>(list, false, 1);
    final EmployeeFiltersDto criteria =
        new EmployeeFiltersDto(null, null, null, null, null, null, null);

    Mockito.when(
        this.employeeService.searchEmployees(toEmployeeFilters(criteria), page))
        .thenReturn(block);

    final Block<EmployeeListDto> result =
        this.employeeController.getEmployeeByFilters(criteria, 0);

    assertEquals(toEmployeesListDto(list), result.getItems());

    Mockito.verify(this.employeeService)
        .searchEmployees(toEmployeeFilters(criteria), page);
  }

  /**
   * List employee by filters with order test.
   *
   * @throws FieldErrorException the field error exception
   */
  @Test
  @DisplayName("List employee by Filters With Order")
  public void listEmployeeByFiltersWithOrderTest() throws FieldErrorException {
    final Employee.EmployeeBuilder employee =
        this.createTestEmployee("test", "test");
    final List<Employee> list =
        Arrays.asList(employee.build(), employee.build(), employee.build());

    final PageRequest page =
        PageRequest.of(0, 10, Sort.by(Sort.Direction.DESC, "name"));
    final Block<Employee> block = new Block<>(list, false, 1);
    final EmployeeFiltersDto criteria =
        new EmployeeFiltersDto(null, null, null, null, null, "desc",
            Arrays.asList("name"));

    Mockito.when(
        this.employeeService.searchEmployees(toEmployeeFilters(criteria), page))
        .thenReturn(block);

    final Block<EmployeeListDto> result =
        this.employeeController.getEmployeeByFilters(criteria, 0);

    assertEquals(toEmployeesListDto(list), result.getItems());

    Mockito.verify(this.employeeService)
        .searchEmployees(toEmployeeFilters(criteria), page);
  }


  /**
   * List employee by filters field error exception test.
   *
   * @throws FieldErrorException the field error exception
   */
  @Test
  @DisplayName("List employee by Filters With Order Not Exist")
  public void listEmployeeByFiltersFieldErrorExceptionTest()
      throws FieldErrorException {
    final PageRequest page = PageRequest
        .of(0, 10, Sort.by(Sort.Direction.ASC, "employeeAuth" + ".username"));
    Mockito
        .when(this.employeeService.searchEmployees(new EmployeeFilter(), page))
        .thenThrow(FieldErrorException.class);
    assertThrows(FieldErrorException.class, () -> {
      this.employeeController.getEmployeeByFilters(
          new EmployeeFiltersDto(null, null, null, null, null, null, null), 0);
    });

    Mockito.verify(this.employeeService)
        .searchEmployees(new EmployeeFilter(), page);
  }

  /**
   * Unsuscribe employee test.
   *
   * @throws InstanceNotFoundException  the instance not found exception
   * @throws AlreadyUnsuscribeException the already unsuscribe exception
   */
  @Test
  @DisplayName("Unsuscribe Employee")
  public void unsuscribeEmployeeTest()
      throws InstanceNotFoundException, AlreadyUnsuscribeException {
    final Long userId = 1L;

    this.employeeController.unsuscribeEmployee(userId);

    Mockito.verify(this.employeeService).unsuscribeEmployee(userId);
  }

  /**
   * Unsuscribe employee instance not found exception test.
   *
   * @throws InstanceNotFoundException  the instance not found exception
   * @throws AlreadyUnsuscribeException the already unsuscribe exception
   */
  @Test
  @DisplayName("Unsuscribe Employee Not Exist")
  public void unsuscribeEmployeeInstanceNotFoundExceptionTest()
      throws InstanceNotFoundException, AlreadyUnsuscribeException {
    final Long userId = 1L;
    Mockito.doThrow(InstanceNotFoundException.class).when(this.employeeService)
        .unsuscribeEmployee(userId);

    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeController.unsuscribeEmployee(userId);
    });

    Mockito.verify(this.employeeService).unsuscribeEmployee(userId);
  }

  /**
   * Unsuscribe employee already unsuscribe exception test.
   *
   * @throws InstanceNotFoundException  the instance not found exception
   * @throws AlreadyUnsuscribeException the already unsuscribe exception
   */
  @Test
  @DisplayName("Unsuscribe Employee Unsuscribed")
  public void unsuscribeEmployeeAlreadyUnsuscribeExceptionTest()
      throws InstanceNotFoundException, AlreadyUnsuscribeException {
    final Long userId = 1L;
    Mockito.doThrow(AlreadyUnsuscribeException.class).when(this.employeeService)
        .unsuscribeEmployee(userId);

    assertThrows(AlreadyUnsuscribeException.class, () -> {
      this.employeeController.unsuscribeEmployee(userId);
    });

    Mockito.verify(this.employeeService).unsuscribeEmployee(userId);
  }
}
