package es.udc.fic.pablohermida.portalempleado.test.unit.rest;

import static es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceSystemConversor.toVideoConferenceSystem;
import static es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceSystemConversor.toVideoConferenceSystemDtos;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import es.udc.fic.pablohermida.portalempleado.model.entities.employee.VideoConferenceSystem;
import es.udc.fic.pablohermida.portalempleado.model.services.employee.VideoConferenceSystemService;
import es.udc.fic.pablohermida.portalempleado.rest.controllers.VideoConferenceSystemController;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceSystemCreateDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceSystemDto;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.FieldErrorException;
import java.util.ArrayList;
import java.util.List;
import javax.management.InstanceAlreadyExistsException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * The type Video conference system controller test.
 */
@SpringBootTest
@DisplayName("VideoConference System Controller")
public class VideoConferenceSystemControllerTest {
  @InjectMocks
  private VideoConferenceSystemController vcsController;

  @Mock
  private VideoConferenceSystemService vcsService;

  /**
   * Sets up.
   */
  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  /**
   * Add vcs test.
   *
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws FieldErrorException            the field error exception
   */
  @Test
  @DisplayName("Add VideoConference System")
  public void addVcsTest()
      throws InstanceAlreadyExistsException, FieldErrorException {
    final String platform = "test";
    final VideoConferenceSystemCreateDto vcsDto =
        new VideoConferenceSystemCreateDto(platform);

    final VideoConferenceSystem vcs = toVideoConferenceSystem(vcsDto);

    Mockito.when(this.vcsService.createVideoConferenceSystem(vcs))
        .thenReturn(Mockito.anyLong());

    this.vcsController.addVideoConferenceSystem(vcsDto);

    Mockito.verify(this.vcsService).createVideoConferenceSystem(vcs);
  }

  /**
   * Add vcs field error test.
   *
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws FieldErrorException            the field error exception
   */
  @Test
  @DisplayName("Add VideoConference System Field Error")
  public void addVcsFieldErrorTest()
      throws InstanceAlreadyExistsException, FieldErrorException {
    final String platform = "test";
    final VideoConferenceSystemCreateDto vcsDto =
        new VideoConferenceSystemCreateDto(platform);

    final VideoConferenceSystem vcs = toVideoConferenceSystem(vcsDto);

    Mockito.doThrow(FieldErrorException.class).when(this.vcsService)
        .createVideoConferenceSystem(vcs);

    assertThrows(FieldErrorException.class, () -> {
      this.vcsController.addVideoConferenceSystem(vcsDto);
    });

    Mockito.verify(this.vcsService).createVideoConferenceSystem(vcs);
  }

  /**
   * Add vcs already exists test.
   *
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws FieldErrorException            the field error exception
   */
  @Test
  @DisplayName("Add VideoConference System Already Exists")
  public void addVcsAlreadyExistsTest()
      throws InstanceAlreadyExistsException, FieldErrorException {
    final String platform = "test";
    final VideoConferenceSystemCreateDto vcsDto =
        new VideoConferenceSystemCreateDto(platform);

    final VideoConferenceSystem vcs = toVideoConferenceSystem(vcsDto);

    Mockito.doThrow(InstanceAlreadyExistsException.class).when(this.vcsService)
        .createVideoConferenceSystem(vcs);

    assertThrows(InstanceAlreadyExistsException.class, () -> {
      this.vcsController.addVideoConferenceSystem(vcsDto);
    });

    Mockito.verify(this.vcsService).createVideoConferenceSystem(vcs);
  }

  /**
   * Gets all vcs test.
   */
  @Test
  @DisplayName("Get All VideoConference System")
  public void getAllVcsTest() {
    final String platform = "test";
    final List<VideoConferenceSystem> vcsList = new ArrayList<>();
    vcsList.add(new VideoConferenceSystem(platform + "C"));
    vcsList.add(new VideoConferenceSystem(platform + "B"));
    vcsList.add(new VideoConferenceSystem(platform + "A"));

    final List<VideoConferenceSystemDto> vcsListDto =
        toVideoConferenceSystemDtos(vcsList);

    Mockito.when(this.vcsService.getVideoConferenceSystems())
        .thenReturn(vcsList);

    final List<VideoConferenceSystemDto> vcsListDtoResult =
        this.vcsController.getAllVideoConferenceSystem();

    assertEquals(vcsListDto, vcsListDtoResult);

    Mockito.verify(this.vcsService).getVideoConferenceSystems();
  }
}
