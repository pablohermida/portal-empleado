package es.udc.fic.pablohermida.portalempleado.test.unit.rest.dtos;

import static es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceSystemConversor.toVideoConferenceEmployeeDtos;
import static es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceSystemConversor.toVideoConferenceEmployees;
import static es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceSystemConversor.toVideoConferenceSystem;
import static es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceSystemConversor.toVideoConferenceSystemDtos;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import es.udc.fic.pablohermida.portalempleado.model.entities.employee.VideoConferenceEmployee;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.VideoConferenceEmployeeKey;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.VideoConferenceSystem;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceEmployeeDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceSystemConversor;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceSystemCreateDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceSystemDto;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * The type Video conference conversors test.
 */
@SpringBootTest
@DisplayName("VideoConference Conversors")
public class VideoConferenceConversorsTest {
  /**
   * Init video conference conversor test.
   */
  @Test
  @DisplayName("Video Conference Conversor")
  public void initVideoConferenceConversorTest() {
    final VideoConferenceSystemConversor conversor =
        new VideoConferenceSystemConversor();
    assertNotNull(conversor);
  }

  /**
   * To video conference system dtos test.
   */
  @Test
  @DisplayName("Video Conference System List Dto Conversor")
  public void toVideoConferenceSystemDtosTest() {
    final List<VideoConferenceSystem> vcs = new ArrayList<>();
    vcs.add(new VideoConferenceSystem("test1"));
    vcs.add(new VideoConferenceSystem("test2"));

    final List<VideoConferenceSystemDto> vcsDtos =
        toVideoConferenceSystemDtos(vcs);

    final AtomicReference<VideoConferenceSystem> vcsExpected =
        new AtomicReference<>();

    vcsDtos.forEach(vcsActual -> {
      vcsExpected.set(new VideoConferenceSystem(vcsActual.getPlatform()));
      assertTrue(vcs.contains(vcsExpected.get()));
    });
  }

  /**
   * To video conference system create test.
   */
  @Test
  @DisplayName("Video Conference System Create Conversor")
  public void toVideoConferenceSystemCreateTest() {
    final VideoConferenceSystemCreateDto vcsDto =
        new VideoConferenceSystemCreateDto("test");

    final VideoConferenceSystem vcs = toVideoConferenceSystem(vcsDto);

    assertEquals(vcsDto.getPlatform(), vcs.getPlatform());
  }

  /**
   * To video conference employees test.
   */
  @Test
  @DisplayName("Video Conference Employee List Conversor")
  public void toVideoConferenceEmployeesTest() {
    final List<VideoConferenceEmployeeDto> vcsDtos = new ArrayList<>();
    vcsDtos.add(new VideoConferenceEmployeeDto(1L, "test1", "test"));
    vcsDtos.add(new VideoConferenceEmployeeDto(2L, "test2", "test"));

    final List<VideoConferenceEmployee> vcs =
        toVideoConferenceEmployees(vcsDtos);

    vcs.forEach(vcsActual -> {
      assertTrue(vcs.contains(vcsActual));
    });

    final List<VideoConferenceEmployee> vcs2 =
        toVideoConferenceEmployees(1L, vcsDtos);

    final AtomicReference<VideoConferenceEmployee> vcsExpected =
        new AtomicReference<>();

    vcs.forEach(vcsActual -> {
      vcsExpected.set(new VideoConferenceEmployee(
          new VideoConferenceEmployeeKey(1l, vcsActual.getId().getIdVcsystem()),
          vcsActual.getData()));
      assertTrue(vcs2.contains(vcsExpected.get()));
    });
  }

  /**
   * To video conference employee dtos test.
   */
  @Test
  @DisplayName("Video Conference Employee List Dto Conversor")
  public void toVideoConferenceEmployeeDtosTest() {
    final Long idEmployee = 1L;
    final VideoConferenceEmployee vce1 = new VideoConferenceEmployee(
        new VideoConferenceEmployeeKey(idEmployee, 1L), "test1");
    final VideoConferenceEmployee vce2 = new VideoConferenceEmployee(
        new VideoConferenceEmployeeKey(idEmployee, 2L), "test2");
    vce1.setVcsystem(new VideoConferenceSystem("test"));
    vce2.setVcsystem(new VideoConferenceSystem("test"));

    final Set<VideoConferenceEmployee> vcs =
        new HashSet<>(Arrays.asList(vce1, vce2));

    final List<VideoConferenceEmployeeDto> vcsDtos =
        toVideoConferenceEmployeeDtos(vcs);

    final AtomicReference<VideoConferenceEmployee> vcsExpected =
        new AtomicReference<>();

    vcsDtos.forEach(vcsActual -> {
      vcsExpected.set(new VideoConferenceEmployee(
          new VideoConferenceEmployeeKey(idEmployee, vcsActual.getIdVcsystem()),
          vcsActual.getData()));
      assertTrue(vcs.contains(vcsExpected.get()));
    });
  }
}
