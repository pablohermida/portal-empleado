package es.udc.fic.pablohermida.portalempleado.test.unit.model.entities;

import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Employee;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.EmployeeAuth;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.EmployeeSecurityData;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Job;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Role;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.VideoConferenceSystem;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * The type Employee entities test.
 */
@SpringBootTest
@DisplayName("Employee Entities")
public class EmployeeEntitiesTest {

  private List<Job> jobs;

  private List<Role> roles;

  private static List<Role> createRoles(final String... names) {
    Role role;
    final List<Role> roles = new ArrayList<>();
    for (final String name : names) {
      role = new Role();
      role.setName(name);
      roles.add(role);
    }
    return roles;
  }

  private static List<Job> createJobs(final String... names) {
    Job job;
    final List<Job> jobs = new ArrayList<>();
    for (final String name : names) {
      job = new Job();
      job.setName(name);
      jobs.add(job);
    }
    return jobs;
  }

  private Employee.EmployeeBuilder createTestEmployee1() {
    final Employee.EmployeeBuilder employee = new Employee.EmployeeBuilder();
    employee.name("Name").lastname("Lastname").image("src/image.jpg")
        .dateBirth(LocalDate.now()).email("email@email.es").dni("47386285Y")
        .address("Address").addressWork("Address Work").phone("123456789")
        .duration("Duration").dateStart(LocalDate.now()).workDay("Work Day")
        .workHours("Work Hours").job(this.jobs.get(0)).role(this.roles.get(0))
        .employeeAuth(
            new EmployeeAuth("username", StringUtils.repeat("a", 96), null))
        .employeeSecurityData(
            new EmployeeSecurityData(StringUtils.repeat("a", 24), true));

    return employee;
  }

  private Employee.EmployeeBuilder createTestEmployee2() {
    final Employee.EmployeeBuilder employee = new Employee.EmployeeBuilder();
    employee.name("test").lastname("test").image("src/test.jpg")
        .dateBirth(LocalDate.now()).email("test@test.es").dni("47386284M")
        .address("test").addressWork("Test Work").phone("1234567890")
        .duration("test").dateStart(LocalDate.now().plusDays(1))
        .workDay("Test Day").workHours("Test Hours").job(this.jobs.get(1))
        .role(this.roles.get(1)).employeeAuth(
        new EmployeeAuth("test", StringUtils.repeat("b", 96), null))
        .employeeSecurityData(
            new EmployeeSecurityData(StringUtils.repeat("b", 24), true));

    return employee;
  }

  /**
   * Sets up.
   */
  @BeforeEach
  void setUp() {
    this.jobs = createJobs("JOB1", "JOB2", "JOB3");
    this.roles = createRoles("ADMIN", "EMPLOYEE");
  }

  /**
   * Equals role contract.
   */
  @Test
  @DisplayName("Role Equals")
  public void equalsRoleContract() {
    EqualsVerifier.forClass(Role.class).usingGetClass().verify();
  }

  /**
   * Equals job contract.
   */
  @Test
  @DisplayName("Job Equals")
  public void equalsJobContract() {
    EqualsVerifier.forClass(Job.class).usingGetClass().verify();
  }

  /**
   * Equals employee contract.
   */
  @Test
  @DisplayName("Employee Equals")
  public void equalsEmployeeContract() {
    EqualsVerifier.forClass(Employee.class)
        .withPrefabValues(Employee.class, this.createTestEmployee1().build(),
            this.createTestEmployee2().build()).usingGetClass().verify();
  }

  /**
   * Equals employee auth contract.
   */
  @Test
  @DisplayName("Employee Auth Equals")
  public void equalsEmployeeAuthContract() {
    EqualsVerifier.forClass(EmployeeAuth.class)
        .withPrefabValues(Employee.class, this.createTestEmployee1().build(),
            this.createTestEmployee2().build()).usingGetClass()
        .withIgnoredFields("employee").verify();
  }

  /**
   * Equals employee security data contract.
   */
  @Test
  @DisplayName("Employee Security Data Equals")
  public void equalsEmployeeSecurityDataContract() {
    EqualsVerifier.forClass(EmployeeSecurityData.class)
        .withPrefabValues(Employee.class, this.createTestEmployee1().build(),
            this.createTestEmployee2().build()).usingGetClass()
        .withIgnoredFields("employee").verify();
  }

  /**
   * Equals video conference system contract.
   */
  @Test
  @DisplayName("VideoConference System Equals")
  public void equalsVideoConferenceSystemContract() {
    EqualsVerifier.forClass(VideoConferenceSystem.class).usingGetClass()
        .verify();
  }
}
