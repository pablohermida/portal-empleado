package es.udc.fic.pablohermida.portalempleado.test.unit.rest.dtos;

import es.udc.fic.pablohermida.portalempleado.rest.dtos.IdDto;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * The type Id dto test.
 */
@SpringBootTest
@DisplayName("Id Dto")
public class IdDtoTest {
  /**
   * Equals video conference system dto contract.
   */
  @Test
  @DisplayName("Id Dto Equals")
  public void equalsVideoConferenceSystemDtoContract() {
    EqualsVerifier.forClass(IdDto.class).usingGetClass().verify();
  }
}
