package es.udc.fic.pablohermida.portalempleado.test.unit.utils.validators;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Employee;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.EmployeeAuth;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.EmployeeSecurityData;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Job;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Role;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.FieldErrorException;
import es.udc.fic.pablohermida.portalempleado.utils.validators.EmployeeValidator;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * The type Employee validator test.
 */
@SpringBootTest
@DisplayName("Employee Validator")
public class EmployeeValidatorTest {
  private static Employee.EmployeeBuilder createTestEmployee() {
    final Employee.EmployeeBuilder employee = new Employee.EmployeeBuilder();
    employee.name("Name").lastname("Lastname").image("src/image.jpg")
        .dateBirth(LocalDate.now()).email("email@email.es").dni("47386285Y")
        .address("Address").addressWork("Address Work").phone("123456789")
        .duration("Duration").dateStart(LocalDate.now()).workDay("Work Day")
        .workHours("Work Hours").job(new Job(1L, "JOB"))
        .role(new Role(1L, "ROLE")).employeeAuth(
        new EmployeeAuth("username", StringUtils.repeat("a", 96), null))
        .employeeSecurityData(
            new EmployeeSecurityData(StringUtils.repeat("a", 24), true));

    return employee;
  }

  private static List<String> createArrayStringTestCase(final String... tests) {
    final List<String> strings = new ArrayList<>();
    for (int i = 0; i < tests.length; i++) {
      strings.add(tests[i]);
    }
    return strings;
  }

  /**
   * When initialize validation is correct.
   *
   * @throws FieldErrorException the field error exception
   */
  @Test
  @DisplayName("Intialize Employee Validator")
  public void whenInitializeValidationIsCorrect() throws FieldErrorException {
    final EmployeeValidator validator = new EmployeeValidator();
    assertNotNull(validator);
  }

  /**
   * When employee is valid then validate is correct.
   *
   * @throws FieldErrorException the field error exception
   */
  @Test
  @DisplayName("All Validate Correct")
  public void whenEmployeeIsValidThenValidateIsCorrect()
      throws FieldErrorException {
    final Employee.EmployeeBuilder employee = createTestEmployee();
    EmployeeValidator.validate(employee.build());
    assertTrue(true);
  }

  /**
   * When employee name is not valid then validate is incorrect.
   */
  @Test
  @DisplayName("Validate Name Incorrect")
  public void whenEmployeeNameIsNotValidThenValidateIsIncorrect() {
    final Employee.EmployeeBuilder employee = createTestEmployee();
    final List<String> tests = EmployeeValidatorTest
        .createArrayStringTestCase(null, "", "a", StringUtils.repeat('a', 31));

    tests.forEach(s -> {
      employee.name(s);
      assertThrows(FieldErrorException.class, () -> {
        EmployeeValidator.validate(employee.build());
      });
    });
  }

  /**
   * When employee last name is not valid then validate is incorrect.
   */
  @Test
  @DisplayName("Validate Lastname Incorrect")
  public void whenEmployeeLastNameIsNotValidThenValidateIsIncorrect() {
    final Employee.EmployeeBuilder employee = createTestEmployee();
    final List<String> tests = EmployeeValidatorTest
        .createArrayStringTestCase(null, "", "a", StringUtils.repeat('a', 31));

    tests.forEach(s -> {
      employee.lastname(s);
      assertThrows(FieldErrorException.class, () -> {
        EmployeeValidator.validate(employee.build());
      });
    });
  }

  /**
   * When employee image is not valid then validate is incorrect.
   */
  @Test
  @DisplayName("Validate Image Incorrect")
  public void whenEmployeeImageIsNotValidThenValidateIsIncorrect() {
    final Employee.EmployeeBuilder employee = createTestEmployee();
    final List<String> tests = EmployeeValidatorTest
        .createArrayStringTestCase("", StringUtils.repeat('a', 256));

    tests.forEach(s -> {
      employee.image(s);
      assertThrows(FieldErrorException.class, () -> {
        EmployeeValidator.validate(employee.build());
      });
    });
  }

  /**
   * When employee date birth is not valid then validate is incorrect.
   */
  @Test
  @DisplayName("Validate Date Birth Incorrect")
  public void whenEmployeeDateBirthIsNotValidThenValidateIsIncorrect() {
    final Employee.EmployeeBuilder employee = createTestEmployee();
    employee.dateBirth(null);
    assertThrows(FieldErrorException.class, () -> {
      EmployeeValidator.validate(employee.build());
    });
    employee.dateBirth(LocalDate.now().plusDays(1));
    assertThrows(FieldErrorException.class, () -> {
      EmployeeValidator.validate(employee.build());
    });
  }

  /**
   * When employee email is not valid then validate is incorrect.
   */
  @Test
  @DisplayName("Validate Email Incorrect")
  public void whenEmployeeEmailIsNotValidThenValidateIsIncorrect() {
    final Employee.EmployeeBuilder employee = createTestEmployee();
    final List<String> tests =
        createArrayStringTestCase(null, "", "a@a.e", "@test.com", "test.com@es",
            ".com@test", "test .@com");

    tests.forEach(s -> {
      employee.email(s);
      assertThrows(FieldErrorException.class, () -> {
        EmployeeValidator.validate(employee.build());
      });
    });
  }

  /**
   * When employee dni is not valid then validate is incorrect.
   */
  @Test
  @DisplayName("Validate Dni Incorrect")
  public void whenEmployeeDniIsNotValidThenValidateIsIncorrect() {
    final Employee.EmployeeBuilder employee = createTestEmployee();
    final List<String> tests =
        createArrayStringTestCase(null, "", StringUtils.repeat("a", 8),
            StringUtils.repeat("a", 10), "12345678A", "A12345678");

    tests.forEach(s -> {
      employee.dni(s);
      assertThrows(FieldErrorException.class, () -> {
        EmployeeValidator.validate(employee.build());
      });
    });
  }

  /**
   * When employee address is not valid then validate is incorrect.
   */
  @Test
  @DisplayName("Validate Address Incorrect")
  public void whenEmployeeAddressIsNotValidThenValidateIsIncorrect() {
    final Employee.EmployeeBuilder employee = createTestEmployee();
    final List<String> tests = EmployeeValidatorTest
        .createArrayStringTestCase(null, "", StringUtils.repeat('a', 101));

    tests.forEach(s -> {
      employee.address(s);
      assertThrows(FieldErrorException.class, () -> {
        EmployeeValidator.validate(employee.build());
      });
    });
  }

  /**
   * When employee address work is not valid then validate is incorrect.
   */
  @Test
  @DisplayName("Validate Address Work Incorrect")
  public void whenEmployeeAddressWorkIsNotValidThenValidateIsIncorrect() {
    final Employee.EmployeeBuilder employee = createTestEmployee();
    final List<String> tests = EmployeeValidatorTest
        .createArrayStringTestCase(null, "", StringUtils.repeat('a', 101));

    tests.forEach(s -> {
      employee.addressWork(s);
      assertThrows(FieldErrorException.class, () -> {
        EmployeeValidator.validate(employee.build());
      });
    });
  }

  /**
   * When employee phone is not valid then validate is incorrect.
   */
  @Test
  @DisplayName("Validate Phone Incorrect")
  public void whenEmployeePhoneIsNotValidThenValidateIsIncorrect() {
    final Employee.EmployeeBuilder employee = createTestEmployee();
    final List<String> tests = EmployeeValidatorTest
        .createArrayStringTestCase(null, "", "12346578",
            StringUtils.repeat('1', 13));

    tests.forEach(s -> {
      employee.phone(s);
      assertThrows(FieldErrorException.class, () -> {
        EmployeeValidator.validate(employee.build());
      });
    });
  }

  /**
   * When employee duration is not valid then validate is incorrect.
   */
  @Test
  @DisplayName("Validate Duration Incorrect")
  public void whenEmployeeDurationIsNotValidThenValidateIsIncorrect() {
    final Employee.EmployeeBuilder employee = createTestEmployee();
    final List<String> tests = EmployeeValidatorTest
        .createArrayStringTestCase(null, "", StringUtils.repeat('a', 11));

    tests.forEach(s -> {
      employee.duration(s);
      assertThrows(FieldErrorException.class, () -> {
        EmployeeValidator.validate(employee.build());
      });
    });
  }

  /**
   * When employee date start is not valid then validate is incorrect.
   */
  @Test
  @DisplayName("Validate Date Start Incorrect")
  public void whenEmployeeDateStartIsNotValidThenValidateIsIncorrect() {
    final Employee.EmployeeBuilder employee = createTestEmployee();

    employee.dateStart(null);
    assertThrows(FieldErrorException.class, () -> {
      EmployeeValidator.validate(employee.build());
    });
  }

  /**
   * When employee work day is not valid then validate is incorrect.
   */
  @Test
  @DisplayName("Validate Work Day Incorrect")
  public void whenEmployeeWorkDayIsNotValidThenValidateIsIncorrect() {
    final Employee.EmployeeBuilder employee = createTestEmployee();
    final List<String> tests = EmployeeValidatorTest
        .createArrayStringTestCase(null, "", StringUtils.repeat('a', 31));

    tests.forEach(s -> {
      employee.workDay(s);
      assertThrows(FieldErrorException.class, () -> {
        EmployeeValidator.validate(employee.build());
      });
    });
  }

  /**
   * When employee work hours is not valid then validate is incorrect.
   */
  @Test
  @DisplayName("Validate Work Hours Incorrect")
  public void whenEmployeeWorkHoursIsNotValidThenValidateIsIncorrect() {
    final Employee.EmployeeBuilder employee = createTestEmployee();
    final List<String> tests = EmployeeValidatorTest
        .createArrayStringTestCase(null, "", StringUtils.repeat('a', 301));

    tests.forEach(s -> {
      employee.workHours(s);
      assertThrows(FieldErrorException.class, () -> {
        EmployeeValidator.validate(employee.build());
      });
    });
  }

  /**
   * When employee role is not valid then validate is incorrect.
   */
  @Test
  @DisplayName("Validate Role Incorrect")
  public void whenEmployeeRoleIsNotValidThenValidateIsIncorrect() {
    final Employee.EmployeeBuilder employee = createTestEmployee();

    employee.role(null);
    assertThrows(FieldErrorException.class, () -> {
      EmployeeValidator.validate(employee.build());
    });
  }

  /**
   * When employee job is not valid then validate is incorrect.
   */
  @Test
  @DisplayName("Validate Job Incorrect")
  public void whenEmployeeJobIsNotValidThenValidateIsIncorrect() {
    final Employee.EmployeeBuilder employee = createTestEmployee();

    employee.job(null);
    assertThrows(FieldErrorException.class, () -> {
      EmployeeValidator.validate(employee.build());
    });
  }

  /**
   * When employee auth is not valid then validate is incorrect.
   */
  @Test
  @DisplayName("Validate Employee Auth Incorrect")
  public void whenEmployeeAuthIsNotValidThenValidateIsIncorrect() {
    final Employee.EmployeeBuilder employee = createTestEmployee();

    employee.employeeAuth(null);
    assertThrows(FieldErrorException.class, () -> {
      EmployeeValidator.validate(employee.build());
    });

    employee.employeeAuth(new EmployeeAuth("", "", null));
    assertThrows(FieldErrorException.class, () -> {
      EmployeeValidator.validate(employee.build());
    });

    employee.employeeAuth(new EmployeeAuth("a", "", null));
    assertThrows(FieldErrorException.class, () -> {
      EmployeeValidator.validate(employee.build());
    });

    employee
        .employeeAuth(new EmployeeAuth(StringUtils.repeat("a", 31), "", null));
    assertThrows(FieldErrorException.class, () -> {
      EmployeeValidator.validate(employee.build());
    });

    employee.employeeAuth(
        new EmployeeAuth("aaa", StringUtils.repeat("a", 59), null));
    assertThrows(FieldErrorException.class, () -> {
      EmployeeValidator.validate(employee.build());
    });

    employee.employeeAuth(
        new EmployeeAuth("aaa", StringUtils.repeat("a", 97), null));
    assertThrows(FieldErrorException.class, () -> {
      EmployeeValidator.validate(employee.build());
    });
  }

  /**
   * When employee security data is not valid then validate is incorrect.
   */
  @Test
  @DisplayName("Validate Employee Security Data Incorrect")
  public void whenEmployeeSecurityDataIsNotValidThenValidateIsIncorrect() {
    final Employee.EmployeeBuilder employee = createTestEmployee();

    employee.employeeSecurityData(null);
    assertThrows(FieldErrorException.class, () -> {
      EmployeeValidator.validate(employee.build());
    });

    employee.employeeSecurityData(new EmployeeSecurityData("", true));
    assertThrows(FieldErrorException.class, () -> {
      EmployeeValidator.validate(employee.build());
    });

    employee.employeeSecurityData(
        new EmployeeSecurityData(StringUtils.repeat("a", 21), true));
    assertThrows(FieldErrorException.class, () -> {
      EmployeeValidator.validate(employee.build());
    });

    employee.employeeSecurityData(
        new EmployeeSecurityData(StringUtils.repeat("a", 25), true));
    assertThrows(FieldErrorException.class, () -> {
      EmployeeValidator.validate(employee.build());
    });
  }
}
