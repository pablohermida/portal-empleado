package es.udc.fic.pablohermida.portalempleado.test.integration.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import es.udc.fic.pablohermida.portalempleado.rest.controllers.VideoConferenceSystemController;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceSystemCreateDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceSystemDto;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.FieldErrorException;
import java.util.List;
import javax.management.InstanceAlreadyExistsException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

/**
 * The type Video conference system controller it.
 */
@SpringBootTest
@ActiveProfiles("test")
@DisplayName("VideoConference System Controller Integration")
@Transactional
public class VideoConferenceSystemControllerIt {
  @Autowired
  private VideoConferenceSystemController vceController;

  /**
   * Add video conference system test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   */
  @Test
  @DisplayName("Add videoconference system and get all")
  public void addVideoConferenceSystemTest()
      throws FieldErrorException, InstanceAlreadyExistsException {
    final String platform = "test";
    final VideoConferenceSystemCreateDto vceDto =
        new VideoConferenceSystemCreateDto(platform);

    this.vceController.addVideoConferenceSystem(vceDto);

    final List<VideoConferenceSystemDto> vcsList =
        this.vceController.getAllVideoConferenceSystem();

    for (final VideoConferenceSystemDto vcsDto : vcsList) {
      assertEquals(platform, vcsDto.getPlatform());
    }
  }

  /**
   * Add video conference system field error test.
   */
  @Test
  @DisplayName("Add videoconference system field error")
  public void addVideoConferenceSystemFieldErrorTest() {
    final VideoConferenceSystemCreateDto vceDto =
        new VideoConferenceSystemCreateDto("a");

    assertThrows(FieldErrorException.class, () -> {
      this.vceController.addVideoConferenceSystem(vceDto);
    });
  }

  /**
   * Add video conference system already exists test.
   *
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws FieldErrorException            the field error exception
   */
  @Test
  @DisplayName("Add videoconference system already exists")
  public void addVideoConferenceSystemAlreadyExistsTest()
      throws InstanceAlreadyExistsException, FieldErrorException {
    final VideoConferenceSystemCreateDto vceDto =
        new VideoConferenceSystemCreateDto("test");

    this.vceController.addVideoConferenceSystem(vceDto);

    assertThrows(InstanceAlreadyExistsException.class, () -> {
      this.vceController.addVideoConferenceSystem(vceDto);
    });
  }
}
