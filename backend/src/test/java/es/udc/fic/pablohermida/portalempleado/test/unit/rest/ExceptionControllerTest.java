package es.udc.fic.pablohermida.portalempleado.test.unit.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import es.udc.fic.pablohermida.portalempleado.rest.controllers.ExceptionController;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.ErrorsDto;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.AlreadyUnsuscribeException;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.FieldErrorException;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.FieldErrorReason;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.NotImplementedException;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.PasswordNotMatchException;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.PermissionException;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * The type Exception controller test.
 */
@SpringBootTest
@DisplayName("Exception Controller")
public class ExceptionControllerTest {
  /**
   * Handle not implemented exception test.
   */
  @Test
  @DisplayName("Not Implemented Exception")
  public void handleNotImplementedExceptionTest() {
    final String CODE = "exceptions.NotImplementedException";
    final String MESSAGE = "test";
    final NotImplementedException exception =
        new NotImplementedException(MESSAGE);
    final ErrorsDto errorExpected = new ErrorsDto(CODE, exception.getMessage());
    final ErrorsDto errorActual = ExceptionController
        .handleNotImplementedException(new NotImplementedException(MESSAGE));

    assertEquals(errorExpected, errorActual);
  }

  /**
   * Handle instance not found exception test.
   */
  @Test
  @DisplayName("Instance Not Found Exception")
  public void handleInstanceNotFoundExceptionTest() {
    final String CODE = "exceptions.InstanceNotFoundException";
    final String MESSAGE = "test";
    final InstanceNotFoundException exception =
        new InstanceNotFoundException(MESSAGE);
    final ErrorsDto errorExpected = new ErrorsDto(CODE, exception.getMessage());
    final ErrorsDto errorActual = ExceptionController
        .handleInstanceNotFoundException(
            new InstanceNotFoundException(MESSAGE));

    assertEquals(errorExpected, errorActual);
  }

  /**
   * Handle instance already exists exception test.
   */
  @Test
  @DisplayName("Instance Already Exists Exception")
  public void handleInstanceAlreadyExistsExceptionTest() {
    final String CODE = "exceptions.InstanceAlreadyExistsException";
    final String MESSAGE = "test";
    final InstanceAlreadyExistsException exception =
        new InstanceAlreadyExistsException(MESSAGE);
    final ErrorsDto errorExpected = new ErrorsDto(CODE, exception.getMessage());
    final ErrorsDto errorActual = ExceptionController
        .handleInstanceAlreadyExistsException(
            new InstanceAlreadyExistsException(MESSAGE));

    assertEquals(errorExpected, errorActual);
  }

  /**
   * Handle field error exception test.
   */
  @Test
  @DisplayName("Field Error Exception")
  public void handleFieldErrorExceptionTest() {
    final String CODE = "exceptions.FieldErrorException";
    final String MESSAGE = "test";
    final FieldErrorException exception =
        new FieldErrorException(MESSAGE, FieldErrorReason.LENGTH);
    final ErrorsDto errorExpected = new ErrorsDto(CODE, exception.getMessage());
    final ErrorsDto errorActual = ExceptionController.handleFieldErrorException(
        new FieldErrorException(MESSAGE, FieldErrorReason.LENGTH));

    assertEquals(errorExpected, errorActual);
  }

  /**
   * Handle field error exception fail test.
   */
  @Test
  @DisplayName("Field Error Exception Fail")
  public void handleFieldErrorExceptionFailTest() {
    final String CODE = "exceptions.FieldErrorException";
    final String MESSAGE = "test";
    final FieldErrorException exception =
        new FieldErrorException(MESSAGE, FieldErrorReason.LENGTH);
    final ErrorsDto errorExpected = new ErrorsDto(CODE, exception.getMessage());
    final ErrorsDto errorActual = ExceptionController.handleFieldErrorException(
        new FieldErrorException(MESSAGE, FieldErrorReason.IS_BLANK));

    assertNotEquals(errorExpected, errorActual);
  }

  /**
   * Handle permission exception test.
   */
  @Test
  @DisplayName("Permission Exception")
  public void handlePermissionExceptionTest() {
    final String CODE = "exceptions.PermissionException";
    final PermissionException exception = new PermissionException();
    final ErrorsDto errorExpected = new ErrorsDto(CODE, exception.getMessage());
    final ErrorsDto errorActual = ExceptionController
        .handlePermissionException(new PermissionException());

    assertEquals(errorExpected, errorActual);
  }

  /**
   * Handle already unsuscribe exception test.
   */
  @Test
  @DisplayName("Already Unsuscribe Exception")
  public void handleAlreadyUnsuscribeExceptionTest() {
    final String CODE = "exceptions.AlreadyUnsuscribeException";
    final String USERNAME = "test";
    final AlreadyUnsuscribeException exception =
        new AlreadyUnsuscribeException(USERNAME);
    final ErrorsDto errorExpected = new ErrorsDto(CODE, exception.getMessage());
    final ErrorsDto errorActual = ExceptionController
        .handleAlreadyUnsuscribeException(
            new AlreadyUnsuscribeException(USERNAME));

    assertEquals(errorExpected, errorActual);
  }

  /**
   * Handle password not match exception test.
   */
  @Test
  @DisplayName("Password Not Match Exception")
  public void handlePasswordNotMatchExceptionTest() {
    final String CODE = "exceptions.PasswordNotMatchException";
    final PasswordNotMatchException exception = new PasswordNotMatchException();
    final ErrorsDto errorExpected = new ErrorsDto(CODE, exception.getMessage());
    final ErrorsDto errorActual = ExceptionController
        .handlePasswordNotMatchException(new PasswordNotMatchException());

    assertEquals(errorExpected, errorActual);
  }
}
