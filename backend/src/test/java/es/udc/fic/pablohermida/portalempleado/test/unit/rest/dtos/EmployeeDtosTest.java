package es.udc.fic.pablohermida.portalempleado.test.unit.rest.dtos;

import es.udc.fic.pablohermida.portalempleado.rest.dtos.ErrorsDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeAuthDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeFiltersDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeListDto;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * The type Employee dtos test.
 */
@SpringBootTest
@DisplayName("Employee Dtos")
public class EmployeeDtosTest {
  /**
   * Equals errors dtos contract.
   */
  @Test
  @DisplayName("Errors Dto Equals")
  public void equalsErrorsDtosContract() {
    EqualsVerifier.forClass(ErrorsDto.class).usingGetClass().verify();
  }

  /**
   * Equals errors dtos contract.
   */
  @Test
  @DisplayName("Employee Dto Equals")
  public void equalsEmployeeDtoContract() {
    EqualsVerifier.forClass(EmployeeDto.class).usingGetClass().verify();
  }

  /**
   * Equals employee auth dto contract.
   */
  @Test
  @DisplayName("Employee Auth Dto Equals")
  public void equalsEmployeeAuthDtoContract() {
    EqualsVerifier.forClass(EmployeeAuthDto.class).withIgnoredFields("token")
        .usingGetClass().verify();
  }

  /**
   * Equals employee list dto contract.
   */
  @Test
  @DisplayName("Employee List Dto Equals")
  public void equalsEmployeeListDtoContract() {
    EqualsVerifier.forClass(EmployeeListDto.class).usingGetClass().verify();
  }

  /**
   * Equals employee filters dto contract.
   */
  @Test
  @DisplayName("Employee Filters Dto Equals")
  public void equalsEmployeeFiltersDtoContract() {
    EqualsVerifier.forClass(EmployeeFiltersDto.class).usingGetClass().verify();
  }
}
