package es.udc.fic.pablohermida.portalempleado.test.integration.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import es.udc.fic.pablohermida.portalempleado.model.daos.employee.EmployeeDao;
import es.udc.fic.pablohermida.portalempleado.model.daos.employee.JobDao;
import es.udc.fic.pablohermida.portalempleado.model.daos.employee.RoleDao;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Employee;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.EmployeeAuth;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.EmployeeSecurityData;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Job;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Role;
import es.udc.fic.pablohermida.portalempleado.model.services.Block;
import es.udc.fic.pablohermida.portalempleado.rest.controllers.EmployeeController;
import es.udc.fic.pablohermida.portalempleado.rest.controllers.VideoConferenceSystemController;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.IdDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeAuthDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeFiltersDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeListDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeLoginDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeSearchDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.JobDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.RoleDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceEmployeeDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceSystemCreateDto;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.AlreadyUnsuscribeException;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.FieldErrorException;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.PasswordNotMatchException;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.PermissionException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

/**
 * The type Employee controller it.
 */
@SpringBootTest
@ActiveProfiles("test")
@DisplayName("Employee Service Integration")
@Transactional
public class EmployeeControllerIt {
  private List<Role> initRoles;
  private List<Job> initJobs;

  @Autowired
  private EmployeeController employeeController;

  @Autowired
  private EmployeeDao employeeDao;

  @Autowired
  private RoleDao roleDao;

  @Autowired
  private JobDao jobDao;

  @Autowired
  private BCryptPasswordEncoder passwordEncoder;

  @Autowired
  private VideoConferenceSystemController vcsystemController;

  private static EmployeeDto createTestEmployee(final Long roleId,
                                                final Long jobId,
                                                final String name,
                                                final String supervisorLogin,
                                                final String password,
                                                final String username,
                                                final String dni) {
    return new EmployeeDto(name, "Hermida Mourelle", null,
        LocalDate.of(1994, 5, 25), "info@pablohermida.com", dni,
        "C/Prueba 15 2D 15006 A Coruña", "C/Prueba 15 2D 15006 A Coruña",
        "+34123456789", "Indefinido", LocalDate.of(2017, 1, 24),
        "Jornada completa", "Infinito", roleId, jobId, "test", supervisorLogin,
        username, password, "ES0000000000000000000000", null);
  }

  private Employee.EmployeeBuilder createTestEmployee(final String password) {
    final Employee.EmployeeBuilder employee = new Employee.EmployeeBuilder();
    employee.name("Name").lastname("Lastname").image("src/image.jpg")
        .dateBirth(LocalDate.now()).email("email@email.es").dni("12346578Z")
        .address("Address").addressWork("Address Work").phone("123456789")
        .duration("Duration").dateStart(LocalDate.now()).workDay("Work Day")
        .workHours("Work Hours").job(this.initJobs.get(0))
        .role(this.initRoles.get(0)).employeeAuth(
        new EmployeeAuth("username", StringUtils.repeat("a", 96), null))
        .employeeSecurityData(
            new EmployeeSecurityData(StringUtils.repeat("a", 24), true))
        .vcsystems(null);

    return employee;
  }

  private void setInitRoles() {
    this.initRoles = new ArrayList<>();
    this.initRoles.add(new Role("Admin"));
    this.initRoles.add(new Role("Employee"));

    for (final Role r : this.initRoles) {
      this.roleDao.save(r);
    }
  }

  private void setInitJobs() {
    this.initJobs = new ArrayList<>();
    this.initJobs.add(new Job("Job 1"));
    this.initJobs.add(new Job("Job 2"));

    for (final Job r : this.initJobs) {
      this.jobDao.save(r);
    }
  }

  /**
   * Sets up.
   *
   * @throws PasswordNotMatchException the password not match exception
   * @throws InstanceNotFoundException the instance not found exception
   */
  @BeforeEach
  void setUp() throws PasswordNotMatchException, InstanceNotFoundException {
    this.setInitRoles();
    this.setInitJobs();
  }

  /**
   * Gets all roles test.
   */
  @Test
  @DisplayName("Get All Roles")
  public void getAllRolesTest() {
    final List<RoleDto> roles = this.employeeController.getRoles();

    for (final RoleDto role : roles) {
      assertNotNull(role.getId());
      assertNotNull(role.getName());
    }

    assertEquals(this.initRoles.size(), roles.size());
  }

  /**
   * Gets all jobs test.
   */
  @Test
  @DisplayName("Get All Jobs")
  public void getAllJobsTest() {
    final List<JobDto> jobs = this.employeeController.getJobs();

    for (final JobDto job : jobs) {
      assertNotNull(job.getId());
      assertNotNull(job.getName());
    }

    assertEquals(this.initJobs.size(), jobs.size());
  }

  /**
   * Sign up test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("Sign Up")
  public void signUpTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException {
    final EmployeeAuthDto authDto = this.employeeController.signUp(
        EmployeeControllerIt.createTestEmployee(this.initRoles.get(0).getId(),
            this.initJobs.get(0).getId(), "Pablo", null, "test", "test",
            "47386285Y"));

    assertNotNull(authDto);
  }

  /**
   * Sign up field error test.
   */
  @Test
  @DisplayName("Sign Up Field Error")
  public void signUpFieldErrorTest() {
    assertThrows(FieldErrorException.class, () -> {
      this.employeeController.signUp(EmployeeControllerIt
          .createTestEmployee(this.initRoles.get(0).getId(),
              this.initJobs.get(0).getId(), "a", null, "test", "test",
              "47386285Y"));
    });
  }

  /**
   * Sign up role not found test.
   */
  @Test
  @DisplayName("Sign Up Role Not Found")
  public void signUpRoleNotFoundTest() {
    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeController.signUp(EmployeeControllerIt
          .createTestEmployee(-1L, this.initJobs.get(0).getId(), "Pablo", null,
              "test", "test", "47386285Y"));
    });
  }

  /**
   * Sign up job not found test.
   */
  @Test
  @DisplayName("Sign Up Job Not Found")
  public void signUpJobNotFoundTest() {
    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeController.signUp(EmployeeControllerIt
          .createTestEmployee(this.initRoles.get(0).getId(), -1L, "Pablo", null,
              "test", "test", "47386285Y"));
    });
  }

  /**
   * Sign up supervisor not found test.
   */
  @Test
  @DisplayName("Sign Up Supervisor Not Found")
  public void signUpSupervisorNotFoundTest() {
    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeController.signUp(EmployeeControllerIt
          .createTestEmployee(this.initRoles.get(0).getId(),
              this.initJobs.get(0).getId(), "Pablo", "test", "test", "test",
              "47386285Y"));
    });
  }

  /**
   * Sign up already exist test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("Sign Up Already Exists")
  public void signUpAlreadyExistTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException {
    this.employeeController.signUp(EmployeeControllerIt
        .createTestEmployee(this.initRoles.get(0).getId(),
            this.initJobs.get(0).getId(), "Pablo", null, "test", "test",
            "47386285Y"));

    assertThrows(InstanceAlreadyExistsException.class, () -> {
      this.employeeController.signUp(EmployeeControllerIt
          .createTestEmployee(this.initRoles.get(0).getId(),
              this.initJobs.get(0).getId(), "Pablo", null, "test", "test",
              "47386285Y"));
    });
  }

  /**
   * Search by user name.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("Get employees by Username")
  public void searchByUserName()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException {
    final EmployeeDto employeeInit = EmployeeControllerIt
        .createTestEmployee(this.initRoles.get(0).getId(),
            this.initJobs.get(0).getId(), "Pablo", null, "test", "test",
            "47386285Y");
    final EmployeeAuthDto authDto =
        this.employeeController.signUp(employeeInit);
    final List<EmployeeSearchDto> employees =
        this.employeeController.searchByUserName(authDto.getUsername());

    assertEquals(1, employees.size());

    for (final EmployeeSearchDto employee : employees) {
      assertEquals(employeeInit.getUsername(), employee.getUsername());
      assertEquals(employeeInit.getName() + " " + employeeInit.getLastname(),
          employee.getFullname());
    }
  }

  /**
   * Login test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   * @throws PasswordNotMatchException      the password not match exception
   */
  @Test
  @DisplayName("Login")
  public void loginTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException, PasswordNotMatchException {
    final EmployeeAuthDto authDto = this.employeeController.signUp(
        EmployeeControllerIt.createTestEmployee(this.initRoles.get(0).getId(),
            this.initJobs.get(0).getId(), "Pablo", null, "test", "test",
            "47386285Y"));

    final EmployeeAuthDto loginDto =
        this.employeeController.login(new EmployeeLoginDto("test", "test"));
    assertEquals(authDto, loginDto);
  }

  /**
   * Login password not match test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("Login Password Not Match")
  public void loginPasswordNotMatchTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException {
    final EmployeeAuthDto authDto = this.employeeController.signUp(
        EmployeeControllerIt.createTestEmployee(this.initRoles.get(0).getId(),
            this.initJobs.get(0).getId(), "Pablo", null, "test", "test",
            "47386285Y"));

    assertThrows(PasswordNotMatchException.class, () -> {
      this.employeeController.login(new EmployeeLoginDto("test", "fail"));
    });
  }

  /**
   * Login instance not found test.
   */
  @Test
  @DisplayName("Login Instance Not Found")
  public void loginInstanceNotFoundTest() {
    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeController.login(new EmployeeLoginDto("fail", "fail"));
    });
  }

  /**
   * Login from service token test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("Login From Service Token")
  public void loginFromServiceTokenTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException {
    final EmployeeAuthDto authDto = this.employeeController.signUp(
        EmployeeControllerIt.createTestEmployee(this.initRoles.get(0).getId(),
            this.initJobs.get(0).getId(), "Pablo", null, "test", "test",
            "47386285Y"));

    final Optional<Employee> empCreated =
        this.employeeDao.findByEmployeeAuthUsername(authDto.getUsername());
    final Long idCreated = empCreated.get().getId();

    final EmployeeAuthDto loginDto = this.employeeController
        .loginFromServiceToken(idCreated, authDto.getToken());

    assertEquals(authDto, loginDto);
  }

  /**
   * Login from service token instance not found test.
   */
  @Test
  @DisplayName("Login From Service Token Instance Not Found")
  public void loginFromServiceTokenInstanceNotFoundTest() {
    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeController.login(new EmployeeLoginDto("fail", "fail"));
    });
  }

  /**
   * Upload employee test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   * @throws PermissionException            the permission exception
   */
  @Test
  @DisplayName("Upload Employee and Get By Id")
  public void uploadEmployeeAndGetEmployeeByIdTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException, PermissionException {
    final EmployeeAuthDto authDto = this.employeeController.signUp(
        EmployeeControllerIt.createTestEmployee(this.initRoles.get(0).getId(),
            this.initJobs.get(0).getId(), "Pablo", null, "test", "test",
            "47386285Y"));
    final Optional<Employee> optEmployee =
        this.employeeDao.findByEmployeeAuthUsername(authDto.getUsername());
    final EmployeeDto employee = this.employeeController
        .getEmployeeById(optEmployee.get().getId(), optEmployee.get().getId());
    final EmployeeDto updateData =
        new EmployeeDto("edit", "edit", employee.getImage(),
            employee.getDateBirth(), employee.getEmail(), employee.getDni(),
            employee.getAddress(), employee.getAddressWork(),
            employee.getPhone(), employee.getDuration(),
            employee.getDateStart(), employee.getWorkDay(),
            employee.getWorkHours(), employee.getRoleId(), employee.getJobId(),
            employee.getJobName(), employee.getSupervisorLogin(),
            employee.getUsername(), "edit", employee.getIban(),
            employee.getVcsystems());
    this.employeeController
        .uploadEmployee(optEmployee.get().getId(), optEmployee.get().getId(),
            updateData);
    final EmployeeDto result = this.employeeController
        .getEmployeeById(optEmployee.get().getId(), optEmployee.get().getId());
    assertNotEquals(result, employee);
  }

  /**
   * Upload employee not exist test.
   */
  @Test
  @DisplayName("Upload Employee Not Exist")
  public void uploadEmployeeNotExistTest() {
    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeController.uploadEmployee(1L, 1L, null);
    });
  }

  /**
   * Upload employee forbidden test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("Upload Employee Forbidden")
  public void uploadEmployeeForbiddenTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException {
    final EmployeeAuthDto authDto = this.employeeController.signUp(
        EmployeeControllerIt.createTestEmployee(this.initRoles.get(0).getId(),
            this.initJobs.get(0).getId(), "Pablo", null, "test", "test",
            "47386285Y"));
    final Optional<Employee> optEmployee =
        this.employeeDao.findByEmployeeAuthUsername(authDto.getUsername());
    assertThrows(PermissionException.class, () -> {
      this.employeeController
          .uploadEmployee(optEmployee.get().getId(), 2L, null);
    });
  }

  /**
   * Upload vc system employee test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   * @throws PermissionException            the permission exception
   */
  @Test
  @DisplayName("Upload VC System Employee")
  public void uploadVcSystemEmployeeTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException, PermissionException {
    final EmployeeAuthDto authDto = this.employeeController.signUp(
        EmployeeControllerIt.createTestEmployee(this.initRoles.get(0).getId(),
            this.initJobs.get(0).getId(), "Pablo", null, "test", "test",
            "47386285Y"));
    final Optional<Employee> optEmployee =
        this.employeeDao.findByEmployeeAuthUsername(authDto.getUsername());
    final EmployeeDto employee = this.employeeController
        .getEmployeeById(optEmployee.get().getId(), optEmployee.get().getId());
    final IdDto vcSystemId = this.vcsystemController
        .addVideoConferenceSystem(new VideoConferenceSystemCreateDto("test"));
    final VideoConferenceEmployeeDto vcSystemEmployee =
        new VideoConferenceEmployeeDto(vcSystemId.getId(), "test", "test");
    employee.getVcsystems().add(vcSystemEmployee);
    final EmployeeDto updateData =
        new EmployeeDto("edit", "edit", employee.getImage(),
            employee.getDateBirth(), employee.getEmail(), employee.getDni(),
            employee.getAddress(), employee.getAddressWork(),
            employee.getPhone(), employee.getDuration(),
            employee.getDateStart(), employee.getWorkDay(),
            employee.getWorkHours(), employee.getRoleId(), employee.getJobId(),
            employee.getJobName(), employee.getSupervisorLogin(),
            employee.getUsername(), "edit", employee.getIban(),
            employee.getVcsystems());
    this.employeeController
        .uploadEmployee(optEmployee.get().getId(), optEmployee.get().getId(),
            updateData);
    final EmployeeDto result = this.employeeController
        .getEmployeeById(optEmployee.get().getId(), optEmployee.get().getId());
    assertNotEquals(result, employee);
  }

  /**
   * List filter employee test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("List Employee Filters")
  public void listFilterEmployeeTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException {
    final EmployeeAuthDto authDto = this.employeeController.signUp(
        EmployeeControllerIt.createTestEmployee(this.initRoles.get(0).getId(),
            this.initJobs.get(0).getId(), "Pablo", null, "test", "test",
            "47386285Y"));

    final Block<EmployeeListDto> result = this.employeeController
        .getEmployeeByFilters(
            new EmployeeFiltersDto(null, null, null, null, null, null, null),
            0);

    assertFalse(result.getExistMoreItems());
    assertEquals(result.getTotalPages(), 1);
    for (final EmployeeListDto employee : result.getItems()) {
      assertEquals(employee.getUsername(), authDto.getUsername());
    }
  }

  /**
   * Unsuscribe employee test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   * @throws AlreadyUnsuscribeException     the already unsuscribe exception
   */
  @Test
  @DisplayName("Unsuscribe Employee")
  public void unsuscribeEmployeeTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException, AlreadyUnsuscribeException {
    final EmployeeAuthDto authDto = this.employeeController.signUp(
        EmployeeControllerIt.createTestEmployee(this.initRoles.get(0).getId(),
            this.initJobs.get(0).getId(), "Pablo", null, "test", "test",
            "47386285Y"));
    Optional<Employee> optEmployee =
        this.employeeDao.findByEmployeeAuthUsername(authDto.getUsername());

    this.employeeController.unsuscribeEmployee(optEmployee.get().getId());
    optEmployee =
        this.employeeDao.findByEmployeeAuthUsername(authDto.getUsername());

    assertFalse(optEmployee.get().getEmployeeSecurityData().isSuscribed());
  }

  /**
   * Unsuscribe employee instance not found test.
   */
  @Test
  @DisplayName("Unsuscribe Employee Not Exist")
  public void unsuscribeEmployeeInstanceNotFoundTest() {
    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeController.unsuscribeEmployee(1L);
    });
  }

  /**
   * Unsuscribe employee already unsuscribe exception test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   * @throws AlreadyUnsuscribeException     the already unsuscribe exception
   */
  @Test
  @DisplayName("Unsuscribe Employee Unsuscribed")
  public void unsuscribeEmployeeAlreadyUnsuscribeExceptionTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException, AlreadyUnsuscribeException {
    final EmployeeAuthDto authDto = this.employeeController.signUp(
        EmployeeControllerIt.createTestEmployee(this.initRoles.get(0).getId(),
            this.initJobs.get(0).getId(), "Pablo", null, "test", "test",
            "47386285Y"));
    final Optional<Employee> optEmployee =
        this.employeeDao.findByEmployeeAuthUsername(authDto.getUsername());

    this.employeeController.unsuscribeEmployee(optEmployee.get().getId());

    assertThrows(AlreadyUnsuscribeException.class, () -> {
      this.employeeController.unsuscribeEmployee(optEmployee.get().getId());
    });
  }
}
