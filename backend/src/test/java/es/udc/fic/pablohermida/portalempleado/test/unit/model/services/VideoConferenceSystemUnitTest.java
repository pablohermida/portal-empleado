package es.udc.fic.pablohermida.portalempleado.test.unit.model.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import es.udc.fic.pablohermida.portalempleado.model.daos.employee.VideoConferenceSystemDao;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.VideoConferenceSystem;
import es.udc.fic.pablohermida.portalempleado.model.services.employee.VideoConferenceSystemServiceImpl;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.FieldErrorException;
import java.util.ArrayList;
import java.util.List;
import javax.management.InstanceAlreadyExistsException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * The type Video conference system unit test.
 */
@SpringBootTest
@DisplayName("VideoConference System Service")
public class VideoConferenceSystemUnitTest {
  @InjectMocks
  private VideoConferenceSystemServiceImpl vcsystemService;

  @Mock
  private VideoConferenceSystemDao vcsystemDao;

  /**
   * Sets up.
   */
  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  /**
   * When create video conference system then is correct.
   *
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws FieldErrorException            the field error exception
   */
  @Test
  @DisplayName("Create VCS Correct")
  public void whenCreateVideoConferenceSystemThenIsCorrect()
      throws InstanceAlreadyExistsException, FieldErrorException {
    final String platformTest = "test";
    final VideoConferenceSystem vcsystemTest =
        new VideoConferenceSystem(platformTest);
    Mockito.when(
        this.vcsystemDao.existsByPlatformIgnoreCaseContaining(platformTest))
        .thenReturn(false);
    Mockito.when(this.vcsystemDao.save(vcsystemTest)).thenReturn(Mockito.any());
    this.vcsystemService.createVideoConferenceSystem(vcsystemTest);
    Mockito.verify(this.vcsystemDao)
        .existsByPlatformIgnoreCaseContaining(platformTest);
    Mockito.verify(this.vcsystemDao).save(vcsystemTest);
  }

  /**
   * When create video conference system then platform is already exist.
   */
  @Test
  @DisplayName("Create VCS Already Exists")
  public void whenCreateVideoConferenceSystemThenPlatformIsAlreadyExist() {
    final String platformTest = "test";
    final VideoConferenceSystem vcsystemTest =
        new VideoConferenceSystem(platformTest);
    Mockito.when(
        this.vcsystemDao.existsByPlatformIgnoreCaseContaining(platformTest))
        .thenReturn(true);
    assertThrows(InstanceAlreadyExistsException.class, () -> {
      this.vcsystemService.createVideoConferenceSystem(vcsystemTest);
    });
    Mockito.verify(this.vcsystemDao)
        .existsByPlatformIgnoreCaseContaining(platformTest);
  }

  /**
   * When create video conference system then platform has field error
   * exception.
   */
  @Test
  @DisplayName("Create VCS Field Error Exception")
  public void whenCreateVideoConferenceSystemThenPlatformHasFieldErrorException() {
    final String platformTest = "a";
    final VideoConferenceSystem vcsystemTest =
        new VideoConferenceSystem(platformTest);
    assertThrows(FieldErrorException.class, () -> {
      this.vcsystemService.createVideoConferenceSystem(vcsystemTest);
    });
  }

  /**
   * When get video conference system then return list platforms.
   */
  @Test
  @DisplayName("Get all VCS")
  public void whenGetVideoConferenceSystemThenReturnListPlatforms() {
    final String platformTest = "test";
    final List<VideoConferenceSystem> vcsystems = new ArrayList<>();
    vcsystems.add(new VideoConferenceSystem(platformTest));
    Mockito.when(this.vcsystemDao.findAllByOrderByPlatform())
        .thenReturn(vcsystems);
    final List<VideoConferenceSystem> vcSystemsResult =
        this.vcsystemService.getVideoConferenceSystems();
    assertEquals(vcsystems, vcSystemsResult);
    Mockito.verify(this.vcsystemDao).findAllByOrderByPlatform();
  }
}
