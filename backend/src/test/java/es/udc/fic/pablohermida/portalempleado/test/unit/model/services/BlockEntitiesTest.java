package es.udc.fic.pablohermida.portalempleado.test.unit.model.services;

import es.udc.fic.pablohermida.portalempleado.model.services.Block;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * The type Block entities test.
 */
@SpringBootTest
@DisplayName("Block Entities")
public class BlockEntitiesTest {
  /**
   * Equals employee filter contract.
   */
  @Test
  @DisplayName("Block Equals")
  public void equalsEmployeeFilterContract() {
    EqualsVerifier.forClass(Block.class).usingGetClass().verify();
  }
}
