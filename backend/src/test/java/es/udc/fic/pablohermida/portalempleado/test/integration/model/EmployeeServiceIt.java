package es.udc.fic.pablohermida.portalempleado.test.integration.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import es.udc.fic.pablohermida.portalempleado.model.daos.employee.EmployeeDao;
import es.udc.fic.pablohermida.portalempleado.model.daos.employee.JobDao;
import es.udc.fic.pablohermida.portalempleado.model.daos.employee.RoleDao;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Employee;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.EmployeeAuth;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.EmployeeSecurityData;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Job;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Role;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.VideoConferenceEmployee;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.VideoConferenceEmployeeKey;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.VideoConferenceSystem;
import es.udc.fic.pablohermida.portalempleado.model.services.Block;
import es.udc.fic.pablohermida.portalempleado.model.services.employee.EmployeeService;
import es.udc.fic.pablohermida.portalempleado.model.services.employee.VideoConferenceSystemService;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.AlreadyUnsuscribeException;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.FieldErrorException;
import es.udc.fic.pablohermida.portalempleado.utils.filters.EmployeeFilter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

/**
 * The type Employee service integration test.
 */
@SpringBootTest
@ActiveProfiles("test")
@DisplayName("Employee Service Integration")
@Transactional
public class EmployeeServiceIt {
  private List<Role> initRoles;
  private List<Job> initJobs;

  @Autowired
  private EmployeeService employeeService;

  @Autowired
  private VideoConferenceSystemService vcsystemService;

  @Autowired
  private EmployeeDao employeeDao;

  @Autowired
  private RoleDao roleDao;

  @Autowired
  private JobDao jobDao;

  /**
   * Create test employee.
   *
   * @return the test employee
   */
  private static Employee.EmployeeBuilder createTestEmployee() {
    final Employee.EmployeeBuilder employee = new Employee.EmployeeBuilder();
    employee.name("Name").lastname("Lastname").image("src/image.jpg")
        .dateBirth(LocalDate.now()).email("email@email.es").dni("47386285Y")
        .address("Address").addressWork("Address Work").phone("123456789")
        .duration("Duration").dateStart(LocalDate.now()).workDay("Work Day")
        .workHours("Work Hours").employeeAuth(
        new EmployeeAuth("username", StringUtils.repeat("a", 96), null))
        .employeeSecurityData(
            new EmployeeSecurityData(StringUtils.repeat("a", 24), true));

    return employee;
  }

  private void setInitRoles() {
    this.initRoles = new ArrayList<>();
    this.initRoles.add(new Role("Admin"));
    this.initRoles.add(new Role("Employee"));

    for (final Role r : this.initRoles) {
      this.roleDao.save(r);
    }
  }

  private void setInitJobs() {
    this.initJobs = new ArrayList<>();
    this.initJobs.add(new Job("Test job 1"));
    this.initJobs.add(new Job("Test job 2"));

    for (final Job j : this.initJobs) {
      this.jobDao.save(j);
    }
  }

  private void setInitVCSystems()
      throws InstanceAlreadyExistsException, FieldErrorException {
    final String platformTest = "test";
    final VideoConferenceSystem vcs = new VideoConferenceSystem(platformTest);
    this.vcsystemService.createVideoConferenceSystem(vcs);
  }

  /**
   * Sets up.
   *
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws FieldErrorException            the field error exception
   */
  @BeforeEach
  void setUp() throws InstanceAlreadyExistsException, FieldErrorException {
    this.setInitJobs();
    this.setInitRoles();
    this.setInitVCSystems();
  }

  /**
   * Sign up test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("SignUp")
  public void signUpTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException {
    final Employee.EmployeeBuilder employee = createTestEmployee();
    final Long employeeId;
    employee.role(this.initRoles.get(0));
    employee.job(this.initJobs.get(0));

    employeeId = this.employeeService.signUp(employee.build());

    assertNotNull(employeeId);
  }

  /**
   * Sign up with username already exist test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("SignUp Already Exist")
  public void signUpWithUsernameAlreadyExistTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException {
    final Employee.EmployeeBuilder employee = createTestEmployee();
    employee.role(this.initRoles.get(0));
    employee.job(this.initJobs.get(0));

    this.employeeService.signUp(employee.build());

    Assert.assertThrows(InstanceAlreadyExistsException.class, () -> {
      this.employeeService.signUp(employee.dni("12345678Z").build());
    });
  }

  /**
   * Sign up with super visor test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("SignUp With Supervisor")
  public void signUpWithSuperVisorTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException {
    final Employee.EmployeeBuilder supervisor = createTestEmployee();
    final Employee.EmployeeBuilder supervised = createTestEmployee();
    final Long supervisorId;
    final Long supervisedId;
    supervisor.role(this.initRoles.get(0));
    supervisor.job(this.initJobs.get(0));

    supervisorId = this.employeeService.signUp(supervisor.build());

    final List<Employee> s =
        this.employeeService.searchEmployeesByUsername("username");

    supervised.employeeAuth(
        new EmployeeAuth("supervised", StringUtils.repeat("a", 96), null));
    supervised.supervisor(s.get(0));
    supervised.dni("12345678Z");
    supervised.role(this.initRoles.get(0));
    supervised.job(this.initJobs.get(0));

    supervisedId = this.employeeService.signUp(supervised.build());

    assertNotNull(supervisedId);
  }

  /**
   * Find supervisor by username test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("Find Supervisor By Username")
  public void findSupervisorByUsernameTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException {
    Employee.EmployeeBuilder employee = createTestEmployee();
    employee.role(this.initRoles.get(0));
    employee.job(this.initJobs.get(0));

    this.employeeService.signUp(employee.build());
    employee = createTestEmployee();
    employee.role(this.initRoles.get(0));
    employee.job(this.initJobs.get(0));
    employee.employeeAuth(
        new EmployeeAuth("username2", StringUtils.repeat("a", 96), null));
    this.employeeService.signUp(employee.dni("12345678Z").build());
    employee = createTestEmployee();
    employee.role(this.initRoles.get(0));
    employee.job(this.initJobs.get(0));
    employee.employeeAuth(
        new EmployeeAuth("username3", StringUtils.repeat("a", 96), null));
    this.employeeService.signUp(employee.dni("87654321X").build());

    List<Employee> employeeFound =
        this.employeeService.searchEmployeesByUsername("username");
    assertEquals(3, employeeFound.size());

    employeeFound = this.employeeService.searchEmployeesByUsername("test");
    assertEquals(0, employeeFound.size());
  }

  /**
   * Find all roles test.
   */
  @Test
  @DisplayName("Find All Roles")
  public void findAllRolesTest() {
    final List<Role> roles = this.employeeService.getAllRoles();
    for (final Role r : roles) {
      assertTrue(this.initRoles.contains(r));
    }
  }

  /**
   * Find role by id test.
   *
   * @throws InstanceNotFoundException the instance not found exception
   */
  @Test
  @DisplayName("Find Role By Id")
  public void findRoleByIdTest() throws InstanceNotFoundException {
    final Long roleId = this.initRoles.get(0).getId();
    final Role role = this.employeeService.getRoleById(roleId);
    assertTrue(this.initRoles.contains(role));
  }

  /**
   * Find role by not exist id test.
   */
  @Test
  @DisplayName("Find By Not Exist Id")
  public void findRoleByNotExistIdTest() {
    final Long roleId = Long.MAX_VALUE;
    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeService.getRoleById(roleId);
    });
  }

  /**
   * Find all jobs test.
   */
  @Test
  @DisplayName("Find All Jobs")
  public void findAllJobsTest() {
    final List<Job> jobs = this.employeeService.getAllJobs();
    for (final Job j : jobs) {
      assertTrue(this.initJobs.contains(j));
    }
  }

  /**
   * Find job by id test.
   *
   * @throws InstanceNotFoundException the instance not found exception
   */
  @Test
  @DisplayName("Find Job By Id")
  public void findJobByIdTest() throws InstanceNotFoundException {
    final Long jobId = this.initJobs.get(0).getId();
    final Job job = this.employeeService.getJobById(jobId);
    assertTrue(this.initJobs.contains(job));
  }

  /**
   * Find job by not exist id test.
   */
  @Test
  @DisplayName("Find Job By Not Exist Id")
  public void findJobByNotExistIdTest() {
    final Long jobId = Long.MAX_VALUE;
    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeService.getJobById(jobId);
    });
  }

  /**
   * Sign up v csystem test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("SignUp with VideoConference System")
  public void signUpVCsystemTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException {
    final Employee.EmployeeBuilder employee = createTestEmployee();
    final List<VideoConferenceSystem> vcsystems =
        this.vcsystemService.getVideoConferenceSystems();
    final Long employeeId;

    employee.role(this.initRoles.get(0));
    employee.job(this.initJobs.get(0));

    final VideoConferenceEmployee vce = new VideoConferenceEmployee(
        new VideoConferenceEmployeeKey(null, vcsystems.get(0).getId()), "test");
    final List<VideoConferenceEmployee> vceList = Arrays.asList(vce);
    employee.vcsystems(new HashSet<>(vceList));

    final Employee expected = employee.build();

    employeeId = this.employeeService.signUp(expected);

    vce.setId(
        new VideoConferenceEmployeeKey(employeeId, vcsystems.get(0).getId()));

    final Optional<Employee> result = this.employeeDao.findById(employeeId);

    assertEquals(result.get().getVcsystems().stream().findFirst().get(), vce);
  }

  /**
   * Gets employee by username.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("Get Employee By Username")
  public void getEmployeeByUsername()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException {
    final Employee.EmployeeBuilder employee = createTestEmployee();
    employee.role(this.initRoles.get(0));
    employee.job(this.initJobs.get(0));

    final Employee createEmployee = employee.build();

    this.employeeService.signUp(createEmployee);

    final EmployeeAuth expected = createEmployee.getEmployeeAuth();

    final Employee result =
        this.employeeService.getEmployeeByUsername(expected.getUsername());

    assertEquals(createEmployee, result);
  }

  /**
   * Gets employee by username instance not found exception test.
   */
  @Test
  @DisplayName("Get Employee By Username If Not Exist")
  public void getEmployeeByUsernameInstanceNotFoundExceptionTest() {
    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeService.getEmployeeByUsername("test");
    });
  }

  /**
   * Gets employee by id.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("Get Employee By Id")
  public void getEmployeeById()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException {
    final Employee.EmployeeBuilder employee = createTestEmployee();
    employee.role(this.initRoles.get(0));
    employee.job(this.initJobs.get(0));

    final Employee createEmployee = employee.build();

    final Long employeeId = this.employeeService.signUp(createEmployee);

    final Employee result = this.employeeService.getEmployeeById(employeeId);

    assertEquals(createEmployee, result);
  }

  /**
   * Gets employee by id instance not found exception test.
   */
  @Test
  @DisplayName("Get Employee By Id If Not Exist")
  public void getEmployeeByIdInstanceNotFoundExceptionTest() {
    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeService.getEmployeeById(1L);
    });
  }

  /**
   * Edit employee test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("Edit Employee")
  public void editEmployeeTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException {
    final Employee.EmployeeBuilder employee = createTestEmployee();
    final Long employeeId;
    final String newName = "Name Edited";
    employee.role(this.initRoles.get(0));
    employee.job(this.initJobs.get(0));

    employeeId = this.employeeService.signUp(employee.build());

    final Employee.EmployeeBuilder employeeCreated = employee.id(employeeId);
    employeeCreated.name(newName);
    this.employeeService.editEmployee(employeeCreated.build());

    final Employee result = this.employeeService.getEmployeeById(employeeId);
    assertEquals(newName, result.getName());
  }

  /**
   * Edit employee field error test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("Edit Employee Field Error Exception")
  public void editEmployeeFieldErrorTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException {
    final Employee.EmployeeBuilder employee = createTestEmployee();
    final Long employeeId;
    final String newName = "a";
    employee.role(this.initRoles.get(0));
    employee.job(this.initJobs.get(0));

    employeeId = this.employeeService.signUp(employee.build());

    final Employee.EmployeeBuilder employeeCreated = employee.id(employeeId);
    employeeCreated.name(newName);
    assertThrows(FieldErrorException.class, () -> {
      this.employeeService.editEmployee(employeeCreated.build());
    });
  }

  /**
   * Edit employee with employee id not exist test.
   */
  @Test
  @DisplayName("Edit Employee Employee Id Not Exist")
  public void editEmployeeWithEmployeeIdNotExistTest() {
    final Employee.EmployeeBuilder employee = createTestEmployee();
    final Long employeeId = 1L;
    employee.role(this.initRoles.get(0));
    employee.job(this.initJobs.get(0));

    final Employee.EmployeeBuilder employeeCreated = employee.id(employeeId);
    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeService.editEmployee(employeeCreated.build());
    });
  }

  /**
   * List all employees paginated test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @Test
  @DisplayName("List All Employees Paginated")
  public void listAllEmployeesPaginatedTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException {
    final int total = 10;
    final int pageSize = 3;
    for (int i = 0; i < total; i++) {
      final Employee.EmployeeBuilder employee = createTestEmployee();
      employee.employeeAuth(
          new EmployeeAuth("test" + i, StringUtils.repeat("a", 96), null));
      employee.role(this.initRoles.get(0));
      employee.job(this.initJobs.get(0));

      this.employeeService.signUp(employee.build());
    }
    final Block<Employee> result = this.employeeService
        .searchEmployees(new EmployeeFilter(), PageRequest.of(0, pageSize));

    assertTrue(result.getExistMoreItems());
    assertEquals(Math.ceil((double) total / pageSize), result.getTotalPages());
    assertEquals(pageSize, result.getItems().size());
  }

  /**
   * Unsuscribe employee test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   * @throws AlreadyUnsuscribeException     the already unsuscribe exception
   */
  @Test
  @DisplayName("Unsuscribe Employee")
  public void unsuscribeEmployeeTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException, AlreadyUnsuscribeException {
    final Employee.EmployeeBuilder employee = createTestEmployee();
    final Long employeeId;
    employee.role(this.initRoles.get(0));
    employee.job(this.initJobs.get(0));

    employeeId = this.employeeService.signUp(employee.build());

    this.employeeService.unsuscribeEmployee(employeeId);

    final Employee result = this.employeeService.getEmployeeById(employeeId);
    assertFalse(result.getEmployeeSecurityData().isSuscribed());
  }

  /**
   * Unsuscribe employee instance not found exception test.
   */
  @Test
  @DisplayName("Unsuscribe Employee Not Exist")
  public void unsuscribeEmployeeInstanceNotFoundExceptionTest() {
    final Employee.EmployeeBuilder employee = createTestEmployee();

    assertThrows(InstanceNotFoundException.class, () -> {
      this.employeeService.unsuscribeEmployee(1L);
    });
  }

  /**
   * Unsuscribe employee unsuscribed test.
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   * @throws AlreadyUnsuscribeException     the already unsuscribe exception
   */
  @Test
  @DisplayName("Unsuscribe Employee Unsuscribed")
  public void unsuscribeEmployeeUnsuscribedTest()
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException, AlreadyUnsuscribeException {
    final Employee.EmployeeBuilder employee = createTestEmployee();
    final Long employeeId;
    employee.role(this.initRoles.get(0));
    employee.job(this.initJobs.get(0));

    employeeId = this.employeeService.signUp(employee.build());

    this.employeeService.unsuscribeEmployee(employeeId);

    assertThrows(AlreadyUnsuscribeException.class, () -> {
      this.employeeService.unsuscribeEmployee(employeeId);
    });
  }
}
