package es.udc.fic.pablohermida.portalempleado.test.unit.utils.filters;

import es.udc.fic.pablohermida.portalempleado.utils.filters.EmployeeFilter;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * The type Employee filter entities test.
 */
@SpringBootTest
@DisplayName("Employee Filter Entities")
public class EmployeeFilterEntitiesTest {
  /**
   * Equals employee filter contract.
   */
  @Test
  @DisplayName("Employee Filter Equals")
  public void equalsEmployeeFilterContract() {
    EqualsVerifier.forClass(EmployeeFilter.class).usingGetClass().verify();
  }
}
