package es.udc.fic.pablohermida.portalempleado.test.unit.rest.dtos;

import static es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeConversor.toEmployeeAuthDto;
import static es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.JobConversor.toJobDtos;
import static es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.RoleConversor.toRoleDtos;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Employee;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.EmployeeAuth;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Job;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Role;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeAuthDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeConversor;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.JobConversor;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.JobDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.RoleConversor;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.RoleDto;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * The type Employee conversors test.
 */
@SpringBootTest
@DisplayName("Employee Conversors")
public class EmployeeConversorsTest {

  /**
   * Role conversor test.
   */
  @Test
  @DisplayName("Role Conversor")
  public void roleConversorTest() {
    final RoleConversor conversor = new RoleConversor();
    assertNotNull(conversor);
    final List<Role> roles = new ArrayList<>();
    roles.add(new Role(1L, "test1"));
    roles.add(new Role(2L, "test2"));

    final List<RoleDto> rolesDtos = toRoleDtos(roles);

    final AtomicReference<Role> roleExpected = new AtomicReference<Role>();

    rolesDtos.forEach(roleActual -> {
      roleExpected.set(new Role(roleActual.getId(), roleActual.getName()));
      assertTrue(roles.contains(roleExpected.get()));
    });
  }

  /**
   * Job conversor test.
   */
  @Test
  @DisplayName("Job Conversor")
  public void jobConversorTest() {
    final JobConversor conversor = new JobConversor();
    assertNotNull(conversor);

    final List<Job> jobs = new ArrayList<>();
    jobs.add(new Job(1L, "test1"));
    jobs.add(new Job(2L, "test2"));

    final List<JobDto> jobsDtos = toJobDtos(jobs);

    final AtomicReference<Job> jobExpected = new AtomicReference<Job>();

    jobsDtos.forEach(jobActual -> {
      jobExpected.set(new Job(jobActual.getId(), jobActual.getName()));
      assertTrue(jobs.contains(jobExpected.get()));
    });
  }

  /**
   * Employee auth conversor test.
   */
  @Test
  @DisplayName("Employee Auth Conversor")
  public void employeeAuthConversorTest() {
    final EmployeeConversor conversor = new EmployeeConversor();
    assertNotNull(conversor);
    final Employee.EmployeeBuilder employee = new Employee.EmployeeBuilder();
    final String username = "name";
    final String token = "test";

    employee.employeeAuth(new EmployeeAuth(username, null, null));
    employee.role(new Role(1L, "test"));

    final EmployeeAuthDto employeeAuthDtoActual =
        toEmployeeAuthDto(employee.build(), token);

    assertEquals(username, employeeAuthDtoActual.getUsername());
    assertEquals(token, employeeAuthDtoActual.getToken());
  }

  /**
   * Employee search conversor test.
   */
  @Test
  @DisplayName("Employee Search Conversor")
  public void employeeSearchConversorTest() {
    final EmployeeConversor conversor = new EmployeeConversor();
    assertNotNull(conversor);
    final Employee.EmployeeBuilder employee = new Employee.EmployeeBuilder();
    final String username = "name";
    final String token = "test";

    employee.employeeAuth(new EmployeeAuth(username, null, null));
    employee.role(new Role(1L, "test"));

    final EmployeeAuthDto employeeAuthDtoActual =
        toEmployeeAuthDto(employee.build(), token);

    assertEquals(username, employeeAuthDtoActual.getUsername());
    assertEquals(token, employeeAuthDtoActual.getToken());
  }
}
