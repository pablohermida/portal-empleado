package es.udc.fic.pablohermida.portalempleado.rest.dtos.employee;

import static es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceSystemConversor.toVideoConferenceEmployeeDtos;
import static es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceSystemConversor.toVideoConferenceEmployees;

import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Employee;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.EmployeeAuth;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.EmployeeSecurityData;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Job;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Role;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.VideoConferenceEmployee;
import es.udc.fic.pablohermida.portalempleado.utils.filters.EmployeeFilter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * The type Employee conversor.
 */
public class EmployeeConversor {

  /**
   * Instantiates a new Employee conversor.
   */
  public EmployeeConversor() {}

  /**
   * To employee employee.
   *
   * @param employeeDto     the employee dto
   * @param role            the role
   * @param job             the job
   * @param supervisor      the supervisor
   * @param passwordEncoder the password encoder
   *
   * @return the employee
   */
  public static Employee toEmployee(final EmployeeDto employeeDto,
                                    final Role role, final Job job,
                                    final Employee supervisor,
                                    final BCryptPasswordEncoder passwordEncoder) {
    Set<VideoConferenceEmployee> vce = new HashSet<>();
    if (employeeDto.getVcsystems() != null) {
      vce =
          new HashSet<>(toVideoConferenceEmployees(employeeDto.getVcsystems()));
    }

    return new Employee(employeeDto.getName(), employeeDto.getLastname(),
        employeeDto.getImage(), employeeDto.getDateBirth(),
        employeeDto.getEmail(), employeeDto.getDni(), employeeDto.getAddress(),
        employeeDto.getAddressWork(), employeeDto.getPhone(),
        employeeDto.getDuration(), employeeDto.getDateStart(),
        employeeDto.getWorkDay(), employeeDto.getWorkHours(), role, job,
        supervisor, new EmployeeAuth(employeeDto.getUsername(),
        passwordEncoder.encode(employeeDto.getPassword()), null),
        new EmployeeSecurityData(employeeDto.getIban(), true), vce);
  }

  /**
   * To employee employee.
   *
   * @param id              the id
   * @param employeeDto     the employee dto
   * @param role            the role
   * @param job             the job
   * @param supervisor      the supervisor
   * @param passwordEncoder the password encoder
   *
   * @return the employee
   */
  public static Employee toEmployee(final Long id,
                                    final EmployeeDto employeeDto,
                                    final Role role, final Job job,
                                    final Employee supervisor,
                                    final BCryptPasswordEncoder passwordEncoder) {
    Set<VideoConferenceEmployee> vce = new HashSet<>();
    if (employeeDto.getVcsystems() != null) {
      vce =
          new HashSet<>(toVideoConferenceEmployees(employeeDto.getVcsystems()));
    }

    return new Employee(id, employeeDto.getName(), employeeDto.getLastname(),
        employeeDto.getImage(), employeeDto.getDateBirth(),
        employeeDto.getEmail(), employeeDto.getDni(), employeeDto.getAddress(),
        employeeDto.getAddressWork(), employeeDto.getPhone(),
        employeeDto.getDuration(), employeeDto.getDateStart(),
        employeeDto.getWorkDay(), employeeDto.getWorkHours(), role, job,
        supervisor, new EmployeeAuth(id, employeeDto.getUsername(),
        passwordEncoder.encode(employeeDto.getPassword()), null),
        new EmployeeSecurityData(id, employeeDto.getIban(), true), vce);
  }

  /**
   * To employee dto employee dto.
   *
   * @param employee the employee
   *
   * @return the employee dto
   */
  public static EmployeeDto toEmployeeDto(final Employee employee) {
    final Employee supervisor = employee.getSupervisor();
    String usernameSupervisor = null;
    if (supervisor != null) {
      usernameSupervisor = supervisor.getEmployeeAuth().getUsername();
    }
    return new EmployeeDto(employee.getName(), employee.getLastname(),
        employee.getImage(), employee.getDateBirth(), employee.getEmail(),
        employee.getDni(), employee.getAddress(), employee.getAddressWork(),
        employee.getPhone(), employee.getDuration(), employee.getDateStart(),
        employee.getWorkDay(), employee.getWorkHours(),
        employee.getRole().getId(), employee.getJob().getId(),
        employee.getJob().getName(), usernameSupervisor,
        employee.getEmployeeAuth().getUsername(), null,
        employee.getEmployeeSecurityData().getIban(),
        toVideoConferenceEmployeeDtos(employee.getVcsystems()));
  }

  /**
   * To employee limit dto employee dto.
   *
   * @param employee the employee
   *
   * @return the employee dto
   */
  public static EmployeeDto toEmployeeLimitDto(final Employee employee) {
    return new EmployeeDto(employee.getName(), employee.getLastname(),
        employee.getImage(), employee.getDateBirth(), employee.getEmail(), null,
        null, null, employee.getPhone(), null, employee.getDateStart(), null,
        null, null, null, employee.getJob().getName(), null,
        employee.getEmployeeAuth().getUsername(), null, null,
        toVideoConferenceEmployeeDtos(employee.getVcsystems()));
  }

  /**
   * To employee auth dto employee auth dto.
   *
   * @param employee the employee
   * @param token    the token
   *
   * @return the employee auth dto
   */
  public static EmployeeAuthDto toEmployeeAuthDto(final Employee employee,
                                                  final String token) {
    return new EmployeeAuthDto(employee.getId(),
        employee.getEmployeeAuth().getUsername(), employee.getRole().getId(),
        token);
  }

  /**
   * To employees search dto list.
   *
   * @param employees the employees
   *
   * @return the list
   */
  public static List<EmployeeSearchDto> toEmployeesSearchDto(
      final List<Employee> employees) {
    return employees.stream().map(EmployeeConversor::toEmployeeSearchDto)
        .collect(Collectors.toList());
  }

  private static EmployeeSearchDto toEmployeeSearchDto(
      final Employee employee) {
    return new EmployeeSearchDto(employee.getEmployeeAuth().getUsername(),
        employee.getName() + " " + employee.getLastname());
  }

  /**
   * To employees list dto list.
   *
   * @param employees the employees
   *
   * @return the list
   */
  public static List<EmployeeListDto> toEmployeesListDto(
      final List<Employee> employees) {
    return employees.stream().map(EmployeeConversor::toEmployeeListDto)
        .collect(Collectors.toList());
  }

  private static EmployeeListDto toEmployeeListDto(final Employee employee) {
    return new EmployeeListDto(employee.getId(), employee.getImage(),
        employee.getName() + " " + employee.getLastname(),
        employee.getEmployeeAuth().getUsername(), employee.getEmail(),
        employee.getJob().getName(), employee.getPhone());
  }

  /**
   * To employee filters employee filter.
   *
   * @param filters the filters
   *
   * @return the employee filter
   */
  public static EmployeeFilter toEmployeeFilters(
      final EmployeeFiltersDto filters) {
    return new EmployeeFilter(filters.getUsername(), filters.getName(),
        filters.getEmail(), filters.getJobsId(), filters.getRoleId());
  }
}