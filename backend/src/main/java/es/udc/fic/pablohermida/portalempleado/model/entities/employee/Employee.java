package es.udc.fic.pablohermida.portalempleado.model.entities.employee;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * The type Employee.
 */
@Entity
@Table(name = "employee", schema = "portalempleado")
public class Employee {

  /**
   * The Autoincremental Id.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  /**
   * The Name.
   */
  private String name;
  /**
   * The Lastname.
   */
  private String lastname;
  /**
   * The Image.
   */
  private String image;
  /**
   * The Date birth.
   */
  @Column(name = "date_birth")
  private LocalDate dateBirth;
  /**
   * The Email.
   */
  private String email;
  /**
   * The Dni.
   */
  private String dni;
  /**
   * The Address.
   */
  private String address;
  /**
   * The Address work.
   */
  @Column(name = "address_work")
  private String addressWork;
  /**
   * The Phone.
   */
  private String phone;
  /**
   * The Duration.
   */
  private String duration;
  /**
   * The Date start.
   */
  @Column(name = "date_start")
  private LocalDate dateStart;
  /**
   * The Work day.
   */
  @Column(name = "work_day")
  private String workDay;
  /**
   * The Work hours.
   */
  @Column(name = "work_hours")
  private String workHours;
  /**
   * The Role.
   */
  @ManyToOne(optional = false, fetch = FetchType.EAGER)
  @JoinColumn(name = "roleId")
  private Role role;
  /**
   * The Job.
   */
  @ManyToOne(optional = false, fetch = FetchType.EAGER)
  @JoinColumn(name = "jobId")
  private Job job;
  /**
   * The Supervisor.
   */
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "employeeId")
  private Employee supervisor;

  /**
   * The Employee auth.
   */
  @OneToOne(mappedBy = "employee", cascade = CascadeType.ALL)
  @PrimaryKeyJoinColumn
  private EmployeeAuth employeeAuth;
  /**
   * The Employee security data.
   */
  @OneToOne(mappedBy = "employee", cascade = CascadeType.ALL)
  @PrimaryKeyJoinColumn
  private EmployeeSecurityData employeeSecurityData;

  @OneToMany(mappedBy = "employee")
  private Set<VideoConferenceEmployee> vcsystems;

  /**
   * Instantiates a new Employee.
   */
  public Employee() {
  }

  /**
   * Instantiates a new Employee.
   * S
   *
   * @param name                 the name
   * @param lastname             the lastname
   * @param image                the image
   * @param dateBirth            the date birth
   * @param email                the email
   * @param dni                  the dni
   * @param address              the address
   * @param addressWork          the address work
   * @param phone                the phone
   * @param duration             the duration
   * @param dateStart            the date start
   * @param workDay              the work day
   * @param workHours            the work hours
   * @param role                 the role
   * @param job                  the job
   * @param supervisor           the supervisor
   * @param employeeAuth         the employee auth
   * @param employeeSecurityData the employee security data
   * @param vcsystems            the vcsystems
   */
  public Employee(final String name, final String lastname, final String image,
                  final LocalDate dateBirth, final String email,
                  final String dni, final String address,
                  final String addressWork, final String phone,
                  final String duration, final LocalDate dateStart,
                  final String workDay, final String workHours, final Role role,
                  final Job job, final Employee supervisor,
                  final EmployeeAuth employeeAuth,
                  final EmployeeSecurityData employeeSecurityData,
                  final Set<VideoConferenceEmployee> vcsystems) {
    this.name = name;
    this.lastname = lastname;
    this.image = image;
    this.dateBirth = dateBirth;
    this.email = email;
    this.dni = dni;
    this.address = address;
    this.addressWork = addressWork;
    this.phone = phone;
    this.duration = duration;
    this.dateStart = dateStart;
    this.workDay = workDay;
    this.workHours = workHours;
    this.role = role;
    this.job = job;
    this.supervisor = supervisor;
    this.employeeAuth = employeeAuth;
    this.employeeSecurityData = employeeSecurityData;
    if (this.employeeAuth != null && this.employeeSecurityData != null) {
      this.employeeAuth.setEmployee(this);
      this.employeeSecurityData.setEmployee(this);
      this.vcsystems = vcsystems;
    }
  }

  /**
   * Instantiates a new Employee.
   *
   * @param id                   the id
   * @param name                 the name
   * @param lastname             the lastname
   * @param image                the image
   * @param dateBirth            the date birth
   * @param email                the email
   * @param dni                  the dni
   * @param address              the address
   * @param addressWork          the address work
   * @param phone                the phone
   * @param duration             the duration
   * @param dateStart            the date start
   * @param workDay              the work day
   * @param workHours            the work hours
   * @param role                 the role
   * @param job                  the job
   * @param supervisor           the supervisor
   * @param employeeAuth         the employee auth
   * @param employeeSecurityData the employee security data
   * @param vcsystems            the vcsystems
   */
  public Employee(final Long id, final String name, final String lastname,
                  final String image, final LocalDate dateBirth,
                  final String email, final String dni, final String address,
                  final String addressWork, final String phone,
                  final String duration, final LocalDate dateStart,
                  final String workDay, final String workHours, final Role role,
                  final Job job, final Employee supervisor,
                  final EmployeeAuth employeeAuth,
                  final EmployeeSecurityData employeeSecurityData,
                  final Set<VideoConferenceEmployee> vcsystems) {
    this(name, lastname, image, dateBirth, email, dni, address, addressWork,
        phone, duration, dateStart, workDay, workHours, role, job, supervisor,
        employeeAuth, employeeSecurityData, vcsystems);
    this.id = id;
  }

  /**
   * Gets id.
   *
   * @return the id
   */
  public Long getId() {
    return this.id;
  }

  /**
   * Gets name.
   *
   * @return the name
   */
  public String getName() {
    return this.name;
  }

  /**
   * Gets lastname.
   *
   * @return the lastname
   */
  public String getLastname() {
    return this.lastname;
  }

  /**
   * Gets image.
   *
   * @return the image
   */
  public String getImage() {
    return this.image;
  }

  /**
   * Gets date birth.
   *
   * @return the date birth
   */
  public LocalDate getDateBirth() {
    return this.dateBirth;
  }

  /**
   * Gets email.
   *
   * @return the email
   */
  public String getEmail() {
    return this.email;
  }

  /**
   * Gets dni.
   *
   * @return the dni
   */
  public String getDni() {
    return this.dni;
  }

  /**
   * Gets address.
   *
   * @return the address
   */
  public String getAddress() {
    return this.address;
  }

  /**
   * Gets address work.
   *
   * @return the address work
   */
  public String getAddressWork() {
    return this.addressWork;
  }

  /**
   * Gets phone.
   *
   * @return the phone
   */
  public String getPhone() {
    return this.phone;
  }

  /**
   * Gets duration.
   *
   * @return the duration
   */
  public String getDuration() {
    return this.duration;
  }

  /**
   * Gets date start.
   *
   * @return the date start
   */
  public LocalDate getDateStart() {
    return this.dateStart;
  }

  /**
   * Gets work day.
   *
   * @return the work day
   */
  public String getWorkDay() {
    return this.workDay;
  }

  /**
   * Gets work hours.
   *
   * @return the work hours
   */
  public String getWorkHours() {
    return this.workHours;
  }

  /**
   * Gets role.
   *
   * @return the role
   */
  public Role getRole() {
    return this.role;
  }

  /**
   * Gets job.
   *
   * @return the job
   */
  public Job getJob() {
    return this.job;
  }

  /**
   * Gets supervisor.
   *
   * @return the supervisor
   */
  public Employee getSupervisor() {
    return this.supervisor;
  }

  /**
   * Gets employee auth.
   *
   * @return the employee auth
   */
  public EmployeeAuth getEmployeeAuth() {
    return this.employeeAuth;
  }

  /**
   * Sets employee auth.
   *
   * @param employeeAuth the employee auth
   */
  public void setEmployeeAuth(final EmployeeAuth employeeAuth) {
    this.employeeAuth = employeeAuth;
  }

  /**
   * Gets employee security data.
   *
   * @return the employee security data
   */
  public EmployeeSecurityData getEmployeeSecurityData() {
    return this.employeeSecurityData;
  }

  /**
   * Sets employee security data.
   *
   * @param employeeSecurityData the employee security data
   */
  public void setEmployeeSecurityData(
      final EmployeeSecurityData employeeSecurityData) {
    this.employeeSecurityData = employeeSecurityData;
  }

  /**
   * Gets vcsystems.
   *
   * @return the vcsystems
   */
  public Set<VideoConferenceEmployee> getVcsystems() {
    return this.vcsystems;
  }

  /**
   * Sets vcsystems.
   *
   * @param vcsystems the vcsystems
   */
  public void setVcsystems(final Set<VideoConferenceEmployee> vcsystems) {
    this.vcsystems = vcsystems;
  }

  /**
   * Override method equals of Object class.
   *
   * @param o the employee
   */
  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || this.getClass() != o.getClass()) { return false; }
    final Employee employee = (Employee) o;
    return Objects.equals(this.getName(), employee.getName()) && Objects
        .equals(this.getLastname(), employee.getLastname()) && Objects
        .equals(this.getImage(), employee.getImage()) && Objects
        .equals(this.getDateBirth(), employee.getDateBirth()) && Objects
        .equals(this.getEmail(), employee.getEmail()) && Objects
        .equals(this.getDni(), employee.getDni()) && Objects
        .equals(this.getAddress(), employee.getAddress()) && Objects
        .equals(this.getAddressWork(), employee.getAddressWork()) && Objects
        .equals(this.getPhone(), employee.getPhone()) && Objects
        .equals(this.getDuration(), employee.getDuration()) && Objects
        .equals(this.getDateStart(), employee.getDateStart()) && Objects
        .equals(this.getWorkDay(), employee.getWorkDay()) && Objects
        .equals(this.getWorkHours(), employee.getWorkHours()) && Objects
        .equals(this.getRole(), employee.getRole()) && Objects
        .equals(this.getJob(), employee.getJob()) && Objects
        .equals(this.getSupervisor(), employee.getSupervisor()) && Objects
        .equals(this.getEmployeeAuth(), employee.getEmployeeAuth()) && Objects
        .equals(this.getEmployeeSecurityData(),
            employee.getEmployeeSecurityData()) && Objects
        .equals(this.getVcsystems(), employee.getVcsystems());
  }

  /**
   * Override method hashCode of Object class.
   *
   * @return int
   */
  @Override
  public int hashCode() {
    return Objects.hash(this.getName(), this.getLastname(), this.getImage(),
        this.getDateBirth(), this.getEmail(), this.getDni(), this.getAddress(),
        this.getAddressWork(), this.getPhone(), this.getDuration(),
        this.getDateStart(), this.getWorkDay(), this.getWorkHours(),
        this.getRole(), this.getJob(), this.getSupervisor(),
        this.getEmployeeAuth(), this.getEmployeeSecurityData(),
        this.getVcsystems());
  }

  /**
   * The type Employee builder.
   */
  public static final class EmployeeBuilder {
    /**
     * The Id.
     */
    private Long id;
    /**
     * The Name.
     */
    private String name;
    /**
     * The Lastname.
     */
    private String lastname;
    /**
     * The Image.
     */
    private String image;
    /**
     * The Date birth.
     */
    private LocalDate dateBirth;
    /**
     * The Email.
     */
    private String email;
    /**
     * The Dni.
     */
    private String dni;
    /**
     * The Address.
     */
    private String address;
    /**
     * The Address work.
     */
    private String addressWork;
    /**
     * The Phone.
     */
    private String phone;
    /**
     * The Duration.
     */
    private String duration;
    /**
     * The Date start.
     */
    private LocalDate dateStart;
    /**
     * The Work day.
     */
    private String workDay;
    /**
     * The Work hours.
     */
    private String workHours;
    /**
     * The Role.
     */
    private Role role;
    /**
     * The Job.
     */
    private Job job;
    /**
     * The Supervisor.
     */
    private Employee supervisor;
    /**
     * The Employee auth.
     */
    private EmployeeAuth employeeAuth;
    /**
     * The Employee security data.
     */
    private EmployeeSecurityData employeeSecurityData;

    private Set<VideoConferenceEmployee> vcsystems;

    /**
     * Instantiates a new Employee builder.
     */
    public EmployeeBuilder() {
    }

    /**
     * Id employee builder.
     *
     * @param id the id
     *
     * @return the employee builder
     */
    public EmployeeBuilder id(final Long id) {
      this.id = id;
      return this;
    }

    /**
     * Name employee builder.
     *
     * @param name the name
     *
     * @return the employee builder
     */
    public EmployeeBuilder name(final String name) {
      this.name = name;
      return this;
    }

    /**
     * Lastname employee builder.
     *
     * @param lastname the lastname
     *
     * @return the employee builder
     */
    public EmployeeBuilder lastname(final String lastname) {
      this.lastname = lastname;
      return this;
    }

    /**
     * Image employee builder.
     *
     * @param image the image
     *
     * @return the employee builder
     */
    public EmployeeBuilder image(final String image) {
      this.image = image;
      return this;
    }

    /**
     * Date birth employee builder.
     *
     * @param dateBirth the date birth
     *
     * @return the employee builder
     */
    public EmployeeBuilder dateBirth(final LocalDate dateBirth) {
      this.dateBirth = dateBirth;
      return this;
    }

    /**
     * Email employee builder.
     *
     * @param email the email
     *
     * @return the employee builder
     */
    public EmployeeBuilder email(final String email) {
      this.email = email;
      return this;
    }

    /**
     * Dni employee builder.
     *
     * @param dni the dni
     *
     * @return the employee builder
     */
    public EmployeeBuilder dni(final String dni) {
      this.dni = dni;
      return this;
    }

    /**
     * Address employee builder.
     *
     * @param address the address
     *
     * @return the employee builder
     */
    public EmployeeBuilder address(final String address) {
      this.address = address;
      return this;
    }

    /**
     * Address work employee builder.
     *
     * @param addressWork the address work
     *
     * @return the employee builder
     */
    public EmployeeBuilder addressWork(final String addressWork) {
      this.addressWork = addressWork;
      return this;
    }

    /**
     * Phone employee builder.
     *
     * @param phone the phone
     *
     * @return the employee builder
     */
    public EmployeeBuilder phone(final String phone) {
      this.phone = phone;
      return this;
    }

    /**
     * Duration employee builder.
     *
     * @param duration the duration
     *
     * @return the employee builder
     */
    public EmployeeBuilder duration(final String duration) {
      this.duration = duration;
      return this;
    }

    /**
     * Date start employee builder.
     *
     * @param dateStart the date start
     *
     * @return the employee builder
     */
    public EmployeeBuilder dateStart(final LocalDate dateStart) {
      this.dateStart = dateStart;
      return this;
    }

    /**
     * Work day employee builder.
     *
     * @param workDay the work day
     *
     * @return the employee builder
     */
    public EmployeeBuilder workDay(final String workDay) {
      this.workDay = workDay;
      return this;
    }

    /**
     * Work hours employee builder.
     *
     * @param workHours the work hours
     *
     * @return the employee builder
     */
    public EmployeeBuilder workHours(final String workHours) {
      this.workHours = workHours;
      return this;
    }

    /**
     * Role employee builder.
     *
     * @param role the role
     *
     * @return the employee builder
     */
    public EmployeeBuilder role(final Role role) {
      this.role = role;
      return this;
    }

    /**
     * Job employee builder.
     *
     * @param job the job
     *
     * @return the employee builder
     */
    public EmployeeBuilder job(final Job job) {
      this.job = job;
      return this;
    }

    /**
     * Supervisor employee builder.
     *
     * @param supervisor the supervisor
     *
     * @return the employee builder
     */
    public EmployeeBuilder supervisor(final Employee supervisor) {
      this.supervisor = supervisor;
      return this;
    }

    /**
     * Employee auth employee builder.
     *
     * @param employeeAuth the employee auth
     *
     * @return the employee builder
     */
    public EmployeeBuilder employeeAuth(final EmployeeAuth employeeAuth) {
      this.employeeAuth = employeeAuth;
      return this;
    }

    /**
     * Employee security data employee builder.
     *
     * @param employeeSecurityData the employee security data
     *
     * @return the employee builder
     */
    public EmployeeBuilder employeeSecurityData(
        final EmployeeSecurityData employeeSecurityData) {
      this.employeeSecurityData = employeeSecurityData;
      return this;
    }

    /**
     * Vcsystems employee builder.
     *
     * @param vcsystems the vcsystems
     *
     * @return the employee builder
     */
    public EmployeeBuilder vcsystems(
        final Set<VideoConferenceEmployee> vcsystems) {
      this.vcsystems = vcsystems;
      return this;
    }

    /**
     * Build employee.
     *
     * @return the employee
     */
    public Employee build() {
      return new Employee(this.id, this.name, this.lastname, this.image,
          this.dateBirth, this.email, this.dni, this.address, this.addressWork,
          this.phone, this.duration, this.dateStart, this.workDay,
          this.workHours, this.role, this.job, this.supervisor,
          this.employeeAuth, this.employeeSecurityData, this.vcsystems);
    }
  }
}
