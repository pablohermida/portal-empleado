package es.udc.fic.pablohermida.portalempleado.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

/**
 * The type Security config.
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
  @Autowired
  private JwtGenerator jwtGenerator;

  /**
   * Cors configuration source cors configuration source.
   *
   * @return the cors configuration source
   */
  @Bean
  public static CorsConfigurationSource corsConfigurationSource() {

    final CorsConfiguration config = new CorsConfiguration();
    final UrlBasedCorsConfigurationSource source =
        new UrlBasedCorsConfigurationSource();

    config.setAllowCredentials(true);
    config.addAllowedOrigin("*");
    config.addAllowedHeader("*");
    config.addAllowedMethod("*");

    source.registerCorsConfiguration("/**", config);

    return source;

  }

  @Override
  protected void configure(final HttpSecurity http) throws Exception {

    http.sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().addFilter(
        new JwtFilter(this.authenticationManager(), this.jwtGenerator));
    http.cors().and().csrf().disable();
    http.headers().frameOptions().disable();

    http.authorizeRequests()
        .antMatchers("/employee/signUp", "/employee/unsuscribe/?")
        .hasRole("ADMIN");
    http.authorizeRequests()
        .antMatchers("/employee/search", "/employee/search/?",
            "/employee/roles", "/employee/jobs", "/employee/?")
        .hasAnyRole("ADMIN", "EMPLOYEE");

    http.authorizeRequests().antMatchers("/h2-console/**", "/employee/login",
        "/employee/loginFromServiceToken").permitAll();
  }

}
