package es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem;

import java.util.Objects;
import javax.validation.constraints.NotBlank;

/**
 * The type Video conference system create dto.
 */
public class VideoConferenceSystemCreateDto {
  @NotBlank
  private String platform;

  /**
   * Instantiates a new Video conference system create dto.
   */
  public VideoConferenceSystemCreateDto() {}

  /**
   * Instantiates a new Video conference system create dto.
   *
   * @param platform the platform
   */
  public VideoConferenceSystemCreateDto(final String platform) {
    this.platform = platform;
  }

  /**
   * Gets platform.
   *
   * @return the platform
   */
  public String getPlatform() {
    return this.platform;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || this.getClass() != o.getClass()) { return false; }
    final VideoConferenceSystemCreateDto that =
        (VideoConferenceSystemCreateDto) o;
    return Objects.equals(this.getPlatform(), that.getPlatform());
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getPlatform());
  }

  /**
   * The interface All validations.
   */
  public interface AllValidations {
  }
}
