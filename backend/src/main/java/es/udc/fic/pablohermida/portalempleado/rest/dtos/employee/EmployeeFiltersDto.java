package es.udc.fic.pablohermida.portalempleado.rest.dtos.employee;

import es.udc.fic.pablohermida.portalempleado.rest.dtos.SortListing;
import java.util.List;
import java.util.Objects;

/**
 * The type Employee filters dto.
 */
public class EmployeeFiltersDto extends SortListing {
  private final String username;
  private final String name;
  private final String email;
  private final List<Long> jobsId;
  private final Long roleId;

  /**
   * Instantiates a new Employee filters dto.
   *
   * @param username       the username
   * @param name           the name
   * @param email          the email
   * @param jobsId         the jobs id
   * @param roleId         the role id
   * @param order          the order
   * @param orderAttribute the order attribute
   */
  public EmployeeFiltersDto(final String username, final String name,
                            final String email, final List<Long> jobsId,
                            final Long roleId, final String order,
                            final List<String> orderAttribute) {
    super(order, orderAttribute);
    this.username = username;
    this.name = name;
    this.email = email;
    this.jobsId = jobsId;
    this.roleId = roleId;
  }

  /**
   * Gets username.
   *
   * @return the username
   */
  public String getUsername() {
    return this.username;
  }

  /**
   * Gets name.
   *
   * @return the name
   */
  public String getName() {
    return this.name;
  }

  /**
   * Gets email.
   *
   * @return the email
   */
  public String getEmail() {
    return this.email;
  }

  /**
   * Gets jobs id.
   *
   * @return the jobs id
   */
  public List<Long> getJobsId() {
    return this.jobsId;
  }

  /**
   * Gets role id.
   *
   * @return the role id
   */
  public Long getRoleId() {
    return this.roleId;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || this.getClass() != o.getClass()) { return false; }
    final EmployeeFiltersDto that = (EmployeeFiltersDto) o;
    return Objects.equals(this.getUsername(), that.getUsername()) && Objects
        .equals(this.getName(), that.getName()) && Objects
        .equals(this.getEmail(), that.getEmail()) && Objects
        .equals(this.getJobsId(), that.getJobsId()) && Objects
        .equals(this.getRoleId(), that.getRoleId()) && Objects
        .equals(this.getOrder(), that.getOrder()) && Objects
        .equals(this.getOrderAttribute(), that.getOrderAttribute());
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getUsername(), this.getName(), this.getEmail(),
        this.getJobsId(), this.getRoleId(), this.getOrder(),
        this.getOrderAttribute());
  }
}
