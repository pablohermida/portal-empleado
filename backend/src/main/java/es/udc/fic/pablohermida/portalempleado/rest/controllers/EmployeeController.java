package es.udc.fic.pablohermida.portalempleado.rest.controllers;

import static es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeConversor.toEmployee;
import static es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeConversor.toEmployeeAuthDto;
import static es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeConversor.toEmployeeDto;
import static es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeConversor.toEmployeeFilters;
import static es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeConversor.toEmployeeLimitDto;
import static es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeConversor.toEmployeesListDto;
import static es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeConversor.toEmployeesSearchDto;
import static es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.JobConversor.toJobDtos;
import static es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.RoleConversor.toRoleDtos;

import es.udc.fic.pablohermida.portalempleado.config.JwtGenerator;
import es.udc.fic.pablohermida.portalempleado.config.JwtInfo;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Employee;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Job;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Role;
import es.udc.fic.pablohermida.portalempleado.model.services.Block;
import es.udc.fic.pablohermida.portalempleado.model.services.employee.EmployeeService;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeAuthDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeFiltersDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeListDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeLoginDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.EmployeeSearchDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.JobDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.employee.RoleDto;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.AlreadyUnsuscribeException;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.FieldErrorException;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.PasswordNotMatchException;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.PermissionException;
import es.udc.fic.pablohermida.portalempleado.utils.filters.EmployeeFilter;
import es.udc.fic.pablohermida.portalempleado.utils.filters.EmployeeOrderEnum;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * The type Employee controller.
 */
@RestController
@RequestMapping("/employee")
public class EmployeeController extends ExceptionController {
  @Autowired
  private EmployeeService employeeService;

  @Autowired
  private JwtGenerator jwtGenerator;

  @Autowired
  private BCryptPasswordEncoder passwordEncoder;

  /**
   * Gets employee by filters.
   *
   * @param filters the filters
   * @param page    the page
   *
   * @return the employee by filters
   *
   * @throws FieldErrorException the field error exception
   */
  @GetMapping("/search")
  public Block<EmployeeListDto> getEmployeeByFilters(
      final EmployeeFiltersDto filters,
      @RequestParam(defaultValue = "0") final int page)
      throws FieldErrorException {
    final Sort sort;
    final Pageable pageable;
    final List<String> order = new ArrayList<>();

    if (filters.getOrderAttribute().size() < 1) {
      filters.getOrderAttribute().add("username");
    }
    for (final String oa : filters.getOrderAttribute()) {
      order.add(EmployeeOrderEnum.fromField(oa));
    }
    sort = Sort.by(filters.getOrder(),
        order.stream().collect(Collectors.joining()));
    pageable = PageRequest.of(page, 10, sort);

    final EmployeeFilter criteria = toEmployeeFilters(filters);

    final Block<Employee> employeeList =
        this.employeeService.searchEmployees(criteria, pageable);

    return new Block<>(toEmployeesListDto(employeeList.getItems()),
        employeeList.getExistMoreItems(), employeeList.getTotalPages());
  }

  /**
   * Search by user name list.
   *
   * @param username the username
   *
   * @return the list
   */
  @GetMapping("/search/{username}")
  public List<EmployeeSearchDto> searchByUserName(
      @PathVariable final String username) {
    final List<Employee> employees =
        this.employeeService.searchEmployeesByUsername(username);
    return toEmployeesSearchDto(employees);
  }

  /**
   * Gets roles.
   *
   * @return the roles
   */
  @GetMapping("/roles")
  public List<RoleDto> getRoles() {
    final List<Role> roles = this.employeeService.getAllRoles();
    return toRoleDtos(roles);
  }

  /**
   * Gets jobs.
   *
   * @return the jobs
   */
  @GetMapping("/jobs")
  public List<JobDto> getJobs() {
    final List<Job> jobs = this.employeeService.getAllJobs();
    return toJobDtos(jobs);
  }

  /**
   * Gets employee by id.
   *
   * @param userId the user id
   * @param id     the id
   *
   * @return the employee by id
   *
   * @throws InstanceNotFoundException the instance not found exception
   */
  @GetMapping("/{id}")
  public EmployeeDto getEmployeeById(@RequestAttribute final Long userId,
                                     @PathVariable final Long id)
      throws InstanceNotFoundException {
    final Employee admin = this.employeeService.getEmployeeById(userId);
    final Employee user = this.employeeService.getEmployeeById(id);
    if (!id.equals(userId) && !this.isAdmin(admin.getRole().getId())) {
      return toEmployeeLimitDto(user);
    }
    return toEmployeeDto(user);
  }

  /**
   * Sign up employee auth dto.
   *
   * @param employeeDto the employee dto
   *
   * @return the employee auth dto
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws InstanceNotFoundException      the instance not found exception
   */
  @PostMapping("/signUp")
  public EmployeeAuthDto signUp(
      @Validated({EmployeeDto.AllValidations.class}) @RequestBody
      final EmployeeDto employeeDto)
      throws FieldErrorException, InstanceAlreadyExistsException,
      InstanceNotFoundException {
    final Role role = this.employeeService.getRoleById(employeeDto.getRoleId());
    final Job job = this.employeeService.getJobById(employeeDto.getJobId());
    Employee supervisor = null;

    if (employeeDto.getSupervisorLogin() != null) {
      final List<Employee> supervisors = this.employeeService
          .searchEmployeesByUsername(employeeDto.getSupervisorLogin());
      if (supervisors.size() == 0) {
        throw new InstanceNotFoundException(employeeDto.getSupervisorLogin());
      }
      supervisor = supervisors.get(0);
    }
    final Employee employee =
        toEmployee(employeeDto, role, job, supervisor, this.passwordEncoder);
    final Long userId = this.employeeService.signUp(employee);

    return toEmployeeAuthDto(employee,
        this.generateServiceToken(employee, userId));
  }

  /**
   * Login employee auth dto.
   *
   * @param employeeLoginDto the employee login dto
   *
   * @return the employee auth dto
   *
   * @throws InstanceNotFoundException the instance not found exception
   * @throws PasswordNotMatchException the password not match exception
   */
  @PostMapping("/login")
  public EmployeeAuthDto login(
      @Validated({EmployeeLoginDto.AllValidations.class}) @RequestBody
      final EmployeeLoginDto employeeLoginDto)
      throws InstanceNotFoundException, PasswordNotMatchException {
    final Employee employee = this.employeeService
        .getEmployeeByUsername(employeeLoginDto.getUsername());

    if (!this.passwordEncoder.matches(employeeLoginDto.getPassword(),
        employee.getEmployeeAuth().getPassword())) {
      throw new PasswordNotMatchException();
    }

    return toEmployeeAuthDto(employee,
        this.generateServiceToken(employee, employee.getId()));
  }

  /**
   * Login from service token employee auth dto.
   *
   * @param userId       the user id
   * @param serviceToken the service token
   *
   * @return the employee auth dto
   *
   * @throws InstanceNotFoundException the instance not found exception
   */
  @PostMapping("/loginFromServiceToken")
  public EmployeeAuthDto loginFromServiceToken(
      @RequestAttribute final Long userId,
      @RequestAttribute final String serviceToken)
      throws InstanceNotFoundException {
    final Employee employee = this.employeeService.getEmployeeById(userId);
    return toEmployeeAuthDto(employee, serviceToken);
  }

  /**
   * Upload employee.
   *
   * @param userId      the user id
   * @param id          the id
   * @param employeeDto the employee dto
   *
   * @throws InstanceNotFoundException the instance not found exception
   * @throws PermissionException       the permission exception
   * @throws FieldErrorException       the field error exception
   */
  @PutMapping("/{id}")
  public void uploadEmployee(@RequestAttribute final Long userId,
                             @PathVariable final Long id,
                             @Validated({EmployeeDto.AllValidations.class})
                             @RequestBody final EmployeeDto employeeDto)
      throws InstanceNotFoundException, PermissionException,
      FieldErrorException {
    final Employee admin = this.employeeService.getEmployeeById(userId);
    if (!id.equals(userId) && !this.isAdmin(admin.getRole().getId())) {
      throw new PermissionException();
    }
    final Role role = this.employeeService.getRoleById(employeeDto.getRoleId());
    final Job job = this.employeeService.getJobById(employeeDto.getJobId());
    Employee supervisor = null;

    if (employeeDto.getSupervisorLogin() != null) {
      supervisor = this.employeeService
          .getEmployeeByUsername(employeeDto.getSupervisorLogin());
    }
    this.employeeService.editEmployee(
        toEmployee(id, employeeDto, role, job, supervisor,
            this.passwordEncoder));
  }

  /**
   * Unsuscribe employee.
   *
   * @param id the id
   *
   * @throws InstanceNotFoundException  the instance not found exception
   * @throws AlreadyUnsuscribeException the already unsuscribe exception
   */
  @PostMapping("/unsuscribe/{id}")
  public void unsuscribeEmployee(@PathVariable final Long id)
      throws InstanceNotFoundException, AlreadyUnsuscribeException {
    this.employeeService.unsuscribeEmployee(id);
  }

  private String generateServiceToken(final Employee employee,
                                      final Long employeeId) {
    final JwtInfo jwtInfo =
        new JwtInfo(employeeId, employee.getEmployeeAuth().getUsername(),
            employee.getRole().getName());

    return this.jwtGenerator.generate(jwtInfo);
  }

  private boolean isAdmin(final Long id) throws InstanceNotFoundException {
    final String adminName = "ADMIN";
    final Role admin = this.employeeService.getRoleById(id);
    return adminName.equals(admin.getName());
  }

}
