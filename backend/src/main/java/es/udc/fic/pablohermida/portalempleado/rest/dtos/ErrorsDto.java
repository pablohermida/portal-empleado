package es.udc.fic.pablohermida.portalempleado.rest.dtos;

import java.util.Objects;

/**
 * The type Errors dto.
 */
public class ErrorsDto {
  private final String code;
  private final String globalError;

  /**
   * Instantiates a new Errors dto.
   *
   * @param code        the code
   * @param globalError the global error
   */
  public ErrorsDto(final String code, final String globalError) {
    this.code = code;
    this.globalError = globalError;
  }

  /**
   * Gets code.
   *
   * @return the code
   */
  public String getCode() {
    return this.code;
  }

  /**
   * Gets global error.
   *
   * @return the global error
   */
  private String getGlobalError() {
    return this.globalError;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || this.getClass() != o.getClass()) { return false; }
    final ErrorsDto errorsDto = (ErrorsDto) o;
    return Objects.equals(this.code, errorsDto.getCode()) && Objects
        .equals(this.getGlobalError(), errorsDto.getGlobalError());
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getCode(), this.getGlobalError());
  }
}
