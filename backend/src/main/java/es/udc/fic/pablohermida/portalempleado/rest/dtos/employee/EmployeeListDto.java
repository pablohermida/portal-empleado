package es.udc.fic.pablohermida.portalempleado.rest.dtos.employee;

import java.util.Objects;

/**
 * The type Employee list dto.
 */
public class EmployeeListDto {
  private final Long id;
  private final String image;
  private final String name;
  private final String username;
  private final String email;
  private final String job;
  private final String phone;

  /**
   * Instantiates a new Employee list dto.
   *
   * @param id       the id
   * @param image    the image
   * @param name     the name
   * @param username the username
   * @param email    the email
   * @param job      the job
   * @param phone    the phone
   */
  EmployeeListDto(final Long id, final String image, final String name,
                  final String username, final String email, final String job,
                  final String phone) {
    this.id = id;
    this.image = image;
    this.name = name;
    this.username = username;
    this.email = email;
    this.job = job;
    this.phone = phone;
  }

  /**
   * Gets id.
   *
   * @return the id
   */
  public Long getId() {
    return this.id;
  }

  /**
   * Gets image.
   *
   * @return the image
   */
  public String getImage() {
    return this.image;
  }

  /**
   * Gets name.
   *
   * @return the name
   */
  public String getName() {
    return this.name;
  }

  /**
   * Gets username.
   *
   * @return the username
   */
  public String getUsername() {
    return this.username;
  }

  /**
   * Gets email.
   *
   * @return the email
   */
  public String getEmail() {
    return this.email;
  }

  /**
   * Gets job.
   *
   * @return the job
   */
  public String getJob() {
    return this.job;
  }

  /**
   * Gets phone.
   *
   * @return the phone
   */
  public String getPhone() {
    return this.phone;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || getClass() != o.getClass()) { return false; }
    final EmployeeListDto that = (EmployeeListDto) o;
    return Objects.equals(getId(), that.getId()) && Objects
        .equals(getImage(), that.getImage()) && Objects
        .equals(getName(), that.getName()) && Objects
        .equals(getUsername(), that.getUsername()) && Objects
        .equals(getEmail(), that.getEmail()) && Objects
        .equals(getJob(), that.getJob()) && Objects
        .equals(getPhone(), that.getPhone());
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(getId(), getImage(), getName(), getUsername(), getEmail(),
            getJob(), getPhone());
  }
}
