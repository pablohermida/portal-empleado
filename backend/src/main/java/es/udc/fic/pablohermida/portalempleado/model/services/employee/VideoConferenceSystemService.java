package es.udc.fic.pablohermida.portalempleado.model.services.employee;

import es.udc.fic.pablohermida.portalempleado.model.entities.employee.VideoConferenceSystem;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.FieldErrorException;
import java.util.List;
import javax.management.InstanceAlreadyExistsException;

/**
 * The interface Video conference system service.
 */
public interface VideoConferenceSystemService {
  /**
   * Create video conference system long.
   *
   * @param vcs the vcs
   *
   * @return the long
   *
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   * @throws FieldErrorException            the field error exception
   */
  Long createVideoConferenceSystem(VideoConferenceSystem vcs)
      throws InstanceAlreadyExistsException, FieldErrorException;

  /**
   * Gets video conference systems.
   *
   * @return the video conference systems
   */
  List<VideoConferenceSystem> getVideoConferenceSystems();
}
