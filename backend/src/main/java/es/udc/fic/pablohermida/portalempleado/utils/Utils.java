package es.udc.fic.pablohermida.portalempleado.utils;

import es.udc.fic.pablohermida.portalempleado.utils.exceptions.FieldErrorException;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.FieldErrorReason;
import java.time.LocalDate;
import org.apache.commons.lang3.StringUtils;

/**
 * The type Utils.
 */
public class Utils {

  private Utils() {}

  /**
   * Validate string.
   *
   * @param s     the s
   * @param min   the min
   * @param max   the max
   * @param field the field
   *
   * @throws FieldErrorException the field error exception
   */
  public static void validateString(final String s, final int min,
                                    final int max, final String field)
      throws FieldErrorException {
    validateString(s, field);
    if (isNotBetween(s, min, max)) {
      throw new FieldErrorException(field, FieldErrorReason.LENGTH);
    }
  }

  private static void validateString(final String s, final String field)
      throws FieldErrorException {
    if (StringUtils.isBlank(s)) {
      throw new FieldErrorException(field, FieldErrorReason.IS_BLANK);
    }
  }

  /**
   * Validate date.
   *
   * @param d     the d
   * @param field the field
   *
   * @throws FieldErrorException the field error exception
   */
  public static void validateDate(final LocalDate d, final String field)
      throws FieldErrorException {
    if (d == null) {
      throw new FieldErrorException(field, FieldErrorReason.IS_BLANK);
    }
  }

  private static boolean isNotBetween(final String s, final int start,
                                      final int end) {
    return s.length() < start || s.length() > end;
  }
}
