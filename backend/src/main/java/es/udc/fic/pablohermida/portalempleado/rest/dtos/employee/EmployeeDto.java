package es.udc.fic.pablohermida.portalempleado.rest.dtos.employee;

import es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceEmployeeDto;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * The type Employee dto.
 */
public class EmployeeDto {
  @NotBlank
  @Min(2)
  @Max(30)
  private final String name;
  @NotBlank
  @Min(2)
  @Max(30)
  private final String lastname;
  @Min(0)
  @Max(255)
  private final String image;
  @NotNull
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
  private final LocalDate dateBirth;
  @NotBlank
  @Email
  @Min(6)
  @Max(255)
  private final String email;
  @NotBlank
  @Min(9)
  @Max(9)
  private final String dni;
  @NotBlank
  @Min(1)
  @Max(100)
  private final String address;
  @NotBlank
  @Min(1)
  @Max(100)
  private final String addressWork;
  @NotBlank
  @Min(9)
  @Max(12)
  private final String phone;
  @NotBlank
  @Min(1)
  @Max(10)
  private final String duration;
  @NotNull
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
  private final LocalDate dateStart;
  @NotBlank
  @Min(1)
  @Max(30)
  private final String workDay;
  @NotBlank
  @Min(1)
  @Max(300)
  private final String workHours;
  private final Long roleId;
  private final Long jobId;
  private final String jobName;
  private final String supervisorLogin;
  private final String username;
  private final String password;
  private final String iban;
  private final List<VideoConferenceEmployeeDto> vcsystems;

  /**
   * Instantiates a new Employee dto.
   *
   * @param name            the name
   * @param lastname        the lastname
   * @param image           the image
   * @param dateBirth       the date birth
   * @param email           the email
   * @param dni             the dni
   * @param address         the address
   * @param addressWork     the address work
   * @param phone           the phone
   * @param duration        the duration
   * @param dateStart       the date start
   * @param workDay         the work day
   * @param workHours       the work hours
   * @param roleId          the role id
   * @param jobId           the job id
   * @param jobName         the job name
   * @param supervisorLogin the supervisor login
   * @param username        the username
   * @param password        the password
   * @param iban            the iban
   * @param vcsystems       the vcsystems
   */
  public EmployeeDto(final String name, final String lastname,
                     final String image, final LocalDate dateBirth,
                     final String email, final String dni, final String address,
                     final String addressWork, final String phone,
                     final String duration, final LocalDate dateStart,
                     final String workDay, final String workHours,
                     final Long roleId, final Long jobId, final String jobName,
                     final String supervisorLogin, final String username,
                     final String password, final String iban,
                     final List<VideoConferenceEmployeeDto> vcsystems) {
    this.name = name;
    this.lastname = lastname;
    this.image = image;
    this.dateBirth = dateBirth;
    this.email = email;
    this.dni = dni;
    this.address = address;
    this.addressWork = addressWork;
    this.phone = phone;
    this.duration = duration;
    this.dateStart = dateStart;
    this.workDay = workDay;
    this.workHours = workHours;
    this.roleId = roleId;
    this.jobId = jobId;
    this.jobName = jobName;
    this.supervisorLogin = supervisorLogin;
    this.username = username;
    this.password = password;
    this.iban = iban;
    this.vcsystems = vcsystems;
  }

  /**
   * Gets name.
   *
   * @return the name
   */
  public String getName() {
    return this.name;
  }

  /**
   * Gets lastname.
   *
   * @return the lastname
   */
  public String getLastname() {
    return this.lastname;
  }

  /**
   * Gets image.
   *
   * @return the image
   */
  public String getImage() {
    return this.image;
  }

  /**
   * Gets date birth.
   *
   * @return the date birth
   */
  public LocalDate getDateBirth() {
    return this.dateBirth;
  }

  /**
   * Gets email.
   *
   * @return the email
   */
  public String getEmail() {
    return this.email;
  }

  /**
   * Gets dni.
   *
   * @return the dni
   */
  public String getDni() {
    return this.dni;
  }

  /**
   * Gets address.
   *
   * @return the address
   */
  public String getAddress() {
    return this.address;
  }

  /**
   * Gets address work.
   *
   * @return the address work
   */
  public String getAddressWork() {
    return this.addressWork;
  }

  /**
   * Gets phone.
   *
   * @return the phone
   */
  public String getPhone() {
    return this.phone;
  }

  /**
   * Gets duration.
   *
   * @return the duration
   */
  public String getDuration() {
    return this.duration;
  }

  /**
   * Gets date start.
   *
   * @return the date start
   */
  public LocalDate getDateStart() {
    return this.dateStart;
  }

  /**
   * Gets work day.
   *
   * @return the work day
   */
  public String getWorkDay() {
    return this.workDay;
  }

  /**
   * Gets work hours.
   *
   * @return the work hours
   */
  public String getWorkHours() {
    return this.workHours;
  }

  /**
   * Gets role id.
   *
   * @return the role id
   */
  public Long getRoleId() {
    return this.roleId;
  }

  /**
   * Gets job id.
   *
   * @return the job id
   */
  public Long getJobId() {
    return this.jobId;
  }

  /**
   * Gets job name.
   *
   * @return the job name
   */
  public String getJobName() {
    return this.jobName;
  }

  /**
   * Gets supervisor login.
   *
   * @return the supervisor login
   */
  public String getSupervisorLogin() {
    return this.supervisorLogin;
  }

  /**
   * Gets username.
   *
   * @return the username
   */
  public String getUsername() {
    return this.username;
  }

  /**
   * Gets password.
   *
   * @return the password
   */
  public String getPassword() {
    return this.password;
  }

  /**
   * Gets iban.
   *
   * @return the iban
   */
  public String getIban() {
    return this.iban;
  }

  /**
   * Gets vcsystems.
   *
   * @return the vcsystems
   */
  public List<VideoConferenceEmployeeDto> getVcsystems() {
    return this.vcsystems;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || this.getClass() != o.getClass()) { return false; }
    final EmployeeDto that = (EmployeeDto) o;
    return Objects.equals(this.getName(), that.getName()) && Objects
        .equals(this.getLastname(), that.getLastname()) && Objects
        .equals(this.getImage(), that.getImage()) && Objects
        .equals(this.getDateBirth(), that.getDateBirth()) && Objects
        .equals(this.getEmail(), that.getEmail()) && Objects
        .equals(this.getDni(), that.getDni()) && Objects
        .equals(this.getAddress(), that.getAddress()) && Objects
        .equals(this.getAddressWork(), that.getAddressWork()) && Objects
        .equals(this.getPhone(), that.getPhone()) && Objects
        .equals(this.getDuration(), that.getDuration()) && Objects
        .equals(this.getDateStart(), that.getDateStart()) && Objects
        .equals(this.getWorkDay(), that.getWorkDay()) && Objects
        .equals(this.getWorkHours(), that.getWorkHours()) && Objects
        .equals(this.getRoleId(), that.getRoleId()) && Objects
        .equals(this.getJobId(), that.getJobId()) && Objects
        .equals(this.getJobName(), that.getJobName()) && Objects
        .equals(this.getSupervisorLogin(), that.getSupervisorLogin()) && Objects
        .equals(this.getUsername(), that.getUsername()) && Objects
        .equals(this.getPassword(), that.getPassword()) && Objects
        .equals(this.getIban(), that.getIban()) && Objects
        .equals(this.getVcsystems(), that.getVcsystems());
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getName(), this.getLastname(), this.getImage(),
        this.getDateBirth(), this.getEmail(), this.getDni(), this.getAddress(),
        this.getAddressWork(), this.getPhone(), this.getDuration(),
        this.getDateStart(), this.getWorkDay(), this.getWorkHours(),
        this.getRoleId(), this.getJobId(), this.getJobName(),
        this.getSupervisorLogin(), this.getUsername(), this.getPassword(),
        this.getIban(), this.getVcsystems());
  }

  /**
   * The interface All validations.
   */
  public interface AllValidations {
  }
}
