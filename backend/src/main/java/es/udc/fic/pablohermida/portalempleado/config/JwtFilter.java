package es.udc.fic.pablohermida.portalempleado.config;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

/**
 * The type Jwt filter.
 */
public class JwtFilter extends BasicAuthenticationFilter {

  /**
   * jwtGenerator.
   */
  private final JwtGenerator jwtGenerator;

  /**
   * Instantiates a new Jwt generator.
   *
   * @param authenticationManager the authentication manager
   * @param jwtGenerator          the jwt generator
   */
  JwtFilter(final AuthenticationManager authenticationManager,
            final JwtGenerator jwtGenerator) {

    super(authenticationManager);

    this.jwtGenerator = jwtGenerator;

  }

  private static void configureSecurityContext(final String userName,
                                               final String role) {

    final Set<GrantedAuthority> authorities = new HashSet<>();

    authorities.add(new SimpleGrantedAuthority("ROLE_" + role));

    SecurityContextHolder.getContext().setAuthentication(
        new UsernamePasswordAuthenticationToken(userName, null, authorities));

  }

  /**
   * Set security to request.
   *
   * @param request     the request
   * @param response    the response
   * @param filterChain the filter request
   */
  @Override
  protected void doFilterInternal(final HttpServletRequest request,
                                  final HttpServletResponse response,
                                  final FilterChain filterChain)
      throws ServletException, IOException {

    final String authHeaderValue = request.getHeader(HttpHeaders.AUTHORIZATION);

    if (authHeaderValue == null || !authHeaderValue.startsWith("Bearer ")) {
      filterChain.doFilter(request, response);
      return;
    }

    try {

      final String serviceToken = authHeaderValue.replace("Bearer ", "");
      final JwtInfo jwtInfo = this.jwtGenerator.getInfo(serviceToken);

      request.setAttribute("serviceToken", serviceToken);
      request.setAttribute("userId", jwtInfo.getUserId());

      configureSecurityContext(jwtInfo.getUserName(), jwtInfo.getRole());

    } catch (final Exception e) {
      response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
      return;
    }

    filterChain.doFilter(request, response);

  }

}
