package es.udc.fic.pablohermida.portalempleado.rest.dtos.employee;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * The type Job dto.
 */
public class JobDto {
  @NotNull
  private final Long id;
  @NotBlank
  private final String name;

  /**
   * Instantiates a new Job dto.
   *
   * @param id   the id
   * @param name the name
   */
  JobDto(final Long id, final String name) {
    this.id = id;
    this.name = name;
  }

  /**
   * Gets id.
   *
   * @return the id
   */
  public Long getId() {
    return this.id;
  }

  /**
   * Gets name.
   *
   * @return the name
   */
  public String getName() {
    return this.name;
  }
}
