package es.udc.fic.pablohermida.portalempleado.model.daos.employee;

import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Job;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * The interface Job dao.
 */
public interface JobDao extends PagingAndSortingRepository<Job, Long> {
  /**
   * Find all by order by name desc list.
   *
   * @return the list
   */
  List<Job> findAllByOrderByNameDesc();
}
