package es.udc.fic.pablohermida.portalempleado.config;

/**
 * The interface Jwt generator.
 */
public interface JwtGenerator {

  /**
   * Generate string.
   *
   * @param info the info
   *
   * @return the string
   */
  String generate(JwtInfo info);

  /**
   * Gets info.
   *
   * @param token the token
   *
   * @return the info
   */
  JwtInfo getInfo(String token);

}
