package es.udc.fic.pablohermida.portalempleado.rest.dtos.employee;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

/**
 * The type Employee login dto.
 */
public class EmployeeLoginDto {
  @NotBlank
  @Min(1)
  @Max(30)
  private final String username;

  @NotBlank
  @Min(1)
  @Max(96)
  private final String password;

  /**
   * Instantiates a new Employee login dto.
   */
  public EmployeeLoginDto() {
    this.username = null;
    this.password = null;
  }

  /**
   * Instantiates a new Employee login dto.
   *
   * @param username the username
   * @param password the password
   */
  public EmployeeLoginDto(final String username, final String password) {
    this.username = username;
    this.password = password;
  }

  /**
   * Gets username.
   *
   * @return the username
   */
  public String getUsername() {
    return this.username;
  }

  /**
   * Gets password.
   *
   * @return the password
   */
  public String getPassword() {
    return this.password;
  }

  /**
   * The interface All validations.
   */
  public interface AllValidations {
  }
}
