package es.udc.fic.pablohermida.portalempleado.model.services.employee;

import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Employee;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Job;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Role;
import es.udc.fic.pablohermida.portalempleado.model.services.Block;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.AlreadyUnsuscribeException;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.FieldErrorException;
import es.udc.fic.pablohermida.portalempleado.utils.filters.EmployeeFilter;
import java.util.List;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import org.springframework.data.domain.Pageable;

/**
 * The interface Employee service.
 */
public interface EmployeeService {
  /**
   * Sign up long.
   *
   * @param employee the employee
   *
   * @return the long
   *
   * @throws InstanceAlreadyExistsException the instance already
   *                                        exists
   *
   *                                        exception
   * @throws FieldErrorException            something field is illegal
   * @throws InstanceNotFoundException      instance not found on repository
   */
  Long signUp(Employee employee)
      throws InstanceAlreadyExistsException, FieldErrorException,
      InstanceNotFoundException;

  /**
   * Gets employee by username.
   *
   * @param username the username
   *
   * @return the employee by username
   */
  List<Employee> searchEmployeesByUsername(String username);

  /**
   * Search employees block.
   *
   * @param criteria the criteria
   * @param page     the page
   *
   * @return the block
   *
   * @throws FieldErrorException the field error exception
   */
  Block<Employee> searchEmployees(EmployeeFilter criteria, Pageable page)
      throws FieldErrorException;

  /**
   * Gets all roles.
   *
   * @return the all roles
   */
  List<Role> getAllRoles();

  /**
   * Gets role by id.
   *
   * @param id the id
   *
   * @return the role by id
   *
   * @throws InstanceNotFoundException the instance not found exception
   */
  Role getRoleById(Long id) throws InstanceNotFoundException;

  /**
   * Gets all jobs.
   *
   * @return the all jobs
   */
  List<Job> getAllJobs();

  /**
   * Gets job by id.
   *
   * @param id the id
   *
   * @return the job by id
   *
   * @throws InstanceNotFoundException the instance not found exception
   */
  Job getJobById(Long id) throws InstanceNotFoundException;

  /**
   * Gets employee by username.
   *
   * @param username the username
   *
   * @return the employee by username
   *
   * @throws InstanceNotFoundException the instance not found exception
   */
  Employee getEmployeeByUsername(String username)
      throws InstanceNotFoundException;

  /**
   * Gets employee by id.
   *
   * @param employeeId the employee id
   *
   * @return the employee by id
   *
   * @throws InstanceNotFoundException the instance not found exception
   */
  Employee getEmployeeById(Long employeeId) throws InstanceNotFoundException;

  /**
   * Edit employee.
   *
   * @param employee the employee
   *
   * @throws FieldErrorException       the field error exception
   * @throws InstanceNotFoundException the instance not found exception
   */
  void editEmployee(Employee employee)
      throws FieldErrorException, InstanceNotFoundException;

  /**
   * Unsuscribe employee.
   *
   * @param employeeId the employee id
   *
   * @throws InstanceNotFoundException  the instance not found exception
   * @throws AlreadyUnsuscribeException the already unsuscribe exception
   */
  void unsuscribeEmployee(Long employeeId)
      throws InstanceNotFoundException, AlreadyUnsuscribeException;
}
