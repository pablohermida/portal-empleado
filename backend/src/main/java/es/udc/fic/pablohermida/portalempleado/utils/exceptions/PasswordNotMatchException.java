package es.udc.fic.pablohermida.portalempleado.utils.exceptions;

/**
 * The type Password not match exception.
 */
public class PasswordNotMatchException extends Exception {

  /**
   * Instantiates a new Password not match exception.
   */
  public PasswordNotMatchException() {
    super("Password not match");
  }
}
