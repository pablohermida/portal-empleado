package es.udc.fic.pablohermida.portalempleado.utils.exceptions;

/**
 * The type Already unsuscribe exception.
 */
public class AlreadyUnsuscribeException extends Exception {
  /**
   * Instantiates a new Already unsuscribe exception.
   *
   * @param username the username
   */
  public AlreadyUnsuscribeException(final String username) {
    super(username + " already unsuscribed");
  }
}
