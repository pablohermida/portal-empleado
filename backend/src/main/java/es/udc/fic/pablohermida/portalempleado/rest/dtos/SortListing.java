package es.udc.fic.pablohermida.portalempleado.rest.dtos;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.domain.Sort;

/**
 * The type Sort listing.
 */
public abstract class SortListing {
  private final Sort.Direction order;
  private final List<String> orderAttribute;

  /**
   * Instantiates a new Sort listing.
   *
   * @param order          the order
   * @param orderAttribute the order attribute
   */
  public SortListing(final String order, final List<String> orderAttribute) {
    if (order != null) {
      this.order = Sort.Direction.fromString(order);
    } else {
      this.order = Sort.Direction.ASC;
    }
    if (orderAttribute != null) {
      this.orderAttribute = orderAttribute;
    } else {
      this.orderAttribute = new ArrayList<>();
    }
  }

  /**
   * Gets order.
   *
   * @return the order
   */
  public Sort.Direction getOrder() {
    return this.order;
  }

  /**
   * Gets order attribute.
   *
   * @return the order attribute
   */
  public List<String> getOrderAttribute() {
    return this.orderAttribute;
  }
}
