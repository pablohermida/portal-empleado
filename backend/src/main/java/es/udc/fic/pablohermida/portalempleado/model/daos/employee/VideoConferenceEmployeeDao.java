package es.udc.fic.pablohermida.portalempleado.model.daos.employee;

import es.udc.fic.pablohermida.portalempleado.model.entities.employee.VideoConferenceEmployee;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * The interface Video conference employee dao.
 */
public interface VideoConferenceEmployeeDao
    extends PagingAndSortingRepository<VideoConferenceEmployee, Long> {
}
