package es.udc.fic.pablohermida.portalempleado.model.services.employee;

import es.udc.fic.pablohermida.portalempleado.model.daos.employee.EmployeeDao;
import es.udc.fic.pablohermida.portalempleado.model.daos.employee.JobDao;
import es.udc.fic.pablohermida.portalempleado.model.daos.employee.RoleDao;
import es.udc.fic.pablohermida.portalempleado.model.daos.employee.VideoConferenceEmployeeDao;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Employee;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.EmployeeSecurityData;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Job;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Role;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.VideoConferenceEmployee;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.VideoConferenceEmployeeKey;
import es.udc.fic.pablohermida.portalempleado.model.services.Block;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.AlreadyUnsuscribeException;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.FieldErrorException;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.FieldErrorReason;
import es.udc.fic.pablohermida.portalempleado.utils.filters.EmployeeFilter;
import es.udc.fic.pablohermida.portalempleado.utils.validators.EmployeeValidator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The type Employee service.
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

  /**
   * The type Employee dao.
   */
  @Autowired
  private EmployeeDao employeeDao;

  /**
   * The type Role dao.
   */
  @Autowired
  private RoleDao roleDao;

  /**
   * The type Job dao.
   */
  @Autowired
  private JobDao jobDao;

  @Autowired
  private VideoConferenceEmployeeDao vcemployeeDao;

  @Transactional
  @Override
  public Long signUp(final Employee employee)
      throws InstanceAlreadyExistsException, FieldErrorException,
      InstanceNotFoundException {
    EmployeeValidator.validate(employee);
    final String username = employee.getEmployeeAuth().getUsername();
    if (this.employeeDao.existsByEmployeeAuthUsername(username)) {
      throw new InstanceAlreadyExistsException("Employee already exists");
    }

    this.checkSupervisorExist(employee.getSupervisor());

    final Employee employeeSave = this.employeeDao.save(employee);

    if (employee.getVcsystems() != null) {
      this.saveVcSystemsEmployee(employeeSave.getId(), employee.getVcsystems());
    }

    return employeeSave.getId();
  }

  @Override
  public List<Employee> searchEmployeesByUsername(final String username) {
    return this.employeeDao
        .findAllByEmployeeAuthUsernameIgnoreCaseContainingOrderByEmployeeAuthUsernameDesc(
            username);
  }

  public Block<Employee> searchEmployees(final EmployeeFilter criteria,
                                         Pageable page)
      throws FieldErrorException {
    if (page.getSort().equals(Sort.unsorted())) {
      page = PageRequest.of(page.getPageNumber(), page.getPageSize(),
          Sort.by(Sort.Direction.ASC, "id"));
    }
    try {
      final Page<Employee> pageEmployee = this.employeeDao
          .findAllByFilters(criteria.getUsername(), criteria.getName(),
              criteria.getEmail(), criteria.getJobsId(), criteria.getRoleId(),
              page);
      return new Block<>(pageEmployee.getContent(), pageEmployee.hasNext(),
          pageEmployee.getTotalPages());
    } catch (final Exception e) {
      throw new FieldErrorException(page.getSort().toString(),
          FieldErrorReason.INSTANCE);
    }
  }

  @Override
  public List<Role> getAllRoles() {
    return this.roleDao.findAllByOrderByNameAsc();
  }

  @Override
  public Role getRoleById(final Long id) throws InstanceNotFoundException {
    final Optional<Role> role = this.roleDao.findById(id);
    if (!role.isPresent()) {
      throw new InstanceNotFoundException("role");
    }
    return role.get();
  }

  @Override
  public List<Job> getAllJobs() {
    return this.jobDao.findAllByOrderByNameDesc();
  }

  @Override
  public Job getJobById(final Long id) throws InstanceNotFoundException {
    final Optional<Job> job = this.jobDao.findById(id);
    if (!job.isPresent()) {
      throw new InstanceNotFoundException("job");
    }
    return job.get();
  }

  @Override
  public Employee getEmployeeByUsername(final String username)
      throws InstanceNotFoundException {
    final Optional<Employee> employee =
        this.employeeDao.findByEmployeeAuthUsername(username);
    if (!employee.isPresent()) {
      throw new InstanceNotFoundException("employee");
    }
    return employee.get();
  }

  @Override
  public Employee getEmployeeById(final Long employeeId)
      throws InstanceNotFoundException {
    final Optional<Employee> employee = this.employeeDao.findById(employeeId);
    if (!employee.isPresent()) {
      throw new InstanceNotFoundException("employee");
    }
    return employee.get();
  }

  @Transactional
  @Override
  public void editEmployee(final Employee employee)
      throws FieldErrorException, InstanceNotFoundException {
    EmployeeValidator.validate(employee);
    final Employee employeeFound = this.getEmployeeById(employee.getId());
    final Set<VideoConferenceEmployee> vceSet = employee.getVcsystems();
    employee.setVcsystems(null);

    this.checkSupervisorExist(employee.getSupervisor());

    this.employeeDao.save(employee);

    if (vceSet != null) {
      this.saveVcSystemsEmployee(employeeFound.getId(), vceSet);
    }
  }

  @Override
  public void unsuscribeEmployee(final Long employeeId)
      throws InstanceNotFoundException, AlreadyUnsuscribeException {
    final Optional<Employee> employee = this.employeeDao.findById(employeeId);
    if (!employee.isPresent()) {
      throw new InstanceNotFoundException("Employee doesn´t exist");
    }
    final Employee employeeGet = employee.get();
    if (!employeeGet.getEmployeeSecurityData().isSuscribed()) {
      throw new AlreadyUnsuscribeException(
          employeeGet.getEmployeeAuth().getUsername());
    }

    employeeGet.setEmployeeSecurityData(new EmployeeSecurityData(employeeId,
        employeeGet.getEmployeeSecurityData().getIban(), false));

    this.employeeDao.save(employeeGet);
  }

  private boolean checkSupervisorExist(final Employee supervisor)
      throws InstanceNotFoundException {
    if (supervisor != null) {
      final Long supervisorId = supervisor.getId();
      if (!this.employeeDao.findById(supervisorId).isPresent()) {
        throw new InstanceNotFoundException("Supervisor doesn´t exist");
      }
    }

    return true;
  }

  private void saveVcSystemsEmployee(final Long employeeId,
                                     final Set<VideoConferenceEmployee> vces) {
    for (final VideoConferenceEmployee vce : vces) {
      vce.setId(new VideoConferenceEmployeeKey(employeeId,
          vce.getId().getIdVcsystem()));
      this.vcemployeeDao.save(vce);
    }
  }
}
