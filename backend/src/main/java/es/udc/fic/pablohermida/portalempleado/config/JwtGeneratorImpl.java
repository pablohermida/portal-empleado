package es.udc.fic.pablohermida.portalempleado.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * The type Jwt generator.
 */
@Component
public class JwtGeneratorImpl implements JwtGenerator {
  @Value("${project.jwt.signKey}")
  private String signKey;

  @Value("${project.jwt.expirationMinutes}")
  private long expirationMinutes;

  @Override
  public String generate(final JwtInfo info) {
    final int interval = 60000;
    return Jwts.builder().claim("userId", info.getUserId())
        .claim("role", info.getRole()).setExpiration(new Date(
            System.currentTimeMillis() + this.expirationMinutes * interval))
        .signWith(SignatureAlgorithm.HS512, this.signKey.getBytes()).compact();

  }

  @Override
  public JwtInfo getInfo(final String token) {

    final Claims claims = Jwts.parser().setSigningKey(this.signKey.getBytes())
        .parseClaimsJws(token).getBody();

    return new JwtInfo(((Integer) claims.get("userId")).longValue(),
        claims.getSubject(), (String) claims.get("role"));

  }

}
