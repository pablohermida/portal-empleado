package es.udc.fic.pablohermida.portalempleado.rest.controllers;

import static es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceSystemConversor.toVideoConferenceSystem;
import static es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceSystemConversor.toVideoConferenceSystemDtos;

import es.udc.fic.pablohermida.portalempleado.model.entities.employee.VideoConferenceSystem;
import es.udc.fic.pablohermida.portalempleado.model.services.employee.VideoConferenceSystemService;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.IdDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceSystemCreateDto;
import es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem.VideoConferenceSystemDto;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.FieldErrorException;
import java.util.List;
import javax.management.InstanceAlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The type Video conference system controller.
 */
@RestController
@RequestMapping("/videoconference")
public class VideoConferenceSystemController extends ExceptionController {
  @Autowired
  private VideoConferenceSystemService vcService;

  /**
   * Add video conference system id dto.
   *
   * @param vcsDto the vcs dto
   *
   * @return the id dto
   *
   * @throws FieldErrorException            the field error exception
   * @throws InstanceAlreadyExistsException the instance already exists
   *
   *                                        exception
   */
  @PostMapping("")
  public IdDto addVideoConferenceSystem(
      @Validated({VideoConferenceSystemCreateDto.AllValidations.class})
      @RequestBody final VideoConferenceSystemCreateDto vcsDto)
      throws FieldErrorException, InstanceAlreadyExistsException {

    final VideoConferenceSystem vcs = toVideoConferenceSystem(vcsDto);
    return new IdDto(this.vcService.createVideoConferenceSystem(vcs));
  }

  /**
   * Gets all video conference system.
   *
   * @return the all video conference system
   */
  @GetMapping("")
  public List<VideoConferenceSystemDto> getAllVideoConferenceSystem() {
    return toVideoConferenceSystemDtos(
        this.vcService.getVideoConferenceSystems());
  }
}
