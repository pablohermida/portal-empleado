package es.udc.fic.pablohermida.portalempleado.utils.filters;

/**
 * The enum Employee order enum.
 */
public enum EmployeeOrderEnum {
  /**
   * Username employee order enum.
   */
  USERNAME("employeeAuth.username");

  private final String field;

  EmployeeOrderEnum(final String field) {
    this.field = field;
  }

  /**
   * From field string.
   *
   * @param field the field
   *
   * @return the string
   */
  public static String fromField(final String field) {
    try {
      return valueOf(field.toUpperCase()).field;
    } catch (final Exception e) {
      return field;
    }
  }
}
