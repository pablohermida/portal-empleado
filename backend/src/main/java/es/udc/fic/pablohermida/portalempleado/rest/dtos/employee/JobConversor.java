package es.udc.fic.pablohermida.portalempleado.rest.dtos.employee;

import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Job;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Job conversor.
 */
public class JobConversor {

  /**
   * Instantiates a new Job conversor.
   */
  public JobConversor() {}

  /**
   * To job dtos list.
   *
   * @param jobs the jobs
   *
   * @return the list
   */
  public static List<JobDto> toJobDtos(final List<Job> jobs) {
    return jobs.stream().map(JobConversor::toJobDto)
        .collect(Collectors.toList());
  }

  private static JobDto toJobDto(final Job job) {
    return new JobDto(job.getId(), job.getName());
  }
}
