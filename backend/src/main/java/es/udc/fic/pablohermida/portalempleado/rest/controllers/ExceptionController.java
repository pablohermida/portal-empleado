package es.udc.fic.pablohermida.portalempleado.rest.controllers;

import es.udc.fic.pablohermida.portalempleado.rest.dtos.ErrorsDto;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.AlreadyUnsuscribeException;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.FieldErrorException;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.NotImplementedException;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.PasswordNotMatchException;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.PermissionException;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * The type Exception controller.
 */
public class ExceptionController {

  /**
   * Handle not implemented exception errors dto.
   *
   * @param exception the exception
   *
   * @return the errors dto
   */
  @ExceptionHandler(NotImplementedException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public static ErrorsDto handleNotImplementedException(
      final NotImplementedException exception) {
    return new ErrorsDto("exceptions.NotImplementedException",
        exception.getMessage());
  }

  /**
   * Handle instance not found exception errors dto.
   *
   * @param exception the exception
   *
   * @return the errors dto
   */
  @ExceptionHandler(InstanceNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ResponseBody
  public static ErrorsDto handleInstanceNotFoundException(
      final InstanceNotFoundException exception) {
    return new ErrorsDto("exceptions.InstanceNotFoundException",
        exception.getMessage());
  }

  /**
   * Handle instance already exists exception errors dto.
   *
   * @param exception the exception
   *
   * @return the errors dto
   */
  @ExceptionHandler(InstanceAlreadyExistsException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public static ErrorsDto handleInstanceAlreadyExistsException(
      final InstanceAlreadyExistsException exception) {
    return new ErrorsDto("exceptions.InstanceAlreadyExistsException",
        exception.getMessage());
  }

  /**
   * Handle field error exception errors dto.
   *
   * @param exception the exception
   *
   * @return the errors dto
   */
  @ExceptionHandler(FieldErrorException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public static ErrorsDto handleFieldErrorException(
      final FieldErrorException exception) {
    return new ErrorsDto("exceptions.FieldErrorException",
        exception.getMessage());
  }

  /**
   * Handle password not match exception errors dto.
   *
   * @param exception the exception
   *
   * @return the errors dto
   */
  @ExceptionHandler(PasswordNotMatchException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public static ErrorsDto handlePasswordNotMatchException(
      final PasswordNotMatchException exception) {
    return new ErrorsDto("exceptions.PasswordNotMatchException",
        exception.getMessage());
  }

  /**
   * Handle already unsuscribe exception errors dto.
   *
   * @param exception the exception
   *
   * @return the errors dto
   */
  @ExceptionHandler(AlreadyUnsuscribeException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public static ErrorsDto handleAlreadyUnsuscribeException(
      final AlreadyUnsuscribeException exception) {
    return new ErrorsDto("exceptions.AlreadyUnsuscribeException",
        exception.getMessage());
  }

  /**
   * Handle permission exception errors dto.
   *
   * @param exception the exception
   *
   * @return the errors dto
   */
  @ExceptionHandler(PermissionException.class)
  @ResponseStatus(HttpStatus.FORBIDDEN)
  @ResponseBody
  public static ErrorsDto handlePermissionException(
      final PermissionException exception) {
    return new ErrorsDto("exceptions.PermissionException",
        exception.getMessage());
  }
}
