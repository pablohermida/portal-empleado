package es.udc.fic.pablohermida.portalempleado.rest.dtos.employee;

import java.util.Objects;

/**
 * The type Employee auth dto.
 */
public class EmployeeAuthDto {
  private final Long employeeId;
  private final String username;
  private final Long roleId;
  private final String token;

  /**
   * Instantiates a new Employee auth dto.
   *
   * @param employeeId the employee id
   * @param username   the username
   * @param roleId     the role id
   * @param token      the token
   */
  EmployeeAuthDto(final Long employeeId, final String username,
                  final Long roleId, final String token) {
    this.employeeId = employeeId;
    this.username = username;
    this.roleId = roleId;
    this.token = token;
  }

  /**
   * Gets employee id.
   *
   * @return the employee id
   */
  public Long getEmployeeId() {
    return this.employeeId;
  }

  /**
   * Gets username.
   *
   * @return the username
   */
  public String getUsername() {
    return this.username;
  }

  /**
   * Gets role id.
   *
   * @return the role id
   */
  public Long getRoleId() {
    return this.roleId;
  }

  /**
   * Gets token.
   *
   * @return the token
   */
  public String getToken() {
    return this.token;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || this.getClass() != o.getClass()) { return false; }
    final EmployeeAuthDto that = (EmployeeAuthDto) o;
    return Objects.equals(this.getEmployeeId(), that.getEmployeeId()) && Objects
        .equals(this.getUsername(), that.getUsername()) && Objects
        .equals(this.getRoleId(), that.getRoleId());
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(this.getEmployeeId(), this.getUsername(), this.getRoleId());
  }
}
