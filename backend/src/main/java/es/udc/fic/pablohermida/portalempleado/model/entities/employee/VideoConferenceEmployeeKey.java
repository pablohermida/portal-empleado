package es.udc.fic.pablohermida.portalempleado.model.entities.employee;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The type Video conference employee key.
 */
@Embeddable
public class VideoConferenceEmployeeKey implements Serializable {
  @Column(name = "id_employee")
  private Long idEmployee;

  @Column(name = "id_vcsystem")
  private Long idVcsystem;

  /**
   * Instantiates a new Video conference employee key.
   */
  public VideoConferenceEmployeeKey() {
  }

  /**
   * Instantiates a new Video conference employee key.
   *
   * @param idEmployee the id employee
   * @param idVcsystem the id vcsystem
   */
  public VideoConferenceEmployeeKey(final Long idEmployee,
                                    final Long idVcsystem) {
    this.idEmployee = idEmployee;
    this.idVcsystem = idVcsystem;
  }

  /**
   * Gets id employee.
   *
   * @return the id employee
   */
  public Long getIdEmployee() {
    return this.idEmployee;
  }

  /**
   * Gets id vcsystem.
   *
   * @return the id vcsystem
   */
  public Long getIdVcsystem() {
    return this.idVcsystem;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || this.getClass() != o.getClass()) { return false; }
    final VideoConferenceEmployeeKey that = (VideoConferenceEmployeeKey) o;
    return Objects.equals(this.getIdEmployee(), that.getIdEmployee()) && Objects
        .equals(this.getIdVcsystem(), that.getIdVcsystem());
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getIdEmployee(), this.getIdVcsystem());
  }
}
