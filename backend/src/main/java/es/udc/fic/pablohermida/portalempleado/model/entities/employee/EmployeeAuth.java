package es.udc.fic.pablohermida.portalempleado.model.entities.employee;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The type Employee auth.
 */
@Entity
@Table(name = "employee_auth", schema = "portalempleado")
public class EmployeeAuth {

  /**
   * The Employee id.
   */
  @Id
  @Column(name = "employeeId")
  private Long employeeId;
  /**
   * The Username.
   */
  private String username;
  /**
   * The Password.
   */
  private String password;
  /**
   * The Recovery password.
   */
  @Column(name = "recovery_pass")
  private String recoveryPassword;

  @OneToOne
  @MapsId
  @JoinColumn(name = "employeeId")
  private Employee employee;

  /**
   * Instantiates a new Employee auth.
   */
  public EmployeeAuth() {
  }

  /**
   * Instantiates a new Employee auth.
   *
   * @param username         the username
   * @param password         the password
   * @param recoveryPassword the recovery password
   */
  public EmployeeAuth(final String username, final String password,
                      final String recoveryPassword) {
    this.username = username;
    this.password = password;
    this.recoveryPassword = recoveryPassword;
  }

  /**
   * Instantiates a new Employee auth.
   *
   * @param employeeId       the employee id
   * @param username         the username
   * @param password         the password
   * @param recoveryPassword the recovery password
   */
  public EmployeeAuth(final Long employeeId, final String username,
                      final String password, final String recoveryPassword) {
    this(username, password, recoveryPassword);
    this.employeeId = employeeId;
  }

  /**
   * Gets employee id.
   *
   * @return the employee id
   */
  public Long getEmployeeId() {
    return this.employeeId;
  }

  /**
   * Gets username.
   *
   * @return the username
   */
  public String getUsername() {
    return this.username;
  }

  /**
   * Gets password.
   *
   * @return the password
   */
  public String getPassword() {
    return this.password;
  }

  /**
   * Gets recovery password.
   *
   * @return the recovery password
   */
  private String getRecoveryPassword() {
    return this.recoveryPassword;
  }

  /**
   * Gets employee.
   *
   * @return the employee
   */
  public Employee getEmployee() {
    return this.employee;
  }

  /**
   * Sets employee.
   *
   * @param employee the employee
   */
  public void setEmployee(final Employee employee) {
    this.employee = employee;
  }

  /**
   * Override method equals of Object class.
   *
   * @param o the employee
   */
  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || this.getClass() != o.getClass()) {
      return false;
    }
    final EmployeeAuth that = (EmployeeAuth) o;
    return Objects.equals(this.getUsername(), that.getUsername()) && Objects
        .equals(this.getPassword(), that.getPassword()) && Objects
        .equals(this.getRecoveryPassword(), that.getRecoveryPassword());
  }

  /**
   * Override method hashCode of Object class.
   *
   * @return int
   */
  @Override
  public int hashCode() {
    return Objects.hash(this.getUsername(), this.getPassword(),
        this.getRecoveryPassword());
  }
}
