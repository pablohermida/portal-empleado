package es.udc.fic.pablohermida.portalempleado.rest.dtos.employee;

/**
 * The type Employee search dto.
 */
public class EmployeeSearchDto {
  private final String username;
  private final String fullname;

  /**
   * Instantiates a new Employee search dto.
   *
   * @param username the username
   * @param fullname the fullname
   */
  EmployeeSearchDto(final String username, final String fullname) {
    this.username = username;
    this.fullname = fullname;
  }

  /**
   * Gets username.
   *
   * @return the username
   */
  public String getUsername() {
    return this.username;
  }

  /**
   * Gets fullname.
   *
   * @return the fullname
   */
  public String getFullname() {
    return this.fullname;
  }
}
