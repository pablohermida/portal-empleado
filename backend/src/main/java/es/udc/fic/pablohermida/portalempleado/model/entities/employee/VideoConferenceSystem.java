package es.udc.fic.pablohermida.portalempleado.model.entities.employee;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The type Video conference system.
 */
@Entity
@Table(name = "videoconference_system", schema = "portalempleado")
public class VideoConferenceSystem {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String platform;

  /**
   * Instantiates a new Video conference system.
   */
  public VideoConferenceSystem() {
  }

  /**
   * Instantiates a new Video conference system.
   *
   * @param platform the platform
   */
  public VideoConferenceSystem(final String platform) {
    this.platform = platform;
  }

  /**
   * Gets id.
   *
   * @return the id
   */
  public Long getId() {
    return this.id;
  }

  /**
   * Gets platform.
   *
   * @return the platform
   */
  public String getPlatform() {
    return this.platform;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || this.getClass() != o.getClass()) { return false; }
    final VideoConferenceSystem that = (VideoConferenceSystem) o;
    return Objects.equals(this.getPlatform(), that.getPlatform());
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getPlatform());
  }
}
