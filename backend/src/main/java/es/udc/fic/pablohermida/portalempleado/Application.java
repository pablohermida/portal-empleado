package es.udc.fic.pablohermida.portalempleado;

import javax.sql.DataSource;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * The type Application.
 */
@SpringBootApplication
public class Application {

  @Autowired
  private DataSource dataSource;

  /**
   * Instantiates a new Application.
   */
  public Application() {
  }

  /**
   * Main.
   *
   * @param args the args
   */
  public static void main(final String[] args) {
    SpringApplication.run(Application.class, args);
  }

  /**
   * Password encoder b crypt password encoder.
   *
   * @return the b crypt password encoder
   */
  @Bean
  public static BCryptPasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  /**
   * Liquibase spring liquibase.
   *
   * @return the spring liquibase
   */
  @Bean
  public SpringLiquibase liquibase() {
    final SpringLiquibase liquibase = new SpringLiquibase();
    liquibase.setChangeLog("classpath:liquibase/db.changelog-master.xml");
    liquibase.setDataSource(this.dataSource);
    return liquibase;
  }

}
