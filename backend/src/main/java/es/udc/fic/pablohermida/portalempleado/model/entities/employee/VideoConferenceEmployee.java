package es.udc.fic.pablohermida.portalempleado.model.entities.employee;

import java.util.Objects;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

/**
 * The type Video conference employee.
 */
@Entity
@Table(name = "videoconference_system_employee", schema = "portalempleado")
public class VideoConferenceEmployee {
  @EmbeddedId
  private VideoConferenceEmployeeKey id;

  @ManyToOne
  @MapsId("id")
  @JoinColumn(name = "id_employee")
  private Employee employee;

  @ManyToOne
  @MapsId("id")
  @JoinColumn(name = "id_vcsystem")
  private VideoConferenceSystem vcsystem;

  private String data;

  /**
   * Instantiates a new Video conference employee.
   */
  public VideoConferenceEmployee() {
  }

  private VideoConferenceEmployee(final String data) {
    this.data = data;
  }

  /**
   * Instantiates a new Video conference employee.
   *
   * @param id   the id
   * @param data the data
   */
  public VideoConferenceEmployee(final VideoConferenceEmployeeKey id,
                                 final String data) {
    this(data);
    this.id = id;
  }

  /**
   * Gets id.
   *
   * @return the id
   */
  public VideoConferenceEmployeeKey getId() {
    return this.id;
  }

  /**
   * Sets id.
   *
   * @param id the id
   */
  public void setId(final VideoConferenceEmployeeKey id) {
    this.id = id;
  }

  /**
   * Gets data.
   *
   * @return the data
   */
  public String getData() {
    return this.data;
  }

  /**
   * Gets vcsystem.
   *
   * @return the vcsystem
   */
  public VideoConferenceSystem getVcsystem() {
    return this.vcsystem;
  }

  /**
   * Sets vcsystem.
   *
   * @param vcsystem the vcsystem
   */
  public void setVcsystem(final VideoConferenceSystem vcsystem) {
    this.vcsystem = vcsystem;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || this.getClass() != o.getClass()) { return false; }
    final VideoConferenceEmployee that = (VideoConferenceEmployee) o;
    return Objects.equals(this.getId(), that.getId()) && Objects
        .equals(this.getData(), that.getData());
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getId(), this.getData());
  }
}
