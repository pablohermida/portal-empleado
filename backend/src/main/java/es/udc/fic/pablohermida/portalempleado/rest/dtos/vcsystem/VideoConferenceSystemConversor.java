package es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem;

import es.udc.fic.pablohermida.portalempleado.model.entities.employee.VideoConferenceEmployee;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.VideoConferenceEmployeeKey;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.VideoConferenceSystem;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * The type Video conference system conversor.
 */
public class VideoConferenceSystemConversor {
  /**
   * Instantiates a new Video conference system conversor.
   */
  public VideoConferenceSystemConversor() {}

  /**
   * To video conference system dtos list.
   *
   * @param vcs the vcs
   *
   * @return the list
   */
  public static List<VideoConferenceSystemDto> toVideoConferenceSystemDtos(
      final List<VideoConferenceSystem> vcs) {
    return vcs.stream()
        .map(VideoConferenceSystemConversor::toVideoConferenceSystemDto)
        .collect(Collectors.toList());
  }

  private static VideoConferenceSystemDto toVideoConferenceSystemDto(
      final VideoConferenceSystem vc) {
    return new VideoConferenceSystemDto(vc.getId(), vc.getPlatform());
  }

  /**
   * To video conference system video conference system.
   *
   * @param vcDto the vc dto
   *
   * @return the video conference system
   */
  public static VideoConferenceSystem toVideoConferenceSystem(
      final VideoConferenceSystemCreateDto vcDto) {
    return new VideoConferenceSystem(vcDto.getPlatform());
  }

  /**
   * To video conference employees list.
   *
   * @param vce the vce
   *
   * @return the list
   */
  public static List<VideoConferenceEmployee> toVideoConferenceEmployees(
      final List<VideoConferenceEmployeeDto> vce) {
    return vce.stream()
        .map(VideoConferenceSystemConversor::toVideoConferenceEmployee)
        .collect(Collectors.toList());
  }

  /**
   * To video conference employees list.
   *
   * @param employeeId the employee id
   * @param vce        the vce
   *
   * @return the list
   */
  public static List<VideoConferenceEmployee> toVideoConferenceEmployees(
      final Long employeeId, final List<VideoConferenceEmployeeDto> vce) {
    return vce.stream().map(vc -> toVideoConferenceEmployee(employeeId, vc))
        .collect(Collectors.toList());
  }

  private static VideoConferenceEmployee toVideoConferenceEmployee(
      final Long employeeId, final VideoConferenceEmployeeDto vc) {
    return new VideoConferenceEmployee(
        new VideoConferenceEmployeeKey(employeeId, vc.getIdVcsystem()),
        vc.getData());
  }

  private static VideoConferenceEmployee toVideoConferenceEmployee(
      final VideoConferenceEmployeeDto vc) {
    return new VideoConferenceEmployee(
        new VideoConferenceEmployeeKey(null, vc.getIdVcsystem()), vc.getData());
  }

  /**
   * To video conference employee dtos list.
   *
   * @param vce the vce
   *
   * @return the list
   */
  public static List<VideoConferenceEmployeeDto> toVideoConferenceEmployeeDtos(
      final Set<VideoConferenceEmployee> vce) {
    List<VideoConferenceEmployeeDto> vceDtoList = null;
    if (vce != null) {
      vceDtoList = vce.stream()
          .map(VideoConferenceSystemConversor::toVideoConferenceEmployeeDto)
          .collect(Collectors.toList());
    }
    return vceDtoList;
  }

  private static VideoConferenceEmployeeDto toVideoConferenceEmployeeDto(
      final VideoConferenceEmployee vc) {
    return new VideoConferenceEmployeeDto(vc.getId().getIdVcsystem(),
        vc.getData(), vc.getVcsystem().getPlatform());
  }
}
