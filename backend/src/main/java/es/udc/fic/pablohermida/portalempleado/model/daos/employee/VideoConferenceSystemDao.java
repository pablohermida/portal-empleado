package es.udc.fic.pablohermida.portalempleado.model.daos.employee;

import es.udc.fic.pablohermida.portalempleado.model.entities.employee.VideoConferenceSystem;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * The interface Video conference system dao.
 */
public interface VideoConferenceSystemDao
    extends PagingAndSortingRepository<VideoConferenceSystem, Long> {

  /**
   * Exists by platform ignore case containing boolean.
   *
   * @param platform the platform
   *
   * @return the boolean
   */
  boolean existsByPlatformIgnoreCaseContaining(String platform);

  /**
   * Find all by order by platform list.
   *
   * @return the list
   */
  List<VideoConferenceSystem> findAllByOrderByPlatform();
}
