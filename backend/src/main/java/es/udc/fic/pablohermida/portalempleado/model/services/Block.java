package es.udc.fic.pablohermida.portalempleado.model.services;

import java.util.List;
import java.util.Objects;

/**
 * The type Block.
 *
 * @param <T> the type parameter
 */
public class Block<T> {

  private final List<T> items;
  private final boolean existMoreItems;
  private final int totalPages;

  /**
   * Instantiates a new Block.
   *
   * @param items          the items
   * @param existMoreItems the exist more items
   * @param totalPages     the total pages
   */
  public Block(final List<T> items, final boolean existMoreItems,
               final int totalPages) {

    this.items = items;
    this.existMoreItems = existMoreItems;
    this.totalPages = totalPages;

  }

  /**
   * Gets items.
   *
   * @return the items
   */
  public List<T> getItems() {
    return this.items;
  }

  /**
   * Gets exist more items.
   *
   * @return the exist more items
   */
  public boolean getExistMoreItems() {
    return this.existMoreItems;
  }

  /**
   * Gets total pages.
   *
   * @return the total pages
   */
  public int getTotalPages() {
    return this.totalPages;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || this.getClass() != o.getClass()) { return false; }
    final Block<?> block = (Block<?>) o;
    return this.getExistMoreItems() == block.getExistMoreItems()
        && this.getTotalPages() == block.getTotalPages() && Objects
        .equals(this.getItems(), block.getItems());
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(this.getItems(), this.getExistMoreItems(), this.getTotalPages());
  }
}
