package es.udc.fic.pablohermida.portalempleado.model.daos.employee;

import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Employee;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * The interface Employee dao.
 */
public interface EmployeeDao
    extends PagingAndSortingRepository<Employee, Long> {
  /**
   * Exists by username boolean.
   *
   * @param username the username
   *
   * @return the boolean
   */
  boolean existsByEmployeeAuthUsername(String username);

  /**
   * Find all by employee auth username ignore case containing order by employee
   * auth username desc list.
   *
   * @param username the username
   *
   * @return the list
   */
  List<Employee> findAllByEmployeeAuthUsernameIgnoreCaseContainingOrderByEmployeeAuthUsernameDesc(
      String username);

  /**
   * Find by employee auth username optional.
   *
   * @param username the username
   *
   * @return the optional
   */
  Optional<Employee> findByEmployeeAuthUsername(String username);

  /**
   * Find all by filters page.
   *
   * @param username the username
   * @param name     the name
   * @param email    the email
   * @param jobsId   the jobs id
   * @param roleId   the role id
   * @param page     the page
   *
   * @return the page
   */
  @Query("SELECT e FROM Employee e WHERE "
      + "employeeSecurityData.suscribed = true AND "
      + "(?1 IS NULL OR employeeAuth.username LIKE %?1%) AND"
      + "(?2 IS NULL OR name LIKE %?2% OR lastname LIKE %?2%) AND"
      + "(?3 IS NULL OR email LIKE %?3%) AND"
      + "((?4) IS NULL OR jobId IN (?4)) AND" + "(?5 IS NULL OR roleId = ?5)")
  Page<Employee> findAllByFilters(String username, String name, String email,
                                  List<Long> jobsId, Long roleId,
                                  Pageable page);
}
