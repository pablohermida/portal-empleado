package es.udc.fic.pablohermida.portalempleado.rest.dtos.employee;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * The type Role dto.
 */
public class RoleDto {
  @NotNull
  private final Long id;
  @NotBlank
  private final String name;

  /**
   * Instantiates a new Role dto.
   *
   * @param id   the id
   * @param name the name
   */
  RoleDto(final Long id, final String name) {
    this.id = id;
    this.name = name;
  }

  /**
   * Gets id.
   *
   * @return the id
   */
  public Long getId() {
    return this.id;
  }

  /**
   * Gets name.
   *
   * @return the name
   */
  public String getName() {
    return this.name;
  }
}
