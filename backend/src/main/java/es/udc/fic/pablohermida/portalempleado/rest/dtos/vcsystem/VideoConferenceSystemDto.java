package es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem;

import java.util.Objects;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * The type Video conference system dto.
 */
public class VideoConferenceSystemDto {
  @NotNull
  private final Long id;
  @NotBlank
  private final String platform;

  /**
   * Instantiates a new Video conference system dto.
   *
   * @param id       the id
   * @param platform the platform
   */
  public VideoConferenceSystemDto(final Long id, final String platform) {
    this.id = id;
    this.platform = platform;
  }

  /**
   * Gets id.
   *
   * @return the id
   */
  public Long getId() {
    return this.id;
  }

  /**
   * Gets platform.
   *
   * @return the platform
   */
  public String getPlatform() {
    return this.platform;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || this.getClass() != o.getClass()) { return false; }
    final VideoConferenceSystemDto that = (VideoConferenceSystemDto) o;
    return Objects.equals(this.getPlatform(), that.getPlatform());
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getPlatform());
  }
}
