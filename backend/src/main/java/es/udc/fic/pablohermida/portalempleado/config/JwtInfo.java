package es.udc.fic.pablohermida.portalempleado.config;

/**
 * The type Jwt info.
 */
public class JwtInfo {
  /**
   * id of user.
   */
  private final Long userId;

  /**
   * login of user.
   */
  private final String userName;

  /**
   * role of user.
   */
  private final String role;

  /**
   * Instantiates a new Jwt info.
   *
   * @param userId   the user id
   * @param userName the user name
   * @param role     the role
   */
  public JwtInfo(final Long userId, final String userName, final String role) {

    this.userId = userId;
    this.userName = userName;
    this.role = role;

  }

  /**
   * Gets user id.
   *
   * @return the user id
   */
  Long getUserId() {
    return this.userId;
  }

  /**
   * Gets user name.
   *
   * @return the user name
   */
  public String getUserName() {
    return this.userName;
  }

  /**
   * Gets role.
   *
   * @return the role
   */
  public String getRole() {
    return this.role;
  }

}
