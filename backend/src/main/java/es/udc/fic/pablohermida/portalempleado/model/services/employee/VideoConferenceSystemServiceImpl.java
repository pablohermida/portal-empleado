package es.udc.fic.pablohermida.portalempleado.model.services.employee;

import es.udc.fic.pablohermida.portalempleado.model.daos.employee.VideoConferenceSystemDao;
import es.udc.fic.pablohermida.portalempleado.model.entities.employee.VideoConferenceSystem;
import es.udc.fic.pablohermida.portalempleado.utils.Utils;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.FieldErrorException;
import java.util.List;
import javax.management.InstanceAlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The type Video conference system service.
 */
@Service
public class VideoConferenceSystemServiceImpl
    implements VideoConferenceSystemService {
  @Autowired
  private VideoConferenceSystemDao vcsystemDao;

  @Override
  public Long createVideoConferenceSystem(final VideoConferenceSystem vcs)
      throws InstanceAlreadyExistsException, FieldErrorException {
    Utils.validateString(vcs.getPlatform(), 2, 50, "vcsystems.fields.platform");
    if (this.vcsystemDao
        .existsByPlatformIgnoreCaseContaining(vcs.getPlatform())) {
      throw new InstanceAlreadyExistsException(
          "VideoConference System " + "already exists");
    }

    VideoConferenceSystem vcsCreated = this.vcsystemDao.save(vcs);

    return vcs.getId();
  }

  @Override
  public List<VideoConferenceSystem> getVideoConferenceSystems() {
    return this.vcsystemDao.findAllByOrderByPlatform();
  }
}
