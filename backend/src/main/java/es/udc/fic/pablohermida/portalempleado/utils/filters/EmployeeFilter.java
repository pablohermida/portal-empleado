package es.udc.fic.pablohermida.portalempleado.utils.filters;

import java.util.List;
import java.util.Objects;

/**
 * The type Employee filter.
 */
public class EmployeeFilter {
  private final String username;
  private final String name;
  private final String email;
  private final List<Long> jobsId;
  private final Long roleId;

  /**
   * Instantiates a new Employee filter.
   */
  public EmployeeFilter() {
    this.username = null;
    this.name = null;
    this.email = null;
    this.jobsId = null;
    this.roleId = null;
  }

  /**
   * Instantiates a new Employee filter.
   *
   * @param username the username
   * @param name     the name
   * @param email    the email
   * @param jobsId   the jobs id
   * @param roleId   the role id
   */
  public EmployeeFilter(final String username, final String name,
                        final String email, final List<Long> jobsId,
                        final Long roleId) {
    this.username = username;
    this.name = name;
    this.email = email;
    this.jobsId = jobsId;
    this.roleId = roleId;
  }

  /**
   * Gets username.
   *
   * @return the username
   */
  public String getUsername() {
    return this.username;
  }

  /**
   * Gets name.
   *
   * @return the name
   */
  public String getName() {
    return this.name;
  }

  /**
   * Gets email.
   *
   * @return the email
   */
  public String getEmail() {
    return this.email;
  }

  /**
   * Gets jobs id.
   *
   * @return the jobs id
   */
  public List<Long> getJobsId() {
    return this.jobsId;
  }

  /**
   * Gets role id.
   *
   * @return the role id
   */
  public Long getRoleId() {
    return this.roleId;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || this.getClass() != o.getClass()) { return false; }
    final EmployeeFilter that = (EmployeeFilter) o;
    return Objects.equals(this.getUsername(), that.getUsername()) && Objects
        .equals(this.getName(), that.getName()) && Objects
        .equals(this.getEmail(), that.getEmail()) && Objects
        .equals(this.getJobsId(), that.getJobsId()) && Objects
        .equals(this.getRoleId(), that.getRoleId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getUsername(), this.getName(), this.getEmail(),
        this.getJobsId(), this.getRoleId());
  }
}
