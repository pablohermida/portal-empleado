package es.udc.fic.pablohermida.portalempleado.rest.dtos.vcsystem;

import java.util.Objects;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * The type Video conference employee dto.
 */
public class VideoConferenceEmployeeDto {
  @NotNull
  private final Long idVcsystem;
  @NotBlank
  private final String data;
  private final String nameVcsystem;

  /**
   * Instantiates a new Video conference employee dto.
   *
   * @param idVcsystem   the idVcsystem
   * @param data         the data
   * @param nameVcsystem the name vcsystem
   */
  public VideoConferenceEmployeeDto(final Long idVcsystem, final String data,
                                    final String nameVcsystem) {
    this.idVcsystem = idVcsystem;
    this.data = data;
    this.nameVcsystem = nameVcsystem;
  }

  /**
   * Gets id vcsystem.
   *
   * @return the id vcsystem
   */
  public Long getIdVcsystem() {
    return this.idVcsystem;
  }

  /**
   * Gets data.
   *
   * @return the data
   */
  public String getData() {
    return this.data;
  }

  /**
   * Gets name vcsystem.
   *
   * @return the name vcsystem
   */
  public String getNameVcsystem() {
    return this.nameVcsystem;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || this.getClass() != o.getClass()) { return false; }
    final VideoConferenceEmployeeDto that = (VideoConferenceEmployeeDto) o;
    return Objects.equals(this.idVcsystem, that.idVcsystem) && Objects
        .equals(this.getData(), that.getData()) && Objects
        .equals(this.getNameVcsystem(), that.getNameVcsystem());
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(this.idVcsystem, this.getData(), this.getNameVcsystem());
  }
}
