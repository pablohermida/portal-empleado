package es.udc.fic.pablohermida.portalempleado.model.entities.employee;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The type Employee security data.
 */
@Entity
@Table(name = "employee_security_data", schema = "portalempleado")
public class EmployeeSecurityData {

  /**
   * The Employee id.
   */
  @Id
  @Column(name = "employeeId")
  private Long employeeId;
  /**
   * The Iban.
   */
  private String iban;

  private boolean suscribed;

  @OneToOne
  @MapsId
  @JoinColumn(name = "employeeId")
  private Employee employee;

  /**
   * Instantiates a new Employee security data.
   */
  public EmployeeSecurityData() {
  }

  /**
   * Instantiates a new Employee security data.
   *
   * @param iban      the iban
   * @param suscribed the suscribed
   */
  public EmployeeSecurityData(final String iban, final boolean suscribed) {
    this.iban = iban;
    this.suscribed = suscribed;
  }

  /**
   * Instantiates a new Employee security data.
   *
   * @param employeeId the employee id
   * @param iban       the iban
   * @param suscribed  the suscribed
   */
  public EmployeeSecurityData(final Long employeeId, final String iban,
                              final boolean suscribed) {
    this(iban, suscribed);
    this.employeeId = employeeId;
  }

  /**
   * Gets employee id.
   *
   * @return the employee id
   */
  public Long getEmployeeId() {
    return this.employeeId;
  }

  /**
   * Gets iban.
   *
   * @return the iban
   */
  public String getIban() {
    return this.iban;
  }

  /**
   * Is suscribed boolean.
   *
   * @return the boolean
   */
  public boolean isSuscribed() {
    return this.suscribed;
  }

  /**
   * Gets employee.
   *
   * @return the employee
   */
  public Employee getEmployee() {
    return this.employee;
  }

  /**
   * Sets employee.
   *
   * @param employee the employee
   */
  public void setEmployee(final Employee employee) {
    this.employee = employee;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || this.getClass() != o.getClass()) { return false; }
    final EmployeeSecurityData that = (EmployeeSecurityData) o;
    return this.isSuscribed() == that.isSuscribed() && Objects
        .equals(this.getIban(), that.getIban());
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getIban(), this.isSuscribed());
  }
}
