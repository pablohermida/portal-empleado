package es.udc.fic.pablohermida.portalempleado.model.entities.employee;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The type Job.
 */
@Entity
@Table(name = "job", schema = "portalempleado")
public class Job {

  /**
   * The Id.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  /**
   * The Name.
   */
  private String name;

  /**
   * Instantiates a new Job.
   */
  public Job() {
  }

  /**
   * Instantiates a new Job.
   *
   * @param name the name
   */
  public Job(final String name) {
    this.name = name;
  }

  /**
   * Instantiates a new Job.
   *
   * @param id   the id
   * @param name the name
   */
  public Job(final Long id, final String name) {
    this(name);
    this.id = id;
  }

  /**
   * Gets id.
   *
   * @return the id
   */
  public Long getId() {
    return this.id;
  }

  /**
   * Gets name.
   *
   * @return the name
   */
  public String getName() {
    return this.name;
  }

  /**
   * Sets name.
   *
   * @param name the name
   */
  public void setName(final String name) {
    this.name = name;
  }

  /**
   * Override method equals of Object class.
   *
   * @param o the employee
   */
  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || this.getClass() != o.getClass()) {
      return false;
    }
    final Job job = (Job) o;
    return Objects.equals(this.getName(), job.getName());
  }

  /**
   * Override method hashCode of Object class.
   *
   * @return int
   */
  @Override
  public int hashCode() {
    return Objects.hash(this.getName());
  }
}
