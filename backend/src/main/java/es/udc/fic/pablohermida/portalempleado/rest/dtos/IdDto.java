package es.udc.fic.pablohermida.portalempleado.rest.dtos;

import java.util.Objects;

/**
 * The type Id dto.
 */
public class IdDto {
  private final Long id;

  /**
   * Instantiates a new Id dto.
   *
   * @param id the id
   */
  public IdDto(final Long id) {
    this.id = id;
  }

  /**
   * Gets id.
   *
   * @return the id
   */
  public Long getId() {
    return this.id;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) { return true; }
    if (o == null || this.getClass() != o.getClass()) { return false; }
    final IdDto idDto = (IdDto) o;
    return Objects.equals(this.getId(), idDto.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getId());
  }
}
