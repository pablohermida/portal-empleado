package es.udc.fic.pablohermida.portalempleado.rest.dtos.employee;

import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Role;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Role conversor.
 */
public class RoleConversor {

  /**
   * Instantiates a new Role conversor.
   */
  public RoleConversor() {}

  /**
   * To role dtos list.
   *
   * @param roles the roles
   *
   * @return the list
   */
  public static List<RoleDto> toRoleDtos(final List<Role> roles) {
    return roles.stream().map(RoleConversor::toRoleDto)
        .collect(Collectors.toList());
  }

  private static RoleDto toRoleDto(final Role role) {
    return new RoleDto(role.getId(), role.getName());
  }

}
