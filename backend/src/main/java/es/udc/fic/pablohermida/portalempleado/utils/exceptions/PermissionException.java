package es.udc.fic.pablohermida.portalempleado.utils.exceptions;

/**
 * The type Permission exception.
 */
public class PermissionException extends Exception {
}
