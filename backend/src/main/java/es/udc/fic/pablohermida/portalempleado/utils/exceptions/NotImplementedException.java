package es.udc.fic.pablohermida.portalempleado.utils.exceptions;

/**
 * The type Not implemented exception.
 */
public class NotImplementedException extends RuntimeException {
  /**
   * Instantiates a new Not implemented exception.
   *
   * @param functionName the function name
   */
  public NotImplementedException(final String functionName) {
    super("[" + functionName + "] Method not implemented yet.");
  }
}
