package es.udc.fic.pablohermida.portalempleado.utils.exceptions;


/**
 * The type Field error exception.
 */
public class FieldErrorException extends Exception {
  private final String field;
  private final FieldErrorReason reason;

  /**
   * Instantiates a new Field error exception.
   *
   * @param field the field
   * @param fer   the fer
   */
  public FieldErrorException(final String field, final FieldErrorReason fer) {
    super(field + " " + fer.getMessage());
    this.field = field;
    this.reason = fer;
  }

  /**
   * Gets field.
   *
   * @return the field
   */
  public String getField() {
    return this.field;
  }

  /**
   * Gets reason.
   *
   * @return the reason
   */
  public FieldErrorReason getReason() {
    return this.reason;
  }
}
