package es.udc.fic.pablohermida.portalempleado.utils.exceptions;

/**
 * The enum Field error reason.
 */
public enum FieldErrorReason {
  /**
   * The Is blank.
   */
  IS_BLANK("is_blank", "is mandatory"),
  /**
   * The Length.
   */
  LENGTH("length", "length isn´t valid"),
  /**
   * The Is after now.
   */
  IS_AFTER_NOW("is_after", "is after than now"),
  /**
   * The Pattern.
   */
  PATTERN("pattern", "format isn´t valid"),
  /**
   * The Instance.
   */
  INSTANCE("instance", "isn´t exist");

  private final String message;

  FieldErrorReason(final String name, final String message) {
    this.message = message;
  }

  /**
   * Gets message.
   *
   * @return the message
   */
  public String getMessage() {
    return this.message;
  }
}
