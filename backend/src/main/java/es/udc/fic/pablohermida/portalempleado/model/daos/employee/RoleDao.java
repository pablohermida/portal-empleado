package es.udc.fic.pablohermida.portalempleado.model.daos.employee;

import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Role;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * The interface Role dao.
 */
public interface RoleDao extends PagingAndSortingRepository<Role, Long> {
  /**
   * Find all by order by name desc list.
   *
   * @return the list
   */
  List<Role> findAllByOrderByNameAsc();
}
