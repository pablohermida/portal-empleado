package es.udc.fic.pablohermida.portalempleado.utils.validators;


import es.udc.fic.pablohermida.portalempleado.model.entities.employee.Employee;
import es.udc.fic.pablohermida.portalempleado.utils.Utils;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.FieldErrorException;
import es.udc.fic.pablohermida.portalempleado.utils.exceptions.FieldErrorReason;
import java.time.LocalDate;
import java.util.regex.Pattern;
import org.apache.commons.lang3.Validate;

/**
 * The type Employee validator.
 */
public class EmployeeValidator extends Validate {

  /**
   * Validate.
   *
   * @param employee the employee
   *
   * @throws FieldErrorException the field error exception
   */
  public static void validate(final Employee employee)
      throws FieldErrorException {
    Utils.validateString(employee.getName(), 2, 30, "employee.fields.name");
    Utils.validateString(employee.getLastname(), 2, 30,
        "employee.fields.lastname");
    if (employee.getImage() != null) {
      Utils
          .validateString(employee.getImage(), 0, 255, "employee.fields.image");
    }
    validateDateBirth(employee.getDateBirth());
    validateEmail(employee.getEmail());
    validateDni(employee.getDni());
    Utils.validateString(employee.getAddress(), 1, 100,
        "employee.fields.address");
    Utils.validateString(employee.getAddressWork(), 1, 100,
        "employee.fields.address_work");
    Utils.validateString(employee.getPhone(), 9, 12, "employee.fields.phone");
    Utils.validateString(employee.getDuration(), 1, 10,
        "employee.fields.duration");
    if (employee.getDateStart() == null) {
      throw new FieldErrorException("employee.fields.date_start",
          FieldErrorReason.INSTANCE);
    }
    Utils.validateString(employee.getWorkDay(), 1, 30,
        "employee.fields.work_day");
    Utils.validateString(employee.getWorkHours(), 1, 300,
        "employee.fields.work_hours");
    if (employee.getRole() == null) {
      throw new FieldErrorException("employee.fields.role",
          FieldErrorReason.INSTANCE);
    }
    if (employee.getJob() == null) {
      throw new FieldErrorException("employee.fields.job",
          FieldErrorReason.INSTANCE);
    }
    if (employee.getEmployeeAuth() != null) {
      Utils.validateString(employee.getEmployeeAuth().getUsername(), 2, 30,
          "employee.fields.username");
      Utils.validateString(employee.getEmployeeAuth().getPassword(), 60, 96,
          "employee.fields.password");
    } else {
      throw new FieldErrorException("employee.fields.employee_auth",
          FieldErrorReason.INSTANCE);
    }
    if (employee.getEmployeeSecurityData() != null) {
      Utils.validateString(employee.getEmployeeSecurityData().getIban(), 22, 24,
          "employee.fields.iban");
    } else {
      throw new FieldErrorException("employee.fields.employee_security_data",
          FieldErrorReason.INSTANCE);
    }
  }

  private static void validateDateBirth(final LocalDate dateBirth)
      throws FieldErrorException {
    Utils.validateDate(dateBirth, "employee.fields.date_birth");
    if (LocalDate.now().isBefore(dateBirth)) {
      throw new FieldErrorException("employee.fields.date_birth",
          FieldErrorReason.IS_AFTER_NOW);
    }
  }

  private static void validateEmail(final String email)
      throws FieldErrorException {
    final Pattern pattern = Pattern.compile("^(.+)@(.+)[.][a-z]{2,3}$");
    Utils.validateString(email, 6, 255, "employee.fields.email");

    if (!pattern.matcher(email).matches()) {
      throw new FieldErrorException("employee.fields.email",
          FieldErrorReason.PATTERN);
    }
  }

  private static void validateDni(final String dni) throws FieldErrorException {
    Utils.validateString(dni, 9, 9, "employee.fields.dni");
    final String lettersDni = "TRWAGMYFPDXBNJZSQVHLCKE";
    final String numberDni = dni.substring(0, 8);
    final char letterDni = dni.charAt(8);
    try {
      final int number = Integer.parseInt(numberDni);
      if (lettersDni.charAt(number % 23) != letterDni) {
        throw new FieldErrorException("employee.fields.dni",
            FieldErrorReason.PATTERN);
      }
    } catch (final NumberFormatException nfe) {
      throw new FieldErrorException("employee.fields.dni",
          FieldErrorReason.PATTERN);
    }
  }
}
